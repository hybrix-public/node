# v1.2.4
## Furry Fuchsia Chupacabra

* insight engine: lower the defaults of maxDustUnspents and maxRegularUnspents to avoid request denials
* logs: truncate large error outputs
* new qrtz method gate: limiter and throttler for intensive processes
* enable deal engine to use gate limiter
* enable swap proxy engine to use gate limiter
* qrtz method bank: spawn deterministic blobs as proper child processes instead of shell
* qrtz method wait: bugfix data passthrough and add test for this
* qrtz methods: deprecate all usage of this., replaced by p.
* updated and recompiled all qrtz tests
* enhancements and bugfixes to schedule processor: hasParentStarted, hasParentStopped
* return reliable process getName results when running from external scripts
* update node_modules: vulnerability of cross-spawn is not an issue for hybrixd
* update documentation
* hybrixd: bugfixes to handling of localhost POST requests
* hybrix-lib: by default use POST requests for transactions
* web-wallet: implementation of using POST requests for remittances
* web-wallet: ensure MAX button takes fees properly into account
* web-wallet: bugfix to history display amount
* web-wallet: improvements to dutch (NL) translations
* web-wallet: packages update


# v1.2.3
## Happy Fuchsia Kraken

* better verbosity for unspent selection errors
* adapt all Bitcoin network compatible asset engines to use new UTXO engine
* more advanced UTXO selection method for automatic dust collection
* ensure proper handling of undefined cumulative value
* add asset in progress Solana
* scripting: no longer include npm updates
* hybrixd: remove unneeded global variable
* valuations: fork CPU intensive valuation calculations (spawnTask)
* valuations: ensure spawned child processes are detached
* valuations: add spawnParse helper
* swap: set regex for virtualSymbol, and expose validateVirtualSymbol method
* swap: always set transaction status to done if progress is 1
* swap: symbols in uppercase for failure notifications
* swap: add error codes to all proposal failure modes
* qrtz: improve documentation for qrtz func


# v1.2.2
## Sparkling Cyan Mermaid

* fix failure notifications for deferred proposals
* more verbosity on swap request failures
* subprocess zero jump handling properly fix
* remove timeout modification of handleSource method
* fix request formatting for XRP
* sparser logging verbosity
* clean up routing 'id' response for initial API calls
* merge request enabling explorer price valuation input flexibility
* deprecate README.npm.md
* update year in launch script
* swap: deal failure notifications
* swap: more lenient timeouts, improved error handling
* swap: fork notification processes increasing execution speed
* swap: send notification to allocator and node operator on swap failures
* swap: proper host array handling for swap proxy module
* qrtz: add per-item index variable to each and fold operations
* swap: correct key of admin dealFailure message
* web-wallet: bugfixes to fee calculations
* web-wallet: corrections to translation files
* web-wallet: better fee handling and avoidance of dust swaps
* web-wallet: ensure newly created swaps are always added as pending
* web-wallet: avoid rendering tx description/attachment if undefined
* web-wallet: add message during progress of swap deal initialization
* web-wallet: disable next button when dust swap or impossible maximum
* web-wallet: cosmetic improvement to modal buttons


# v1.2.1
## Chirpy Cyan Pegasus

* proper host array handling for swap proxy module
* improved error handling for swap proxy module
* APIqueue: repair socks and tor connectivity
* improved handling of fasthost option
* return allocator contact to node operator
* bugfix notifications for node administrators
* ensure push verification for BNB asset
* add error codes to allocation engine
* add error codes to ledger exceptions
* remove deprecated proposal list prefix
* bugfixing liquidity calculation interdependencies
* add alias for Alpha to valuations engine
* fix regression bugs due to allocation submodules reorganization
* smtp module: send node administrator more specific messages
* logging: remove warning on sync errors
* increase maximum childprocess spawn depth
* update node_modules
* update package json
* web-wallet: ensure to decode URI component for paymentRequest descriptions
* web-wallet: always sync-refresh pending once when opening viewAsset
* web-wallet: correct display of error messages in swapAmount modal
* web-wallet: bugfix edge-case lockup when logging in from QR scan
* web-wallet: ensure qrScanner release stored methods after login
* web-wallet: correction of translation strings
* web-wallet: add missing contact variable
* web-wallet: error message corrections
* web-wallet: cosmetic UI fixes
* web-wallet: add quotes


# v1.2.0
## Merry Cyan Noodlefish

* bugfix to bitcore unspent handling
* remove deprecated elements
* renaming and parking (deprecated) modules
* ignore fake Alpha token valuation listed on Binance
* remove deprecated static HY pricing mechanism
* rename allocation sub-engines, tidy JSON for all modules
* tidy JSON for all recipes
* halt processes with spawn depth larger than 32
* more precise error message in case of zero jump
* cleaner output for non-root sessions
* add unified asset alpha
* updated icons script
* added Alpha to icons
* deprecated dependency on node_modules git repository
* update SMLY asset
* repair history functionality for VIC assets
* documentation update
* update qrtz tests
* routing correction
* integrated previously external node_modules into repository


# v1.1.34
## Furry Lime Idris

* recipe updates
* ability to parse hybrix valuations from swap allocation rates
* new compression method for unified assets resulting in shorter addresses
* ensure bank transfers never exceed minimum and maximum sufficiencies
* deprecated static valuation method for HY token
* removed hy-legacy from HY unified asset
* overhaul and modernization of deterministic modules
* addition of Smileycoin recipe and deterministic module
* qrtz: improved mime handling
* qrtz: bugfixes to various methods
* swap: store stats formatted to $storageFactor precision
* swap: auto-update valuations based on change in total liquidity
* swap: allow querying allocation rates via swap proxy engine
* wallet: bugfixes in balance and sufficiency calculation
* wallet: warn on minimum and maximum banking amount limits
* wallet: refresh history if opened and pending transaction is done
* wallet: more efficient and faster refresh on pending transactions
* wallet: add user message for assets in testing or maintenance mode
* wallet: tighter padding on preview numbers in swap selectable assets
* wallet: empty search field when opening manageAssets modal
* wallet: disable dispute button on swaps older than 14 days
* wallet: cosmetic fixes and translation corrections
* hybrix-lib: better handling of cached asset details
* hybrix-lib: optimize load and save methods
* hybrix-lib: bugfix to merge strategy for pending transaction data
* hybrix-lib: no fetching remote updates on swap status after 14 days
* hybrix-lib: more verbose errors for deterministic code blobs


# v1.1.33
## Happy Lime Changeling

* bugfix execution possibility of qrtz flat-syntax scripts
* unified asset parsing of both legacy and new HY token
* more verbosity on parsing errors
* standardization of setting data using standard GET URL parameters
* add simplified boot.json example
* qrtz: option to quick-trim right side of string/array
* recipes: bugfix automatic gas price updates
* recipes: update HY unified asset recipe
* recipes: replaced asset TOMO with asset VIC
* recipes: migrate TOMO-based assets to VIC
* recipes: updated Ethereum default gas price
* recipes: add legacy HY recipe, bind to VIC
* recipes: updates to token recipes
* modules: bugfix to valuations
* modules: added new valuations source
* modules: wavalidator update
* modules: explorer update with edge-case bch bugfix
* web-wallet: cosmetic UX improvements to modals
* web-wallet: make logo on QR-code more transparent
* web-wallet: paymentRequest optimization and bugfixing of address handling
* web-wallet: add description option to payment requests
* web-wallet: add descriptions to payment requests, adhering to BIP21
* web-wallet: renaming 'legacy' mode to more apt 'sessionKey' mode
* web-wallet: auto-translation script using trans tool
* web-wallet: vertical height Apple devices bugfix
* web-wallet: routing fix when scanning QR code
* web-wallet: additional translation files for NL language
* web-wallet: compilation script accepts language abbreviation
* web-wallet: local styling fix for multi-line text buttons
* web-wallet: enable tab-title translation
* web-wallet: more readable error messages
* web-wallet: bugfix and improve pending transaction UX
* web-wallet: translation infrastructure
* web-wallet: translation updates to libraries
* web-wallet: translation strings insertion, remove duplicate header IDs
* web-wallet: auto-translation after compilation
* web-wallet: update node_modules dependencies for nodejs upgrade
* web-wallet: update webpack configuration
* hybrix-jslib: enable alternative POST method in edge cases
* hybrix-jslib: rename 'legacy' mode to more apt sessionKey mode
* hybrix-jslib: shorter caching period for asset details
* hybrix-jslib: bring node_modules up to date
* hybrix-jslib: update default test symbols
* hybrix-jslib: update pending transaction routines
* documentation: change github to gitlab icons
* documentation: updated links
* documentation: updated qrtz documentation
* commmon: BIP27 improvements to qr-validation


# v1.1.32
## Sparkling Lavender Ghost

* ensure pushtime registered with immediacy
* improvement to automatic swap claims
* proper handling of BNB failed swap transactions
* retry swap remittances for BSC chain on failure
* swap fee mutation routine: remove magic numbers inline
* maximum set amount of swap proposals per time period
* better support for handling legacy unified addresses
* remove poisoned sources from valuations module
* add proper regex validation for all EVM chains
* improved history functionality for unified assets
* bugfixes to SBCH asset RPC calls
* updates to documentation
* qrtz: bugfix for method atom
* qrtz: bugfix for method tran
* web-wallet: banking modal UX improvements
* web-wallet: reinitialize transaction modal on open
* web-wallet: edit email for reporting in settings modal
* web-wallet: hide allocation dashboard from newbie users
* web-wallet: transaction details race condition fix
* web-wallet: cosmetic fixes across all modals
* web-wallet: quote of the day feature on login page


# v1.1.31
## Chirpy Lavender Vampire

* banking UX/UI implementation for gateway enabled transactions
* new module for agnostic, federated and/or decentralized banking
* more stringent checking of non-empty qrtz with statements
* initial host source adaptation to test static UI logins
* bugfix to avoid fatal parsing error
* universal login cosmetic update
* renaming and culling of assets
* update to allocation module
* update to documentation
* update to qrtz tests
* move BNB.HY to recipes.EXTRA


# v1.1.30
## Merry Lavender Nopperabo

* enable sending of multiple successive transactions on EVM compatible chains
* remove old EVM transaction nonces from cache
* removed unnecessary variables from Qrtz method each
* removal of non-responsive RPC endpoints in recipes
* updates and bugfixes to all Qrtz method tests
* new method: Qrtz exec, a root-only method to execute shell commands
* enable Qrtz method atom to use only one argument
* memory improvements to running hybrix node on low-end hardware
* deprecation of empty exchange template module
* move mockchain module to .EXTRA
* update to valuations sources and parsing
* documentation updates and category reorganization
* new Qrtz method to print info notices to log console
* testing of hybrix integration with web-commerce packages


# v1.1.29
## Jolly Lavender Tengu

* upgraded dialog getSomeHy avoiding needless scanning
* support descriptions in unified transactions
* updated applications list
* fixes to listAssets, login in web-wallet
* QR scanning and history functionality fixes
* login using compact credentials endpoint
* fixed tx by URI
* payment requests bugfix
* allow empty tx descriptions
* CSS bugfixes and cosmetic updates
* remove disfunctional API
* shortened descriptions for unified HY assets
* implement POST for y-chan cryptography
* add eslint, gitignore and create empty conf on first boot
* unified subassets and assets now store cache under similar keys
* transaction history larger buffer and prefix-less storage
* update to stat reporter
* bugfixes to unified asset functionality
* illegal jumps now handled properly
* updated qrtz methods
* avoid false zero step reporting
* removed unnecessary logging
* form decimal error bugfix
* stale data bugfix to avoid longtime cache mismatches
* improvements to modules
* swap module: mark local data as stale if no metadata available


# v1.1.28
## Cheery Lavender Churel
## 23-11-2022

* signed storage implementation
* update asset support and history functions
* added support for transaction descriptions
* moved SHIFT to recipes.EXTRA
* module storage: allow signed storage when formatting key using semicolons
* move non-functional sign module to extra
* move DGB and RISE to extra
* hop counting on synchronization to introduce locality for storage blobs
* informative changes to setup script
* setup: make empty dist directory in order to create valid web-wallet symbolic link
* moved deprecated Burst recipes to recipes.EXTRA
* added symlinking and auto-download for deterministic
* remove deterministic module to prepare for symlinking
* add support for arm64 binaries
* symlink to web-wallet instead of default inclusion
* prepare interface and web-wallet for always symlinking
* build, audit and testing scripts bugfixes
* security audited packages
* hybrixd: suppress .conf not found error, change copyright date


# v1.1.27
## Curly Lavender Leyak
## 03-10-2022

* updated the QR-scanner and added guides in UI
* suppress .conf not found error, change copyright date
* finetuned compile for distribution
* update all dependencies
* add support for aarch64 in setup.sh
* pilot gateway link under banking
* nodejs version bump to v16 LTS
* separate sass compilation from webpack process
* styling improvements in web-wallet
* usability fixes across the board
* fixed XRP ledger problem when swapping
* work on bank swaps ongoing
* allow for message passing on transaction rout to login view
* renamed blockchain message to attachment
* auto-hide Create Account block when logging in from pass/QR
* bugfixes to virtual asset allocation management
* asset select modal initial commit
* balance now termed correctly as sufficiency
* fix submit swap to pending
* use remit method instead of claim to complete swaps


# v1.1.26
## Shiny Lavender Dragon
## 19-05-2022

* adapted buildrelease
* find method to locate deals based upon content like TXID
* improved parsing of labels, ignore @ symbol inside of quotes
* put xDAO into maintenance mode
* bugfixes in swap proxy
* bugfixes in setContact and related methods
* prepend data objects separately
* allow for returning error codes
* set contact bug fixed
* fix history issues
* fix tag destination issue for XRP
* auto purge deals in history
* auto purge stale accounts
* remove exceptions for HY fiat unifieds
* asynchronous compressed binary storage of local variables
* properly handle remote 404 errors
* retry or fail on 502 proxy errors
* simplified fiat assets
* validate simplified fiat assets
* update documentation
* allocator panel UX/UI redesign
* CSS styling fixes
* pending modal improvement


# v1.1.25
## Bouncy Lavender Minotaur
## 19-04-2022

* improved unified asset handling
* removed deprecated transport module
* auto-insert missing TXID on null when disputing
* fixed module hystat
* capitalization of logs
* further async efficiency improvements
* fixed excl step
* avoid selection of own endpoints
* remove indexes of non-existing files marked for synchronization
* disable logs of defective peers
* prepare y-chan for adding POST payload in the future
* improved save method
* small bugfixes that hamper swaps
* proper meta handling in swap proxy module
* integration of contacts management
* rewrite of contacts UI
* enable contacts application in UX/UI


# v1.1.24
## Fluffy Lavender Chupacabra
## 18-03-2022

* add many new fiat symbols to valuations parse
* increased efficiency of APIqueue using asyncronous functions
* qrtz: more efficient handling of command calls
* efficiency rewrite of synchronization and transports
* efficiency rewrite of storage engine
* new elliptic curve crypto methods for signing data
* only synchronize when remote list is non-empty
* avoid shuffling sync list if not needed
* data object size overflow bug fixed
* allow non-root to query sync peers
* avoid crashing on connection failures
* option to pass own endpoint when requesting peers
* testing page for decentralized storage functions
* security and dependency audits and fixes
* cosmetic fixes and UX/UI improvements
* update documentation for developers


# v1.1.23
## Cosy Lavender Kraken
## 15-02-2022

* updated documentation
* ethereum module: data stream not used as variable address, so removed
* new module enabling sending of messages over SMTP
* parked deprecated modules to modules.EXTRA
* parked Kubernetes scripts
* new scripts to enable building on any VPS
* fixed swap issues for Waves
* more flexible scriptPubKey handling for Waves
* full confirmations support for Waves
* qrtz: fix fatal error in peek when looking for non-existing variable data
* qrtz: parse labels only outside of quotes
* qrtz: make sure scan returns numeric values properly
* qrtz: replace tran with an each until fallback bug can be repaired
* qrtz: handle proper numeric values in objects correctly


# v1.1.22
## Little Lavender Pixie
## 14-01-2022

* rebuild release pipeline to work outside of Kubernetes
* backup of hybrix docker files
* replace tran with an each until fallback bug can be repaired
* handle proper numeric values in objects correctly
* web-wallet: dispute modal and functionality
* web-wallet: provide correct calculation for remote liquidity
* web-wallet: fix spinner on swapAmount modal
* web-wallet: finetuning of swap modals
* web-wallet: CSS cosmetic polishing


# v1.1.21
## Happy Ivory Ogre
## 09-12-2021

* web-wallet swapAmount: modal now properly warns on low liquidity
* web-wallet swapReview: modal catch sufficiency issues
* Qrtz method flow: added string comparison option
* fix to allow proposal deletion with proper key access
* determine liquidity already during estimation
* speed up Qrtz execution
* adjust gas in BNB recipe
* generate scriptPubKey locally for Bitcoin Cash
* helper module assetfunctions to assist with outlier dependencies
* NowNodes module now uses assetfunctions module
* some finetuning for XDAO


# v1.1.20
## Sparkling Indigo Mermaid
## 01-12-2021

* generate address if empty object
* save local data simultaneously
* remove debugging
* updates to SCSS styling
* general UI module changes
* new engine module: xDAO application
* ensure all rout calls are quoted
* make swap engine addressCache persistent
* simplify swap engine estimate rout call
* do not produce error on claim of swap
* compare numeric values when finding swap pairs
* fasthost option for alternative routing and fallback in case of connectivity problems
* new engine module to allow bulk portfolio queries
* split balances method into live sequential and parallelized versions
* allow returning valuations in any denomination simultaneously
* fixed bug of reinitializing addresses
* more flexible validation and decompression of addresses
* allow qrtz method take without arguments
* allow loop to take length of array or object


# v1.1.19
## Chirpy Indigo Pegasus
## 16-11-2021

* modal button fixes using CSS flex
* unified assets to include essential units
* fiat unified assets now have samples
* fixing of small issue with asset XRP
* avoid parallel pushing of transactions
* many UI modal updates
* applications infrastructure
* introduction of applications modal


# v1.1.18
## Merry Indigo Noodlefish
## 12-11-2021

* added sources nomics and coincodex to valuations engine
* oracle ignores coingecko when it shares poisoned data


# v1.1.17
## Jolly Indigo Unicorn
## 03-11-2021

* fix merg to properly handle arrays of objects
* add configuration default for swap module
* fix qrtz scan to return proper objects when matched
* simplify unspents in NowNodes engine
* reviewed functionality of LTC and other BTC clones
* added token: ETH.DOG


# v1.1.16
## Cheery Indigo Phoenix
## 03-11-2021

* updated compiled bitcoinjslib


# v1.1.15
## Curly Indigo Fairy
## 28-10-2021

* fixed factor bug in web-wallet swapAmount modal
* improve pairStats compiling
* take BCH fee down to sane level
* filter out allocation accounts that have pairs with dust amounts
* make sure rout calls are quoted to avoid erroneous colon splitting
* speed up pair find and select
* repair reformatTx


# v1.1.14
## Shiny Indigo Sphinx
## 22-10-2021

* properly qrtz form result amounts
* allow multi transactionID transaction claims
* multi transactionID queries for unified assets
* enable qrtz key-value merges
* double the fee for Bitcoin-like cryptos
* remove swap proposal from queue when force settled
* enable forced settlements in case of disputes
* improve qrtz scan method
* bugfix to qrtz sort method
* bitcore engine unspents dust fix


# v1.1.13
## Bouncy Indigo Elf
## 08-10-2021

* caching mechanism for swap relay module
* add unified asset HY.BCH
* add translation for HY.BCH in valuations parser
* proper sample for SBCH recipe
* allocation module: noCache option for getBalance
* bugfix in pending modal


# v1.1.12
## Fluffy Indigo Gremlin
## 07-10-2021

* avoid splitting colon addresses during raw transaction


# v1.1.11
## Cosy Indigo Kobold
## 07-10-2021

* fix breaking bug with colon in unified addressing


# v1.1.10
## Little Indigo Leprechaun
## 07-10-2021

* put read method into maintenance


# v1.1.9
## Furry Indigo Goblin
## 04-10-2021

* fix universal login bugs
* globally restyle select elements
* sort HY assets to top of list
* swap matcher: fix several liquidity issues
* swap matcher: proper rebalancing of cached asset values


# v1.1.8
## Happy Indigo Ogre
## 29-09-2021

* ensure entire pairlist is produced if accountID is undefined
* partly unify SBCH into BCH for valuation rate determination
* park SBCH tokens until properly supported
* added icon for SBCH
* improve history caching and retrieval, introduce history for SBCH
* move deprecated recipes to extra
* better description for BSC
* new qrtz method iter
* replace some each methods by iter
* improve speed on swap system
* finetuning rout
* improve routing over TOR and process progress relaying
* adjust prog to new format (seconds instead of milliseconds)
* improvements to hcmd
* proper progress handling, and repair of prog method
* progress level reporting across process parents and children
* bugfix and optimizations in Ethereum API engine
* increase timeout for new swap proposal
* remove accounts with zero allocation from eligibility
* update XRP deterministic module
* Lisk parked while we wait for help from their community
* update documentation
* updated icons to include SBCH
* prohibit negative timeout values in APIqueue


# v1.1.7
## Sparkling Fuchsia Mermaid
## 17-09-2021

* auto-refresh for pending transactions
* auto-correct comma's in amount fields
* better handling of offset derived keys
* improvements to allocator panel logic
* modal CSS fixes
* change send note on tomo.hy recipe
* leading zeroes in asset view bugfix
* pass offset to transaction signing process in hybrix-jslib


# v1.1.6
## Chirpy Fuchsia Pegasus
## 14-09-2021

* further improvement to QR scanner
* create pending right after initial swap transaction
* adapt recipes for DGB, ETC, stage AVAX and SBCH
* remove WIP recipes for this release round


# v1.1.5
## Merry Fuchsia Noodlefish
## 24-08-2021

* check if fee-modifier is proper number
* better max-fee calculation for unified assets
* allow exact fuel amount for EVM transactions
* added average rate to valuations engine
* lessen needed amount of confirmations for BCH
* upgrade valuations engine with new sources


# v1.1.4
## Jolly Fuchsia Unicorn
## 20-08-2021

* QR scanning on login page fixed
* shorten asset names in recipes
* sanitize logging messages
* improve qrtz methods: loop, jump
* swap engine: avoid continuous re-derivation of addresses
* swap engine: automatic determination of unclaimed deals
* hybrixd start script: better PID discovery and handling
* CSS improvements


# v1.0.0
## Fluffy White Kitten
## 08-08-2021

* v1.0.0 release candidate


# v0.9.26
## Cosy Indigo Kobold
## 07-08-2021

* Fix transaction details for unifieds


# v0.9.25
## Little Indigo Leprechaun
## 06-08-2021

* Safeguard close api connection
* Added GALA and GPXS token. Thanks AltcoinAdoption and Jamid Arboleda Gomez!
* add BNB USD stablecoins
* Remove biki from stats
* Implement socks header and chunk parsing
* Also confirm chains that report only true/false confirms
* Reflect amount of confirmations
* Add shortcut for identical valuations
* Added wrapped ETH and BTC tokens on Binance chain


# v0.9.24
## Furry Indigo Goblin
## 29-07-2021

* Verify eth transaction after push
* Improve error feedback for signature troubles
* web-wallet: reduce calls when viewing single asset


# v0.9.23
## Happy Indigo Ogre
## 22-07-2021

* Fixed Ripple private key import. Thanks AltcoinAdoption!
* Disallow erroneous negative fees in nownodes engine. Keep things positive!
* Enable sending of Ripple even if remote account is not activated. 
* Link node binaries in update script.
* Sort hybrix-jslib live examples in documentation
* qrtz: implement rout to other host
* web-wallet: implement universal login. Next level deterministic! 
* web-wallet: improve fee calculations for unified asset swaps
* web-wallet: fixed send asset modal not closing on error
* web-wallet: cache valuation rates
* web-wallet: allow send to deterministic offset address
* swap: Add supported ledgers endpoint
* swap: Update sufficiency calculations 
* swap: Improvements to pair rebalance methods
* hybrix-jslib: implement deterministic offset
* hybrix-jslib: update pending on refreshAsset


# v0.9.22
## Sparkling Green Hobgoblin
## 09-07-2021

* hybrix-jslib: implement universal login in session method.


# v0.9.21
## Chirpy Fuchsia Pegasus
## 29-06-2021

* Added grape token, Thanks George! Gotta catch em all!
* Implement socks connector. This enables hosting on TOR. Private and secure.
* Curl connectors are modular and initialization is asynchronous. Who you gonna call? 
* Improve cli-wallet documentation. Added (allocation) module syntax to help out aspiring allocators.
* Handle sub properties for configuration values. Tweak everything to your heart's desire
* swap: Pair liquidity overview page at /e/deal/swap
* swap: Dispute handler
* swap: Implement user defined risk per swap


# v0.9.20
## Merry Yellow Noodlefish
## 10-06-2021

* Improve conf defaults
* Update transport and evm dependencies
* Improve conf defaults
* qrtz: Warn for missing labels and fixes for missing labels
* hybrix-jslib: Improve unified transaction feedback and performance
* swap: only count volume of completed swaps
* swap: handle sufficiency at zero


# v0.9.19
## Jolly Fuchsia Unicorn
## 05-06-2021

* Update web-wallet


# v0.9.18
## Cheery Yellow Phoenix
## 05-06-2021

* Add Clever (CLVA) token. Thanks Hero!
* Catch faulty 0x0 eth gasprice
* Auto stop after 5 seconds
* Improve restart
* Upgrade ws
* Init recipe vars on error
* Purge lingering processes
* Handle missing amount in xrp transaction
* Catch unknown value in xrp transaction
* Init log folder on first startup
* qrtz: Fix void with object compare
* qrtz: parse math results to proper types where possible


# v0.9.17
## Fluffy Pink Minotaur
## 22-05-2021

* Added Radix, thanks h0ll0wstick!
* Check for Ripple (XRP) target activation balance at unspent. 
* Updates to default fee configurations.
* qrtz: Add incl method to check array values or strings include needles.
* swap: Enable request pairs of single account
* swap: Implement ledger exceptions (handle activation balance of Ripple)
* swap: Upgraded liquity availability handler, security lock and claim status


# v0.9.16
## Shiny Fuchsia Sphinx
## 13-05-2021

* Fix to boot script to accommodate to the  jungle of linux, unix, bsd and macOs shells.


# v0.9.15
## Bouncy Fuchsia Owlbear
## 07-05-2021

* Add GET and Glitch tokens Thanks JerBot and JeanBuild!!
* Add uni and sand tokens. Thanks Bill Brown!
* Unified addition: BNB.HY
* Remove TomoDEX as valuation source as it has not enough liquidity.
* Handle bnb zero balances.
* Add option to curl to ignore 404 and errors
* Adapt tomo to have a default fee-symbol
* All eth-api engine assets now check native balance on unspent
* Adjusted gas settings for Binance Smart Chain
* Update ETH-API engine to support alternative unspent routines
* Simplify push tx, transaction hash always means Ripple host will attempt tx broadcast
* Increase default XRP fee
* Fix datestamp in startup log


# v0.9.14
## Tiny Bouncy Sasquatch
## 29-04-2021

* Added BEPRO token. Thanks Mihn!
* Added SingularityNET (AGI) token. Thanks Rasoul!
* Implement graceful shutdown. Finish all tasks before stopping daemon.
* Show ascii art splash logo on startup. Make terminal pretty :)  
* Hide log stream unless output = true is defined. 
* Add Ganache (ethereum test) connectors.
* Swap deal engine: caching of pair list, extended fee calculation change
* Swap deal engine: Handle reserve unlocking and rebalancing.


# v0.9.13
## Sparkling Rainbow Caticorn
## 22-04-2021

* Added wise token. Thanks icobird!!
* Handle plain text responses in cli.
* Activated option to store by POST method.
* Improved handling of outliers in valuations engine
* Improve valuation symbol sanitation
* Extend logging for local var. Disable async writes until improvements have been made to handle SIGINT.
* Cache xrp sequence numbers locally.
* Upgraded bitcore engine to properly filter change addresses, also in returned amount.
* Refactor qrtz tests to method files.
* Swap deal engine: unlimited deferral time on push failure for accepted swap deals.
* Swap deal engine: cache derived addresses.
* qrtz: Add variableName option to void.
* hybrix-jslib: Added data option to rout method for POST requests.
* hybrix-jslib: Use POST for large save requests.
* hybrix-jslib: Improvement to unified transaction fee feedback.


# v0.9.12
## Giant Yellow Leprechaun
## 16-04-2021

* Add Polygon Network (formerly Matic) token. Thanks Thomas Maij!
* Handle Ripple minimum activation balance. As you can't leave less than 20 XRP on an account.
* Waves: increase fee amount to account for smart accounts
* Add enable conf toggle to all sources and engines
* Improvements to recipe editor


# v0.9.11
## Furry Fuchsia Goblin
## 09-04-2021

* Add mechanism to trim logs. Keep things nice and tidy.
* Provide transaction-details without target.  
* Shortcut valuations for zero. Zero is zero
* Fix missing waves amount in transactions. 
* Sort xrp history. Top to bottom
* hybrix-jslib: Define specific hosts per pending transaction type
* hybrix-jslib:  Reset hosts on logout


# v0.9.10
## Happy Fuchsia Ogre
## 06-04-2021

* Add notes to asset details. To provide specifics about assets. 
* Add Ripple (xrp) destination tag note.
* Improvements to Ripple (xrp) tokens .
* Fix unified /confirm and /confirmed endpoint.
* hybrix-jslib: Smart handling of address prefixes. 
* hybrix-jslib: Prevent passing expanded unified addresses for transaction review.
* hybrix-jslib: Fix handling errors in decoding of unified addresses.
* explorer: Error in confirmed broke entire transaction displaying. Bad UX.
* Swap deal engine: Fully verify account ids.
* Swap deal engine: Remove account.


# v0.9.9
## Sparkling Crystal Mermaid
## 25-03-2021

* Handle when a broken regex is provided: Regex is hard enough as it is.
* Improve caching: No need to retrieve everything all the time.


# v0.9.8
## Chirpy Mustard Pegasus
## 19-03-2021

* Manage your configuration through ui instead of code. Making things easier.  /c/conf
* Implement local vars for custom exec scripts. More powerful quartz scripting /c/exec
* Extend api queue and process debug. Find out what went wrong quicker. /p/debug
* Swap deal engine : Reserve deposit on swap. Lock and load!


# v0.9.7
## Merry Cyan Noodlefish
## 11-03-2021

* Add tcps support. TLS over TCP is now supported. Nice and secure.
* Refactor valuations for resilience. 
* Swap deal engine : Improvements to sufficiency calculations
* Catch non illegal mockchain contracts for testing
* HY asset icon had wrong SVG format, compared to the other icons
* Relocate Namecoin, until we get ElectrumX hosts from the community
* Fix fee calculation edge cases.
* Add event hook introspection using /c/events for debug purposes


# v0.9.6
## Jolly Yellow Unicorn
## 27-02-2021

* Improve valuation engine. Added more sources, sanitize symbols.
* Tweaks to recipe editor 
* Fix to tomo token history and unified history
* Swap deal engine: validate proposal target address
* Swap deal engine: pair volume reporting and more statistics
* Swap deal engine: added general estimate endpoint. 


# v0.9.5
## Curly Auburn Fairy
## 11-02-2021

* Implement modified fees for TOMO tokens. Tomo chains allows fees to be paid in tokens themselves. A very nice feature that we're glad to support.
* Fix bitcoin cash test address prefix parsing. Enable TEST_BCH (TBCH) bitcoin cash testnet. 
* Add feature overview to list asset details. This will enable filtering assets in based on supported assets.
* NPM Audit improvements and dependency updates. Made it easier to maintain the npm dependencies in the hybrix code.


# v0.9.3
## Shiny Sapphire Sphinx
## 05-02-2021

* Asset features endpoint. Use /asset/SYMBOL/features to see which features an asset supports.
* Fixed: eth token fees use fee-factor precision.


# v0.9.2
## Bouncy Cyan Elf
## 30-01-2021

* Fix transaction-details for unified assets. Determine what amount you can transfer from complex unified addresses.
* Update download links and release locations to download.hybrix.io. A one stop shop for all your hybrix downloads.
* hybrix-jslib: Add encryption by default option to rout and addHost methods.
* hybrix-jslib: Add specialized hosts. These will only be used when specified.


# v0.9.1
## Fluffy White Gremlin
## 12-01-2021

* Enable inter module communication by safely exposing specific methods.
* Improve custom configuration: (partial) recipes in var/recipes/delta overwrite the base recipes
* Fix bug that prevented hybrixd cli command to connect to other hosts
* Allow higher NodeJS versions.
* qrtz: added hook without parameters to clear/reset hook
* hybrix-jslib: Track pending swap deals
* hybrix-jslib: Improve unified asset balance refresh


# v0.9.0
## Cosy Cyan Kobold
## 08-01-2021

* Modules for cross-ledger value swapping and allocation. 
* Automatic Ethereum nonce caching. Submit multiple transactions even when network is slow.
* Add nowNodes API connector.
* Add artificial delays to mockchain confirms for testing purposes.
* Conf overview. Check your node configuration at /c/conf.
* Improve module error feedback. Full stack trace to help with the bug hunting.
* Decode Ethereum transactions. 
* Add valuations for mockchain test coins.
* Limit logging of long routing request to prevent log clogging.
* hybrix-jslib: upgraded to webpack 5.
* hybrix-jslib: track pending transactions. Keep an overview of your pending transactions.
* hybrix-jslib: update session checks before methods.
* qrtz: bank method : secure handeling and delegated handling of transactions.
* qrtz: multi with : do more with less code.
* qrtz: you can now use tran to transform object values or keys.
* qrtz: form and atom by symbol. Easier to make prettier numbers.
* qrtz: multi step math. Handle complex math in multiple steps.
* qrtz: root method. Run code based on whether user has root access.


# v0.8.9
## Cosy Violet Sprite
## 26-11-2020

* Do not use btc unconfirmed balances as they provided wrong results. Thanks to mehrdd!
* qrtz: code browser. Browse to process/code on your local node and view and debug the qrtz 'api as code' methods.
* Add option to delay scheduled jobs for all you procrastinators
* Updated hy icon. New, nice and spiffy!
* Add cli quick test endpoint for assets /asset/SYMBOL/test-cli. 
* hybrix-jslib: add more hashing and encryption options.
* hybrix-jslib: add getBalance method and implement throttle for refreshAsset.
* hybrix-jslib: add file storage connector. Synchronize local and storages for nodejs projects.


# v0.8.8
## Little Violet Leprechaun
## 17-11-2020

* Add spends  to transaction. See all different target addresses in a multi spend transaction in /a/SYMBOL/transaction.
* Get the spend for a specific target with /a/SYMBOL/transaction/TARGET.
* Prevent duplicate default steps which could make processes slow. Made them mean and lean again.
* Remove broken zcash (ZEC) host
* explorer: Add value conversion method to search bar. 


# v0.8.7
## Furry Violet Goblin
## 11-11-2020

* Loading data from storage would fail if meta data was corrupt. Bad, made this more resilient.


# v0.8.6
## Happy Violet Ogre
## 06-11-2020

* Update bitcore engine
* Add bitcoin and bitcoincash testnet connectors. Test all the things!
* Improve valuation samples and api help copy


# v0.8.5
## Sparkling Turqoise Mermaid
## 27-10-2020

* Implement btc using bitcore engine. New and improved!
* handle prefixless unified asset validation.  #unify!
* Improve storage tests. Failing ain't bad, as long as you know what to do better.
* qrtz: skip empty lines instead of crashing, which is nice.


# v0.8.4
## Fluffy Magenta Pegasus
## 25-09-2020

* Use cached as fallback for history and improve hook
* Pass timeout to all child and parent processes
* transport: ignore bad requests
* transport: remove any characters that are not properly routa

#!/bin/sh
OLDPATH="$PATH"
WHEREAMI="`pwd`"

# $HYBRIXD/node/scripts/npm  => $HYBRIXD
SCRIPTDIR="`dirname \"$0\"`"
HYBRIXD="`cd \"$SCRIPTDIR/../../..\" && pwd`"

NODE="$HYBRIXD/node"
DETERMINISTIC="$HYBRIXD/deterministic"
NODEJS="$HYBRIXD/nodejs"
COMMON="$HYBRIXD/common"
INTERFACE="$HYBRIXD/hybrix-lib"
WEB_WALLET="$HYBRIXD/web-wallet"

export PATH=$NODE/node_binaries/bin:"$PATH"

cd "$NODE/scripts/docs"
node "$NODE/scripts/docs/docs.js"

node "$NODE/scripts/docs/conf.js"

node "$NODE/scripts/docs/cli.js" "$HYBRIXD/cli-wallet"



cd "$WHEREAMI"
export PATH="$OLDPATH"

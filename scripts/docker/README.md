# base-image

Run hybrixd using docker. Host your own wallet, be your own bank and have a powerful multi-blockchain system at your fingertips.

`docker run --entrypoint "/bin/bash" -it docker.pkg.github.com/hybrix-io/hybrixd/hybrixd:latest -p 1111:1111 -p 8080:8080 -p 8090:8090`

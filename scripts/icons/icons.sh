#!/bin/sh
WHEREAMI=`pwd`
OLDPATH="$PATH"

SCRIPTDIR="`dirname \"$0\"`"
HYBRIXD="`cd \"$SCRIPTDIR/../../..\" && pwd`"
NODE="$HYBRIXD/node"

export PATH="$WHEREAMI/node_binaries/bin:$PATH"

#INTERFACE="$HYBRIXD/hybrix-lib"
#DETERMINISTIC="$HYBRIXD/deterministic"
#NODEJS="$HYBRIXD/nodejs"
#COMMON="$HYBRIXD/common"
#INTERFACE="$HYBRIXD/interface"
#WEB_WALLET="$HYBRIXD/web-wallet"

cd "$NODE/scripts/icons"
node "$NODE/scripts/icons/icons.js"

cd "$WHEREAMI"
export PATH="$OLDPATH"

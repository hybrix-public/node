#!/bin/sh
WHEREAMI="`pwd`";
OLDPATH="$PATH"

# $NODE/scripts/npm  => $NODE
SCRIPTDIR="`dirname \"$0\"`"
NODE="`cd \"$SCRIPTDIR/../..\" && pwd`"
NODEMODULES="$NODE/../node_modules"

export PATH="$NODE/node_binaries/bin:$PATH"

cd "$NODE"
echo "[i] Auditing hybrixd components..."
cd "$NODEMODULES"
#npm i
#npm update
npm audit

cd "$NODE/modules"

for D in *; do
    if [ -d "${D}" ] && [ -e "$D/package.json" ] && [ -e "$D/node_modules" ]; then
        echo "[.] Checking hybrixd:${D}..."
        cd ${D}
        npm i
        npm update
        npm audit fix --force
        cd ..
    fi
done

export PATH="$OLDPATH"
cd "$WHEREAMI"

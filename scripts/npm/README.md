# NPM scripts for hybrixd

## Overview
This directory contains scripts that can be run to setup, test and compile hybrixd.

All scripts can be invoked as follows:

```
npm run $SCRIPTNAME
```
  or from the root of the repository
```
./scripts/npm/npm.sh run $SCRIPTNAME
```

## Functionality of the scripts
 - setup:   Set up hybrixd in a development environment, by symlinking to other repositories.
 - compile: Compile a release version of hybrixd in the directory ./dist .
 - test:    Run all testing scripts.
 - audit:   Attempt to automatically audit all necessary NPM dependencies.
 - update:  Update a release version of hybrixd.


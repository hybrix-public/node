#!/bin/sh
WHEREAMI=`pwd`
OLDPATH=$PATH

# $HYBRIXD/$NODE/scripts/npm  => $HYBRIXD
SCRIPTDIR="`dirname \"$0\"`"

HYBRIXD="`cd \"$SCRIPTDIR/../../..\" && pwd`"
COMMON="$HYBRIXD/common"
NODEJS="$HYBRIXD/nodejs"
NODE="$HYBRIXD/node"
INTERFACE="$HYBRIXD/hybrix-lib"
DETERMINISTIC="$HYBRIXD/deterministic"
WEBWALLET="$HYBRIXD/web-wallet"

URL_DETERMINISTIC="https://gitlab.com/hybrix-public/deterministic.git"
URL_INTERFACE="https://gitlab.com/hybrix-public/hybrix-lib.git"
URL_WEBWALLET="https://gitlab.com/hybrix-public/clients/web-wallet.git"
URL_COMMON="https://gitlab.com/hybrix-public/common.git"
URL_NODEJS="https://gitlab.com/hybrix-public/dependencies/nodejs.git"
#URL_NODE_MODULES="https://gitlab.com/hybrix-public/dependencies/node_modules.git"

if [ -e "$HYBRIXD/hybrixd" ]; then
    ENVIRONMENT="public"
    echo "[i] Environment is public..."
elif [ -e "$HYBRIXD/node" ]; then
    ENVIRONMENT="dev"
    echo "[i] Environment is development..."
else
    echo "[!] Unknown environment"
    export PATH="$OLDPATH"
    cd "$WHEREAMI"
    exit 1
fi

if [ "`uname`" = "Darwin" ]; then
    SYSTEM="darwin-x64"
elif [ "`uname -m`" = "i386" ] || [ "`uname -m`" = "i686" ] || [ "`uname -m`" = "x86_64" ]; then
    SYSTEM="linux-x64"
elif [ "`uname -m`" = "arm64" ] || [ "`uname -m`" = "aarch64" ]; then
    SYSTEM="linux-arm64"
else
    echo "[!] Unknown architecture (or incomplete implementation)"
    export PATH="$OLDPATH"
    cd "$WHEREAMI"
    exit 1;
fi

# NODE
if [ "$ENVIRONMENT" = "public" ]; then
    if [ ! -e "$HYBRIXD/node" ];then
        echo "[.] linking hybrixd to node folder."
        ln -sf "$HYBRIXD/hybrixd" "$HYBRIXD/node"
    else
        echo "[i] node folder exists."
    fi
fi

# NODE_BINARIES
if [ ! -e "$NODE/node_binaries" ];then

    echo "[!] $NODE/node_binaries not found"

    if [ ! -e "$NODEJS" ];then
        cd "$HYBRIXD"
        echo "[.] Clone node js runtimes files"
        git clone "$URL_NODEJS"
    fi
    echo "[.] Link node_binaries"
    ln -sf "$NODEJS/$SYSTEM" "$NODE/node_binaries"
fi

export PATH="$NODE/node_binaries/bin:$PATH"

# COMMON
if [ ! -e "$NODE/common" ];then

    echo "[!] $NODE/common not found"

    if [ ! -e "$COMMON" ];then
        cd "$HYBRIXD"
        echo "[.] Clone common files"
        git clone "$URL_COMMON"
    fi
    echo "[.] Link common files"
    ln -sf "$COMMON" "$NODE/common"
fi

# HYBRIX-LIB INTERFACE
if [ ! -e "$NODE/interface" ];then

    echo "[!] $NODE/interface not found"

    if [ ! -e "$INTERFACE" ];then
        cd "$HYBRIXD"
        echo "[.] Clone interface files"
        git clone "$URL_INTERFACE"
    fi
    echo "[.] Link hybrix-lib interface files"
    ln -sf "$INTERFACE/dist" "$NODE/interface"
fi

# DETERMINISTIC MODULE
if [ ! -e "$NODE/modules/deterministic" ];then

    echo "[!] $NODE/modules/deterministic not found"

    if [ ! -e "$DETERMINISTIC" ];then
        cd "$HYBRIXD"
        echo "[.] Clone deterministic files"
        git clone "$URL_DETERMINISTIC"
    fi
    echo "[.] Link deterministic files"
    ln -sf "$DETERMINISTIC/dist" "$NODE/modules/deterministic"
fi

# WEB-WALLET MODULE
if [ ! -e "$NODE/modules/web-wallet" ];then

    echo "[!] $NODE/modules/web-wallet not found"

    if [ ! -e "$WEBWALLET" ];then
        cd "$HYBRIXD"
        echo "[.] Clone web-wallet files"
        git clone "$URL_WEBWALLET"
        mkdir "$WEBWALLET/dist"
    fi
    echo "[.] Link web-wallet files"
    ln -sf "$WEBWALLET/dist" "$NODE/modules/web-wallet"
fi

# NODE_MODULES
#if [ "$ENVIRONMENT" = "public" ]; then
#    read -p "[?] Do you wish to use the supported node_modules from hybrix? [y/n] " CONFIRM
#
#    if [ "$CONFIRM" = "n" ]; then
#        USE_SUPPORTED=false
#    else
#        USE_SUPPORTED=true
#    fi
#else
#    USE_SUPPORTED=true
#fi
#
#echo "[i] Clear node_modules"
#rm -rf "$NODE/node_modules"
#
#if [ "$USE_SUPPORTED" = true ]; then
#
#    if [ ! -e "$HYBRIXD/node_modules" ];then
#        cd "$HYBRIXD"
#        echo "[.] Clone node_modules dependencies"
#        git clone "$URL_NODE_MODULES"
#    fi
#
#    echo "[.] Link node_modules"
#    ln -sf "$HYBRIXD/node_modules" "$NODE/node_modules"
#else
#    echo "[.] Downloading dependencies from NPM."
#    cd "$NODE"
#    npm install
#fi

# DEPRECATED: GIT HOOKS
#sh "$COMMON/hooks/hooks.sh" "$NODE"

export PATH="$OLDPATH"
cd "$WHEREAMI"

echo "[+] Note: Installing tor will enable your node to connect to other hybrix"
echo "          nodes and swaps even when they are firewalled. This is highly"
echo "          recommended for general usage."
echo "          If you would like to use the web-wallet, please cd to the wallet"
echo "          directory and do setup there, and then compile the wallet before"
echo "          starting hybrixd. When all is set, run ./hybrixd to start hybrix."
echo "[i] All done"


#!/bin/bash
OLDPATH="$PATH"
WHEREAMI="`pwd`"

# $HYBRIXD/node/scripts/npm  => $HYBRIXD
SCRIPTDIR="`dirname \"$0\"`"
HYBRIXD="`cd \"$SCRIPTDIR/../../..\" && pwd`"

NODE="$HYBRIXD/node"
DIST="$NODE/dist"
WEBWALLET="$HYBRIXD/web-wallet"

export PATH="$NODE/node_binaries/bin:$PATH"
NPMINST="`which npm`"
#NPMINST="$NODE/scripts/npm/npm.sh"

CURRENT_WEBWALLET_VERSION=$(cat "$WEBWALLET/package.json" \
  | grep version \
  | head -1 \
  | awk -F: '{ print $2 }' \
  | sed 's/[",]//g' \
  | tr -d '[[:space:]]')

CURRENT_NODE_VERSION=$(cat "$NODE/package.json" \
  | grep version \
  | head -1 \
  | awk -F: '{ print $2 }' \
  | sed 's/[",]//g' \
  | tr -d '[[:space:]]')

MONSTERNAME="`node $SCRIPTDIR/nameRelease.js $CURRENT_NODE_VERSION`"

echo
echo "Node hybrixd version:   $CURRENT_NODE_VERSION ($MONSTERNAME)"
echo "Web-wallet version:     $CURRENT_WEBWALLET_VERSION"
echo
read -n 1 -p "About to edit changelog. Press CTRL-C now if you want to cancel!"

cat <<- EOF > /tmp/CHANGELOG.md
# v$CURRENT_NODE_VERSION
## $MONSTERNAME

`git log --pretty="* %s" | head -n 25`



EOF

cat $NODE/CHANGELOG.md >> /tmp/CHANGELOG.md
nano /tmp/CHANGELOG.md
read -n 1 -p "Changelog editing done. About to commit. Press CTRL-C now if you want to cancel!"

# Prepare and commit release
git checkout main
git pull
mv /tmp/CHANGELOG.md $NODE/CHANGELOG.md
git checkout -b release-v$CURRENT_NODE_VERSION
git commit $NODE/package.json $NODE/CHANGELOG.md -m "release v$CURRENT_NODE_VERSION"
git push --set-upstream origin release-v$CURRENT_NODE_VERSION
read -n 1 -p "Version commit done. Now merge it on Gitlab, then continue. Press CTRL-C now if you want to cancel!"
git checkout main
git pull

# We're done
echo " [i] All done.   To build release distribution: ./buildRelease"
echo


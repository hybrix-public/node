#!/bin/bash
OLDPATH="$PATH"
WHEREAMI="`pwd`"

# $HYBRIXD/node/scripts/npm  => $HYBRIXD
SCRIPTDIR="`dirname \"$0\"`"
HYBRIXD="`cd \"$SCRIPTDIR/../../..\" && pwd`"

NODE="$HYBRIXD/node"
DIST="$NODE/dist"
WEBWALLET="$HYBRIXD/web-wallet"

export PATH="$NODE/node_binaries/bin:$PATH"
NPMINST="`which npm`"
#NPMINST="$NODE/scripts/npm/npm.sh"

CURRENT_WEBWALLET_VERSION=$(cat "$WEBWALLET/package.json" \
  | grep version \
  | head -1 \
  | awk -F: '{ print $2 }' \
  | sed 's/[",]//g' \
  | tr -d '[[:space:]]')

CURRENT_NODE_VERSION=$(cat "$NODE/package.json" \
  | grep version \
  | head -1 \
  | awk -F: '{ print $2 }' \
  | sed 's/[",]//g' \
  | tr -d '[[:space:]]')

MONSTERNAME="`node $SCRIPTDIR/nameRelease.js $CURRENT_NODE_VERSION`"

prepareTarget () {
  target="$1"
  echo
  echo
  echo " [i] Preparing to build $target..."
  echo
  if [ -d "$HYBRIXD/$target" ]; then
    cd "$HYBRIXD/$target"
    git checkout main > /dev/null 2>&1
    if [ $? -eq 0 ]; then
      echo " [.] checked out main branch of $target"
    else
      echo " [!] could not checkout main branch of $target!"
      exit 1
    fi
  else
    echo " [!] could not find $target repository directory! (code $?)"
    exit 1
  fi
}

# Prepare and compile distribution
prepareTarget "deterministic"
echo " [.] Building deterministic"
$NPMINST run build
prepareTarget "hybrix-lib"
echo " [.] Building hybrix-lib interface"
$NPMINST run build
prepareTarget "web-wallet"
echo " [.] Building web-wallet"
$NPMINST run build

# Finalize release
echo
echo " [.] Building node distribution"
prepareTarget "node"
$NPMINST run compile
cd "$HYBRIXD/node"
git checkout - > /dev/null 2>&1
echo

# We're done
echo " [i] All done.   To upload distribution: ./uploadRelease.sh"
echo


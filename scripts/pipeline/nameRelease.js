/*
 * return a monster name to fit the version
 */

const monstername = (major, minor, patch) => {
  // descriptive adjectives, colors and monsters - https://study.com/learn/lesson/mythical-creatures-list.html
  const adjectives = ['Happy', 'Furry', 'Little', 'Cosy', 'Fluffy', 'Bouncy', 'Shiny', 'Curly', 'Cheery', 'Jolly', 'Merry', 'Chirpy', 'Sparkling'];
  const colors = ['Cyan', 'Fuchsia', 'Indigo', 'Ivory', 'Khaki', 'Lavender', 'Lime', 'Magenta', 'Maroon', 'Pink', 'Purple', 'Sienna', 'Teal', 'Turqoise', 'Violet'];
  const monsters = ['Ogre', 'Goblin', 'Leprechaun', 'Kobold', 'Gremlin', 'Elf', 'Sphinx', 'Griffin', 'Fairy', 'Phoenix', 'Unicorn', 'Noodlefish', 'Pegasus', 'Mermaid', 'Pixie', 'Kraken', 'Chupacabra', 'Minotaur', 'Dragon', 'Monopod', 'Leyak', 'Churel', 'Tengu', 'Nopperabo', 'Vampire', 'Ghost', 'Manticore', 'Changeling', 'Idris', 'Hakuturi', 'Dryad', 'Knocker', 'Satyr', 'Kelpie', 'Ogopogo', 'Sylph'];

  const n = major * 2531 + minor * 607 + patch;
  const name = adjectives[n % adjectives.length] + ' ' + colors[Math.floor(n / adjectives.length) % colors.length] + ' ' + monsters[Math.floor(n / adjectives.length * colors.length) % monsters.length];

  return name;
}

const args = process.argv.slice(2);
const v = args[0].split('.');

console.log(monstername(v[0],v[1],v[2]));

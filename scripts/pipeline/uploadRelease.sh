#!/bin/bash
OLDPATH="$PATH"
WHEREAMI="`pwd`"

# $HYBRIXD/node/scripts/npm  => $HYBRIXD
SCRIPTDIR="`dirname \"$0\"`"
HYBRIXD="`cd \"$SCRIPTDIR/../../..\" && pwd`"

NODE="$HYBRIXD/node"
DIST="$NODE/dist"
WEBWALLET="$HYBRIXD/web-wallet"

export PATH="$NODE/node_binaries/bin:$PATH"
NPMINST="`which npm`"

CURRENT_WEBWALLET_VERSION=$(cat "$WEBWALLET/package.json" \
  | grep version \
  | head -1 \
  | awk -F: '{ print $2 }' \
  | sed 's/[",]//g' \
  | tr -d '[[:space:]]')

CURRENT_NODE_VERSION=$(cat "$NODE/dist/package.json" \
  | grep version \
  | head -1 \
  | awk -F: '{ print $2 }' \
  | sed 's/[",]//g' \
  | tr -d '[[:space:]]')

cd $NODE/dist

echo " [.] Compressing distribution into dist.zip ..."
cd $DIST
zip -q -r ../dist.zip .

echo " [.] Compressing distribution into dist.tar.gz ..."
cd $DIST
tar -czf ../dist.tar.gz .

cd $NODE
TARGETHOST="service@download.hybrix.io"
TARGETPATH="~/public_html/releases/hybrixd"
ssh $TARGETHOST "mkdir -p $TARGETPATH/latest ; mkdir -p $TARGETPATH/v$CURRENT_NODE_VERSION"

echo " [.] Uploading distribution to v$CURRENT_NODE_VERSION ..."
TARGETFILEA="hybrixd.v$CURRENT_NODE_VERSION.zip"
scp dist.zip $TARGETHOST:$TARGETPATH/v$CURRENT_NODE_VERSION/$TARGETFILEA

TARGETFILEB="hybrixd.v$CURRENT_NODE_VERSION.tar.gz"
scp dist.tar.gz $TARGETHOST:$TARGETPATH/v$CURRENT_NODE_VERSION/$TARGETFILEB

echo " [.] Setting v$CURRENT_NODE_VERSION as latest..."
ssh $TARGETHOST "cp $TARGETPATH/v$CURRENT_NODE_VERSION/$TARGETFILEA $TARGETPATH/latest/hybrixd.latest.zip ; cp $TARGETPATH/v$CURRENT_NODE_VERSION/$TARGETFILEB $TARGETPATH/latest/hybrixd.latest.tar.gz"

echo " [.] Cleaning up"
rm "$NODE/dist.zip"
rm "$NODE/dist.tar.gz"

echo " [i] All done!   Go upgrade live instances. Then let the world know."
echo "                 To fire off social media: ./socialsRelease.sh"
echo

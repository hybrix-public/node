let packageObject;

const products = {
  '6980168': {name: 'hybrixd', path: 'hybrix/hybrixd/node'},
  '8345870': {name: 'hybrix-jslib', path: 'hybrix/hybrixd/interface'},
  '9166263': {name: 'cli-wallet', path: 'hybrix/hybrixd/client/implementations/cli-wallet'}
};

const markAsDone = (id, href) => {
  const e = document.getElementById(id);
  if (e) {
    e.classList.add('done');
    if (e.nextElementSibling && href) {
      e.nextElementSibling.setAttribute('href', href);
    }
  }
};

const markAsFailed = (id, href, error) => {
  const e = document.getElementById(id);
  if (e) {
    console.error(id, error);
    e.classList.add('failed');
    if (e.nextElementSibling && href) {
      e.nextElementSibling.setAttribute('href', href);
    }
  }
};

const writeMode = () => {
  document.getElementById('previewMode').classList.remove('active');
  document.getElementById('writeMode').classList.add('active');

  document.getElementById('previewMode').style.display = 'inline';
  document.getElementById('writeMode').style.display = 'inline';

  document.getElementById('releaseNotes').style.display = 'block';
  document.getElementById('preview').style.display = 'none';
};

const previewMode = () => {
  const notes = document.getElementById('releaseNotes').value;
  document.getElementById('releaseNotes').style.display = 'none';
  document.getElementById('preview').style.display = 'block';
  document.getElementById('previewMode').classList.add('active');
  document.getElementById('writeMode').classList.remove('active');

  const lines = notes.split('\n');
  let html = '';
  let olOpen = false;
  let ulOpen = false;
  for (let i = 0; i < lines.length; ++i) {
    const line = lines[i].trim();
    if (line.startsWith('##')) {
      if (olOpen) { html += '</ol>'; olOpen = false; }
      html += '<h2>' + line.substr(2) + '</h2>';
    } else if (line.startsWith('#')) {
      if (olOpen) { html += '</ol>'; olOpen = false; }
      html += '<h1>' + line.substr(2) + '</h1>';
    } else if (line.startsWith('1.')) {
      if (!olOpen) { html += '<ol>'; olOpen = true; }
      if (ulOpen) {
        html += '</ul></li>';
      }
      html += '<li>';
      html += '<ul>';
      ulOpen = true;
    } else if (line.startsWith('*')) {
      html += '<li>' + line.substr(1) + '</li>';
    }
  }
  if (olOpen) { html += '</ol>'; olOpen = false; }
  document.getElementById('preview').innerHTML = html;
};

const releaseSelect = (myUserName, myUserId, userName, userId, weekNumber, month, year) => {
  return '';
};

function actionMerge (projectId, newCHANGELOGMD, mergeRequest, currentVersion, newVersion) {
  request('PUT', '/projects/' + projectId + '/merge_requests/' + mergeRequest.iid + '/merge?remove_source_branch=true&squash_commit_message=true', merge => {
    markAsDone('stepMerge', 'https://gitlab.com/' + products[projectId].path + '/-/merge_requests/' + mergeRequest.iid);
    document.getElementById('pipeline').href = 'https://gitlab.com/' + products[projectId].path + '/pipelines';
    document.getElementById('stepProduction').classList.add('manual');

    const onlyNewestCHANGELOGMD = newCHANGELOGMD.substr(0, newCHANGELOGMD.indexOf('# v' + currentVersion));

    const tagData = '{"tag_name":"v' + newVersion + '","ref":"' + merge.sha + '", "release_description":"' + onlyNewestCHANGELOGMD + '"}';

    request('POST', '/projects/' + projectId + '/releases?tag_name=v' + newVersion + '&ref=' + merge.sha, tag => {
      markAsDone('stepTag', 'https://gitlab.com/' + products[projectId].path + '/-/tags/v' + newVersion);

      document.getElementById('save').onclick = () => {
        window.open('https://gitlab.com/' + products[projectId].path + '/pipelines', '_blank');
      };

      document.getElementById('save').classList.remove('modified');
      document.getElementById('save').classList.add('manual');
      document.getElementById('currentVersion').style.color = 'white';
      document.getElementById('currentVersion').classList.remove('modified');
      document.getElementById('currentVersion').classList.add('updated');
    }, undefined, tagData);
  },
  error => markAsFailed('stepMerge', 'https://gitlab.com/' + products[projectId].path + '/-/merge_requests/' + mergeRequest.iid, error)
  );
}

const checkApprovalStatus = (projectId, newCHANGELOGMD, mergeRequest, currentVersion, newVersion) => () => {
  // TODO mark as busy
  request('GET', '/projects/' + projectId + '/merge_requests/' + mergeRequest.iid + '/approval_state', approvals => {
    console.log('approvals', approvals); // TODO
    if (approvals.rules[0] === undefined || approvals.rules[0].approved) {
      actionMerge(projectId, newCHANGELOGMD, mergeRequest, currentVersion, newVersion);
    } else {
      setTimeout(checkApprovalStatus(projectId, newCHANGELOGMD, mergeRequest, currentVersion, newVersion), 5000);
    }
  },
  error => markAsFailed('stepMerge', 'https://gitlab.com/' + products[projectId].path + '/-/merge_requests/' + mergeRequest.iid, error),
  undefined /* data */, false /* skipParsing */, true /* noCache */
  );
};

function actionCommit (projectId, newCHANGELOGMD, currentVersion, newVersion, commitData) {
  request('POST', '/projects/' + projectId + '/repository/commits', (commit) => {
    markAsDone('stepPackageJson', 'https://gitlab.com/' + products[projectId].path + '/blob/v' + newVersion + '/package.json');
    markAsDone('stepChangeLogMd', 'https://gitlab.com/' + products[projectId].path + '/blob/v' + newVersion + '/CHANGELOG.md');
    markAsDone('stepReleaseBranch', 'https://gitlab.com/' + products[projectId].path + '/tree/v' + newVersion);

    request('POST', '/projects/' + projectId + '/merge_requests?title=v' + newVersion + '&source_branch=v' + newVersion + '&target_branch=master&should_remove_source_branch=true&squash_commit_message=true', mergeRequest => {
      markAsDone('stepMergeRequest', 'https://gitlab.com/' + products[projectId].path + '/-/merge_requests/' + mergeRequest.iid);

      setTimeout(checkApprovalStatus(projectId, newCHANGELOGMD, mergeRequest, currentVersion, newVersion), 5000);
    },
    error => markAsFailed('stepMergeRequest', undefined, error)

    );
  },
  error => {
    markAsFailed('stepPackageJson', undefined, error);
    markAsFailed('stepChangeLogMd', undefined, error);
    markAsFailed('stepReleaseBranch', undefined, error);
  },

  commitData);
}

const renderSubRelease = (projectId, myUserName, myUserId, userName, userId, weekNumber, month, year) => {
  let r = '';

  r += '<table id="issuesTable" class="releaseTable">';
  r += '<tr><td colspan="9">';
  r += '<b>Current Version</b>: <span id="currentVersion">?</span><br/><br/>';

  r += '<span  id="major" class="button">Major</span> <span id="minor" class="button">Minor</span> <span id="patch" class="button">Patch</span>';
  r += '<br/><br/><ul>';
  r += '<span class="tick" id="stepPackageJson"></span> <a target="_blank">Update package.json</a><br/>';
  r += '<span class="tick" id="stepChangeLogMd"></span>  <a target="_blank">Update CHANGELOG.md</a><br/>';
  r += '<span class="tick" id="stepReleaseBranch"></span> <a target="_blank">Create a release branch</a><br/>';
  r += '<span class="tick" id="stepMergeRequest"></span>  <a target="_blank">Create a merge request</a> (Approve only)<br/>';

  r += '<span class="tick" id="stepMerge"></span> Merge release branch into master<br/>';
  r += '<span class="tick" id="stepTag"></span> Add tag to merge commit<br/>';

  r += '<span class="tick" id="stepProduction"></span> <a target="_blank"href="https://gitlab.com/' + products[projectId].path + '/pipelines" id="pipeline" target="_blank">Deploy to production...</a><br/>';

  // r+='<li><span id="stepLatestPackage">TODO create hybrixd.node.latest-stable.zip in dist </span></li>';
  //  r+='<li><span id="stepProduction">TODO Update production </span></li>';

  r += '</ul><br/><br/>';
  r += '<b>Release notes</b>';
  r += '<span class="mode active" style="display:none;" onclick="writeMode();" id="writeMode">Write</span> <span onclick="previewMode();" style="display:none;" class="mode" id="previewMode">Preview</span>';
  r += '<textarea style="display:none; height:500px;" id="releaseNotes" placeholder="Release Notes"></textarea>';
  r += '<div id="preview"></div>';

  r += '</td>';
  r += '</tr>';
  // GET /projects/:id/repository/files/:file_path

  r += '</table>';
  r += '<div id="save" class="save"></div>';

  document.getElementById('view').innerHTML = r;

  request('GET', '/projects/' + projectId + '/repository/files/package.json/raw?ref=master',
    data => {
      packageObject = data;
      const element = document.getElementById('currentVersion');
      if (element) {
        element.innerHTML = packageObject.version;
      }
    });

  request('GET', '/projects/' + projectId + '/repository/files/CHANGELOG.md/raw?ref=master',
    changeLog => {
      const element = document.getElementById('releaseNotes');
      if (element) {
        element.innerHTML = changeLog;
        previewMode();
      }
    }, undefined, null, true);

  const update = (major, minor, patch) => {
    window.onbeforeunload = () => 'Do you want to close?';

    const adjectives = ['Happy', 'Furry', 'Little', 'Cosy', 'Fluffy', 'Bouncy', 'Shiny', 'Curly', 'Cheery', 'Jolly', 'Merry', 'Chirpy', 'Sparkling'];
    const colors = ['Cyan', 'Fuchsia', 'Indigo', 'Ivory', 'Khaki', 'Lavender', 'Lime', 'Magenta', 'Maroon', 'Pink', 'Purple', 'Sienna', 'Teal', 'Turqoise', 'Violet'];
    const monsters = ['Ogre', 'Goblin', 'Leprechaun', 'Kobold', 'Gremlin', 'Elf', 'Sphinx', 'Griffin', 'Fairy', 'Phoenix', 'Unicorn', 'Noodlefish', 'Pegasus', 'Mermaid', 'Pixie'];

    document.getElementById('major').style.display = 'none';
    document.getElementById('minor').style.display = 'none';
    document.getElementById('patch').style.display = 'none';

    const n = major * 2531 + minor * 607 + patch;
    const name = adjectives[n % adjectives.length] + ' ' + colors[Math.floor(n / adjectives.length) % colors.length] + ' ' + monsters[Math.floor(n / adjectives.length * colors.length) % monsters.length];

    const currentVersion = document.getElementById('currentVersion').innerHTML;

    request('GET', '/projects/' + projectId + '/repository/tags/v' + currentVersion, tag => { // Get the tag for this version
      request('GET', '/projects/' + projectId + '/repository/commits?since=' + tag.commit.created_at, commits => {
        const newVersion = [major, minor, patch].join('.');
        const releaseNotes = document.getElementById('releaseNotes');

        let r = '# v' + newVersion + '\n';
        if (products[projectId].name === 'hybrixd') { // only hybrixd gets a name
          r += '## ' + name + '\n';
        }
        r += '## ' + moment().format('DD-MM-YYYY') + '\n\n';

        for (let commit of commits) {
          if (!commit.title.startsWith('Merge ')) {
            r += '* ' + commit.title + '\n';
          }
        }

        r += '\n\n\n\n' + releaseNotes.value;

        releaseNotes.value = r;
        writeMode();

        document.getElementById('save').classList.add('modified');
        document.getElementById('currentVersion').classList.add('modified');
        document.getElementById('currentVersion').innerHTML = newVersion;

        document.getElementById('save').onclick = () => {
          packageObject.version = newVersion;
          const newPackageJSON = JSON.stringify(packageObject, null, 2).replace(/"/g, '\\"').replace(/\n/g, '\\n');
          const newCHANGELOGMD = releaseNotes.value.replace(/"/g, '\\"').replace(/\n/g, '\\n');

          const commitData = '{"start_branch":"master","branch": "v' + newVersion + '","commit_message": "Release Version ' + newVersion + '","actions": [{"action": "update","file_path": "package.json","content": "' + newPackageJSON + '"},{"action": "update","file_path": "CHANGELOG.md","content": "' + newCHANGELOGMD + '"}]}';

          previewMode();
          document.getElementById('previewMode').style.display = 'none';
          document.getElementById('writeMode').style.display = 'none';

          actionCommit(projectId, newCHANGELOGMD, currentVersion, newVersion, commitData);
        };
      });
    },
    (e, m) => { alert('Could not found tag'); }
    );
  };
  document.getElementById('patch').onclick = () => {
    const version = document.getElementById('currentVersion').innerHTML;
    const versions = version.split('.');
    const major = Number(versions[0]);
    const minor = Number(versions[1]);
    const patch = Number(versions[2]) + 1;
    update(major, minor, patch);
  };
  document.getElementById('minor').onclick = () => {
    const version = document.getElementById('currentVersion').innerHTML;
    const versions = version.split('.');
    const major = Number(versions[0]);
    const minor = Number(versions[1]) + 1;
    const patch = 0;
    update(major, minor, patch);
  };
  document.getElementById('major').onclick = () => {
    const version = document.getElementById('currentVersion').innerHTML;
    const versions = version.split('.');
    const major = Number(versions[0]) + 1;
    const minor = 0;
    const patch = 0;
    update(major, minor, patch);
  };
};

const renderRelease = (myUserName, myUserId, userName, userId, weekNumber, month, year) => {
  let r = '<select id="selectProduct"><option value="" disabled selected>select product</option>';

  for (let projectId in products) {
    r += `<option value="${projectId}">${products[projectId].name}</option>`;
  }
  r += '</select>';
  document.getElementById('view').innerHTML = r;

  document.getElementById('selectProduct').onchange = () => {
    const projectId = document.getElementById('selectProduct').value;
    renderSubRelease(projectId, myUserName, myUserId, userName, userId, weekNumber, month, year);
  };

  let h = '';
  h += renderNavigation('release', myUserName);
  h += '<span class="divider">|</span>';
  h += '<span class="menu-label">quick links:</span>';
  h += renderLinks(myUserName);
  document.getElementById('quicklinks').innerHTML = h;

  activateNavigation('release', myUserName, myUserId, userName, userId, weekNumber, month, year);
};

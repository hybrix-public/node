/*
 * hybrix DAO - multi-ledger distributed autonomous organization
 * first example of a hybrix application that can render both in
 * wallet, cli-wallet as well as a normal browser tab
 *
 */

function startApplication (context,properties) {
  switch (properties.modal) {
    case 'vote':
      const DIV_content = document.getElementById('app-modal-content');
      const DIV_buttons = document.getElementById('app-modal-buttons');
      DIV_content.innerHTML = `<p>Supportive voting of <b>"${properties.voteObject.title}"</b> takes place by sending HY to its voting address. Please enter your desired amount of votes below. Every HY token is worth one vote.</p>
      <form class="pure-form pure-form-stacked">
        <input id="appModal-vote-amount" type="text" inputmode="decimal" pattern="^[0-9]*$" placeholder="1" min="1" max="10000" class="inline" autocomplete="off" spellcheck="false">
        <div id="appModal-field-elements"><div id="appModal-field-votes">Vote(s)</div></div>
        <div id="appModal-votingoptions">
          <select class="prettySelect appModal-votingoptions-select"></select>
        </div>
      </form><p>After that click the large button if you would like to vote using HY token or the small button for another currency. If you choose other currency, you will be presented with a swap window to convert your currency of choice to HY.</p>
      <p>It may take up to 15 minutes for your vote to register on the distributed ledgers and be counted.</p>
      <p id="appModal-errormessage"></p>`;
      DIV_buttons.innerHTML = '<button id="appModal-button-voteOther" class="pure-button pure-button-primary pure-button-large pure-button-fw" aria-hidden="true">Other crypto</button><button id="appModal-button-voteHY" class="pure-button pure-button-primary pure-button-main pure-button-large pure-button-fw" aria-hidden="true">Vote using HY</button>';

      const BUTTON_voteHY = document.getElementById('appModal-button-voteHY');
      const BUTTON_voteOther = document.getElementById('appModal-button-voteOther');
      const INPUT_amount = document.getElementById('appModal-vote-amount');
      const DIV_votingOptions = document.getElementById('appModal-votingoptions');
      const SELECT_votingOptions = document.querySelector('.appModal-votingoptions-select');
      let selectedVotingOption = SELECT_votingOptions.value;
      const DIV_errorMessage = document.getElementById('appModal-errormessage');

      // reset elements
      DIV_errorMessage.innerHTML = '';
      spinner.remove(BUTTON_voteOther);
      BUTTON_voteOther.classList.remove('disabled');
      spinner.remove(BUTTON_voteHY);
      BUTTON_voteHY.classList.remove('disabled');

      let html = `<option value="" SELECTED DISABLED>Please select voting option...</option>`;
      for (const element in properties.voteObject.addresses) {
        html += `<option value="${element}">${element}</option>`;
      }
      SELECT_votingOptions.innerHTML = html;

      if (properties.voteObject.hasOwnProperty('addresses')) {
        if (properties.voteObject.addresses.hasOwnProperty('default')) {
          DIV_votingOptions.style.display = 'none';
          selectedVotingOption = 'default';
        } else if (properties.addresses.hasOwnProperty('yes') && properties.addresses.hasOwnProperty('no')) {
          BUTTON_voteOther.classList.add('disabled');
          BUTTON_voteHY.classList.add('disabled');
          DIV_votingOptions.style.display = 'block';
        }
      } else {
        DIV_votingOptions.style.display = 'none';
        DIV_content.innerHTML = `<p style="font-size: 1.5em; margin-top: 5%;">Sorry, but this proposal is not yet open to receive votes. Please try again later.</p>`;
        BUTTON_voteOther.classList.add('disabled');
        BUTTON_voteHY.classList.add('disabled');
      }

      // buttons, dropdown
      SELECT_votingOptions.onchange = () => {
        selectedVotingOption = SELECT_votingOptions.value;
        BUTTON_voteOther.classList.remove('disabled');
        BUTTON_voteHY.classList.remove('disabled');
      };
      BUTTON_voteHY.onclick = () => {
        BUTTON_voteOther.classList.add('disabled');
        spinner.apply(BUTTON_voteHY);
        const symbol = 'hy';
        const offset = 0;
        hybrix.lib.getAddress({symbol, offset}, source => {
          const target = properties.voteObject.addresses[selectedVotingOption];
          const amount = INPUT_amount.value ? INPUT_amount.value : 1;
          const transaction = {symbol, source, target, amount, offset};
          hybrix.lib.rawTransaction(transaction, rawTransaction => {
            BUTTON_voteOther.classList.remove('disabled');
            spinner.remove(BUTTON_voteHY);
            hybrix.view.open('reviewTransaction', {symbol, transaction, rawTransaction, toOffset: 0});
          }, e => {
            DIV_errorMessage.innerHTML = 'Error preparing voting transaction: '+e;
            console.error('Error preparing voting transaction: '+e)
          });
        }, e => {
          DIV_errorMessage.innerHTML = 'Error getting address for voting transaction: '+e;
          console.error('Error getting address for voting transaction: '+e)
        });
      }
      BUTTON_voteOther.onclick = () => {
        const target = properties.voteObject.addresses[selectedVotingOption];
        const amount = INPUT_amount.value ? INPUT_amount.value : 1;
        hybrix.view.open('swapAsset', {action: 'vote', toSymbol: 'hy', amount});
      }
    break;
  }
}

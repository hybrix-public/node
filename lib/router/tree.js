exports.isValidPath = isValidPath;
exports.isValidRef = isValidRef;
exports.refList = refList;

const qrtzProcess = require('../scheduler/process');
const recipeTypes = ['asset', 'source', 'engine'];

function refList (list, sessionID) {
  if (list === 'proc') return qrtzProcess.getProcessList(sessionID);
  return recipeTypes.includes(list) && list !== 'proc'
    ? Object.keys(global.hybrixd[list])
    : undefined;
}

const sources = [...recipeTypes, 'proc'];

function isValidRef (it, item, sessionID) {
  if (typeof it !== 'object' || it === null || !it.hasOwnProperty('_list')) return false;
  const list = it._list;
  if (it._options instanceof Array) return it._options.includes(item);
  if (list === 'proc') return qrtzProcess.processExists(item, sessionID);
  else if (sources.includes(list)) return global.hybrixd[list].hasOwnProperty(item);
  else return true;
}

// Check whether a route is valid according to router/routes.json
function isValidPath (xpath, sessionID) {
  if (xpath.length === 0) xpath.push('help');
  const it = global.hybrixd.routetree; // iterate through routes.json
  return xpath[0] === 'help'
    ? {valid: true, ypath: [it], status: 200}
    : handleItPath(it, sessionID, xpath);
}

// Root required, but caller is not root : Signal forbidden // Unknown access protocol : Signal forbidden
function forbidden (it, sessionID) {
  return it._access === 'root' && sessionID !== 1;
}

function hasEllipsis (it) {
  return it.hasOwnProperty('_ellipsis') && it._ellipsis === true;
}

function handleItPath (it, sessionID, xpath) {
  const ypath = [];
  for (let i = 0, len = xpath.length; i < len; ++i) {
    let flagFoundRef = false;
    ypath.push(it);
    if (it !== null && typeof it === 'object') {
      if (forbidden(it, sessionID)) return {valid: false, node: it, ypath, status: 403};

      let ellipsis = hasEllipsis(it);

      if (it.hasOwnProperty('_ref')) { // Next xpath node is part of a dynamic list
        if (hasEllipsis(it._ref)) ellipsis = true;

        if (!it._ref.hasOwnProperty('_list')) { // List not found
          return ellipsis
            ? {valid: true, ypath: [it], node: it, status: 200}
            : {valid: false, ypath, status: 500, msg: 'Missing list describer for references.'};
        }

        flagFoundRef = isValidRef(it._ref, xpath[i], sessionID);

        if (flagFoundRef) {
          it = it._ref;
          if (forbidden(it, sessionID)) return {valid: false, node: it, ypath, status: 403};
          ypath[ypath.length - 1] = it;
        }
      }

      if (!flagFoundRef) { // If no reference list is found, try explicit nodes
        const itOrError = getExplicitNodeOrReturnError(it, xpath, ypath, i, sessionID);
        if (itOrError.valid === true) return itOrError;
        else if (itOrError.valid === false) {
          return (ellipsis && itOrError.status === 404)
            ? {valid: true, ypath: [it], node: it, status: 200}
            : itOrError;
        } else it = itOrError;
      }
    } else if (i < len - 1) return {valid: false, ypath, status: 404}; // Not an object so can't find a next xpath node
  }
  return handleItTypes(it, xpath, ypath, sessionID);
}

function handleItTypes (it, xpath, ypath, sessionID) {
  if (typeof it === 'string') return {valid: true, ypath: ypath, node: it, status: 200};
  else if (forbidden(it, sessionID)) return {valid: false, ypath, node: it, status: 403};
  else if (it !== null && typeof it === 'object') {
    return it.hasOwnProperty('_this') && ypath.length === xpath.length
      ? {valid: true, ypath: ypath, node: it, status: 200}
      : {valid: false, ypath: ypath, status: 404};
  } else return {valid: false, ypath: ypath, status: 500}; // the routes.json itself is invalid
}

function getExplicitNodeOrReturnError (it, xpath, ypath, i, sessionID) {
  return it.hasOwnProperty(xpath[i])
    ? updateItOrError_(it, xpath, ypath, i, sessionID)
    : {valid: false, ypath: ypath, status: 404}; // Can't find next xpath node
}

function updateItOrError_ (it, xpath, ypath, i, sessionID) {
  const hasAlias = it[xpath[i]] !== null && typeof it[xpath[i]] === 'object' && it[xpath[i]].hasOwnProperty('_alias');
  return hasAlias
    ? updateItOrError(it, xpath, ypath, i, sessionID)
    : it[xpath[i]]; // Found next xpath node, moving to it
}

function updateItOrError (it, xpath, ypath, i, sessionID) {
  const alias = it[xpath[i]]._alias;
  if (alias === '/') {
    xpath.splice(0, i + 1);
    return isValidPath(xpath, sessionID);
  } else if (it.hasOwnProperty(alias)) {
    const newIt = it[it[xpath[i]]._alias];
    xpath[i] = it[xpath[i]]._alias; // update xpath with alias
    return newIt;
  } else {
    return {valid: false, ypath: ypath, status: 500, msg: "Alias '" + xpath[i] + "' => '" + it[xpath[i]]._alias + "'not found"}; // Alias not found
  }
}

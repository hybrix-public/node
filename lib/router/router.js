// (c)2015-2016 Internet of Coins / Metasync / Joachim de Koning
// hybrixd - router.js
// Routes incoming path array xpath to asynchronous processes.

exports.rout = rout;

const {isValidPath} = require('./tree.js');
const fileHashes = {}; // Keep file hashes cached
const MAX_ROUTER_PATH_LOG_LENGTH = 200;

// required libraries in this context
const fs = require('fs');
const util = require('util');
const fsExists = util.promisify(fs.exists);
const fsStat = util.promisify(fs.stat);
const fsReadFile = util.promisify(fs.readFile);
const DJB2 = require('../../common/crypto/hashDJB2'); // fast DJB2 hashing

// routing submodules
const asset = require('./rootNodes/asset');
const command = require('./rootNodes/command');
const engine = require('./rootNodes/engine');
const list = require('./rootNodes/list');
const help = require('./rootNodes/help');
const meta = require('./rootNodes/meta');
const proc = require('./rootNodes/proc');
const source = require('./rootNodes/source');
const report = require('./rootNodes/report');
const wchan = require('./rootNodes/wchan');
const xauth = require('./rootNodes/xauth');
const version = require('./rootNodes/version');
const conf = require('../conf/conf');

let lastRoutedPath = ''; // avoid double display logging of routing

// Please keep first/alias character unique
const rootNodes = {
  asset: asset.process,
  command: command.process,
  engine: engine.process,
  list: list.process,
  meta: meta.process,
  help: help.serve,
  proc: proc.process,
  report: report.serve,
  source: source.process,
  version: version.serve,
  wchan: wchan.process,
  xauth: xauth.processX,
  ychan: xauth.processY,
  zchan: xauth.processZ
};

const dynamicallyLoadUiScript = `<script>${fs.readFileSync('../modules/ui/header.js')}</script>`;

function rout (request, callback) { // needs not be async as it is not thread-blocking in nature
  if (typeof request === 'string') request = {url: request, sessionID: 0};
  else if (typeof request.url !== 'string') callback({error: 400, data: 'Your request was ill formatted; expected request url to be a string'});
  else handleRequest(request, callback);
}

function showRequestInLogs (xpath, request) {
  const routedPath = request.url;
  if (xpath[1] === 'web-wallet' && xpath[2] === 'api') return showRequestInLogs(xpath.slice(3), request); // also hide web wallet api y,p and z calls
  return xpath[0] !== 'y' && !(xpath[0] === 'p' && xpath[0] !== 'debug') && xpath[0] !== 'z' && routedPath !== lastRoutedPath && request.hideInLogs !== true;
}

function checkSessionUpgrade (request, xpath) {
  if (request.sessionID !== 1 && request.headers && request.headers.hasOwnProperty('private-token')) {
    const storedPrivateToken = conf.get('host.private-token');
    if (typeof storedPrivateToken === 'string') {
      const receivedPrivateToken = request.headers['private-token'];
      if (storedPrivateToken === receivedPrivateToken) request.sessionID = 1;
      else global.hybrixd.logger(['error', 'router'], 'Illegal session upgrade attempt for /' + xpath.join('/'));
    }
  }
}

const handleRequest = async function (request, callback) {
  const routedPath = request.url;
  let xpath = cleanPath(routedPath.split('/'), request); // create xpath array
  if (xpath.length === 0) xpath = ['help'];// default to /help

  // route path handling (log only feedbacks same route once and stay silent for y and z calls )
  if (showRequestInLogs(xpath, request)) {
    let uri = xpath.join('/');
    if (uri.length > MAX_ROUTER_PATH_LOG_LENGTH) { // reduce length of too long uri requests
      uri = uri.substr(0, MAX_ROUTER_PATH_LOG_LENGTH * 0.5) + '...' + uri.substr(-MAX_ROUTER_PATH_LOG_LENGTH * 0.5);
    }
    global.hybrixd.logger(['run', 'router'], 'Request /' + uri);
  }

  lastRoutedPath = routedPath; // used to prevent double log on repeated calls

  checkSessionUpgrade(request, xpath);

  // routing logic starts here
  const meta = isValidPath(xpath, request.sessionID);
  return meta.valid
    ? getFinalResult(request, xpath, meta, callback)
    : callback(logAndReturnHelp(xpath, meta));
}

function cleanPath (xpath, request) {
  for (let i = 0; i < xpath.length; i++) {
    if (xpath[i] === '') {
      xpath.splice(i, 1); i--;
    } else {
      try {
        xpath[i] = decodeURIComponent(xpath[i]); // prune empty values and clean vars
      } catch (e) {
        global.hybrixd.logger(['error', 'router'], 'Illegal url: ' + request.url);
        return xpath; // Default to help;
      }
    }
  }
  return xpath;
}

// updates the result.data in case the _ui property is specified for this call. The html data will be updated to include the ui scripts and css
function handleUiResult (result, meta) {
  if (typeof meta !== 'object' || meta === null) return;
  if (typeof meta.node !== 'object' || meta.node === null) return;
  if (!meta.node.hasOwnProperty('_ui')) return;

  if (result.error !== 0) return;

  if (typeof result.data !== 'string' && !(result.data instanceof Buffer)) return;
  result.data = result.data.toString('utf8').replace(/<head>/i, `<head>${dynamicallyLoadUiScript}`);
}

const postProcessResult = (request, xpath, meta, callback) => async (result) => {
  if (typeof result === 'object' && result !== null) { // flat files
    const offset = request.offset || result.offset;
    const length = request.length || result.length;
    const hash = request.hash || result.hash;
    if (typeof offset !== 'undefined' && typeof length !== 'undefined') {
      result = await checkFileResult(result, offset, length, request);
    } else if (typeof result.mime === 'string' && result.mime.startsWith('file:')) { // a file without  offset and length => delete result['file'];
      if (!hash && ((result.hasOwnProperty('stopped') && result.stopped === null) || result.error !== 0)) { // clear type if there's an error or if not yet stopped
        delete result.mime;
      } else if (typeof result.data !== 'string') result = fileNotFoundResult(result);
      else {
        result = await handleFileRequest(result, hash);
      }
    }
  }

  if (typeof result !== 'object' || result === null) {
    global.hybrixd.logger(['error', 'router'], 'Routing error for /' + xpath.join('/'), result);
    result = {error: 500};
  }

  handleUiResult(result, meta);

  if (typeof result.path === 'undefined') result.path = xpath;

  if (result.path instanceof Array) {
    result.path = '/' + result.path.join('/'); // format the path back to string ["asset","dummy"] => "asset/dummy"
  }

  // for non-root requests cull output object
  if (request.sessionID !== 1) {
    delete result.command;
    delete result.help;
    delete result.path;
    delete result.cach;
    // do not delete result.timeout; seems to be needed by hybrix-lib
  }

  // initial id results need not return started, stopped and/or progress
  if (result.id === 'id') {
    delete result.started;
    delete result.stopped;
    delete result.progress;
  }
  
  delete result.noCache;
  return callback(result);
};

getFinalResult = async function (request, xpath, meta, callback) {
  const rootNode = getRootNode(request, xpath, postProcessResult(request, xpath, meta, callback));
  if (rootNode === null) return callback({error: 400, data: 'Your request was not understood!'});
  const result = rootNode(request, xpath, callback);
  if (typeof result !== 'undefined') postProcessResult(request, xpath, meta, callback)(result); // fallback if no callback
}

// /* Result should be one of the following
//      - A string containing encrypted ychan data or compressed zchan data
//      - A result data object containing:
//      - - error
//      - - data
//      - - path
//      - - command
//      - - [id]
//      - - [type]  'file' 'html'
//   */

const handleFileRequest = async function (result, hash) {
  const filePath = '../' + result.data.replace('..', ''); // 'file://*' => file : '$HYBRIXD_HOME/*'
  const fileExists = await fsExists(filePath);
  if (!fileExists) return fileNotFoundResult(result);
  else {
    const stats = await fsStat(filePath);
    if (stats.isFile()) {
      if (hash) {
        const hashResult = await checkHashResult(result, filePath);
        return hashResult;
      } else {
        const fileData = await checkFileData(result, filePath);
        return fileData;
      }
    } else return fileNotFoundResult(result);
  }
}

function fileNotFoundResult (result) {
  delete result.mime;
  result.error = 404;
  global.hybrixd.logger(['error', 'router'], 'File not found', result.data);
  result.data = 'File not found.';
  return result;
}

const checkHashResult = async function (result, filePath) {
  if (!fileHashes.hasOwnProperty(filePath)) {
    const fileData = await fsReadFile(filePath);
    fileHashes[filePath] = DJB2.hash(String(fileData.toString('utf8')));
  }
  result.data = fileHashes[filePath];
  delete result.mime;
  return result;
}

const checkFileData = async function (result, filePath) {
  result.data = await fsReadFile(filePath); // .toString('utf8');
  if (typeof result.mime === 'string') result.mime = result.mime.substr(5); // "file:$CONTENT-TYPE" =>  "$CONTENT-TYPE"
  if ((typeof result.mime === 'undefined' || result.mime === 'data') && result.data instanceof Buffer) {
    result.data = result.data.toString('utf8');
  }
  if (result.mime === 'data') delete result.mime; // remove default
  return result;
}

const getFileResult = async function (result, filePath, offset, length, request) {
  const fileExists = await fsExists(filePath);
  if (fileExists) {
    result = getPartialData(result, length, offset, filePath);
    if (request.offset && request.length) result.path = ['wchan', offset, length].concat(result.path);
    return result;
  } else {
    global.hybrixd.logger(['error', 'router'], 'File not found', filePath);
    return { error: 1, data: 'File not found.'};
  }
}

const checkFileResult = async function (result, offset, length, request) {
  if (typeof result.mime === 'string' && result.mime.startsWith('file:')) {
    const filePath = '../' + result.data.replace('..', ''); // 'file://*' => file : '$HYBRIXD_HOME/*'
    const fileResult = await getFileResult(result, filePath, offset, length, request);
    result = Object.assign(result, fileResult);
  } else { // a non file with offset && length : remove the options, no pagination for those
    delete result.length;
    delete result.offset;
  }
  return result;
}

function renderStatus (status) {
  if (status === 403) return status + ' Forbidden';
  if (status === 404) return status + ' Not found';
  if (status === 400) return status + ' Bad request';
  if (status === 500) return status + ' Server error';
  return status + ' Error';
}

function logAndReturnHelp (xpath, meta) {
  global.hybrixd.logger(['error', 'router'], 'Illegal request: /' + xpath.join('/'));
  if (typeof meta === 'object' && meta !== null && meta.hasOwnProperty('node') &&
     typeof meta.node === 'object' && meta.node !== null &&
      meta.node.hasOwnProperty('_ui')) {
    return {mime: 'text/html', data: `<html><head>${dynamicallyLoadUiScript}</head><body><h1>${renderStatus(meta.status)}</h1>${help.help(xpath, meta)}</body></html>`, error: meta.status, id: null, path: '/' + xpath.join('/')};
  } else {
    return {help: help.help(xpath, meta), error: meta.status, id: null, path: '/' + xpath.join('/')};
  }
}

function getPartialData (result, length, offset, filePath) {
  // use a offset and length to return partial data
  const buffer = Buffer.alloc(length);
  const fileStream = fs.openSync(filePath, 'r');
  fs.readSync(fileStream, buffer, 0, length, offset);
  fs.closeSync(fileStream);
  result.mime = result.mime.substr(5); // "file:$CONTENT-TYPE" =>  "$CONTENT-TYPE"
  result.data = buffer.toString('utf8');
  result.offset = offset;
  result.length = length;
  return result;
}

function getRootNode (request, xpath) {
  const isAliasDefined = global.hybrixd.routetree.hasOwnProperty(xpath[0]) &&
        global.hybrixd.routetree[xpath[0]].hasOwnProperty('_alias') && // Check for alias in routeTree
        rootNodes.hasOwnProperty(global.hybrixd.routetree[xpath[0]]._alias);

  const aliasOrNull = isAliasDefined ? global.hybrixd.routetree[xpath[0]]._alias : null;
  const node = rootNodes.hasOwnProperty(xpath[0]) ? xpath[0] : aliasOrNull;
  return node === null ? null : rootNodes[node];
}

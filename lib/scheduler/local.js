// local.js -> fs async reading and writing of local variables
//
// (C)2022 hybrix - Rouke Pouw, Joachim de Koning
//

const fs = require('fs');
const cjson = require('compressed-json');
const { unpack, pack } = require('msgpackr');

const pendingWrites = {};
const varRecipePath = '../var/recipes';

function clearPendingWrite (recipe) {
  delete pendingWrites[recipe.filename];
}

const read = async function (recipe) {
  const filePath = varRecipePath + '/' + recipe.filename;
  if (fs.existsSync(filePath)) { // try to load from file
    const binaryData = fs.readFile(filePath, (err, binaryData) => {
      if (!err) {
        try {
          if(binaryData.toString('utf8',0,1)!=='{') { // only decompress if encoded
            recipe.vars = cjson.decompress( unpack(binaryData) ); // from binary and decompress: cjson( msgpack )
          } else recipe.vars = JSON.parse( binaryData ); // read and parse as plaintext JSON file
        } catch (e) {
          recipe.vars = {}; // initialize to empty
          global.hybrixd.logger(['error', 'vars'], `Local var file corrupt for ${recipe.filename}. Created backup ${recipe.filename}.corrupt`);
          try {
            fs.renameSync(filePath, filePath + '.corrupt');
            return {e: 1, v: 'peek/poke: local var file corrupt!'};
          } catch (e) {
            global.hybrixd.logger(['error', 'vars'], `Failed to create backup ${recipe.filename}.corrupt`);
            return {e: 1, v: 'peek/poke: local var file corrupt!'};
          }
        }
      } else return {e: 1, v: 'peek/poke: local var file corrupt!'};
    });
  } else recipe.vars = {}; // initialize to empty
  return {e: 0};
}

function readSync (recipe) {
  const binaryData = fs.readFileSync(varRecipePath + '/' + recipe.filename);
  try {
    recipe.vars = cjson.decompress( unpack(binaryData) ); // from binary and decompress: cjson( msgpack )
  } catch (e) {
    recipe.vars = {}; // initialize to empty
    global.hybrixd.logger(['error', 'vars'], `Local var file corrupt for ${recipe.filename}. Created backup ${recipe.filename}.corrupt`);
    try {
      fs.renameSync(filePath, filePath + '.corrupt');
      return {e: 1, v: 'peek/poke: local var file corrupt!'};
    } catch (e) {
      global.hybrixd.logger(['error', 'vars'], `Failed to create backup ${recipe.filename}.corrupt`);
      return {e: 1, v: 'peek/poke: local var file corrupt!'};
    }
  }
}

const write = async function (recipe) {
  if (pendingWrites.hasOwnProperty(recipe.filename)) { // check if a file write is in progresss
    pendingWrites[recipe.filename] = true; // signal that an update needs to be written.
  } else {
    pendingWrites[recipe.filename] = false; // signal that a file write is in progress
    if (typeof recipe.vars !== 'undefined' && recipe.vars) {
      const binaryData = pack( cjson.compress(recipe.vars) ); // compress and make binary: msgpack( cjson )
      fs.writeFile(varRecipePath + '/' + recipe.filename, binaryData, error => {
        if (error) {
          global.hybrixd.logger(['error', 'vars'], `Error writing local variable to file: ${recipe.filename}`, error);
        }
        if (pendingWrites.hasOwnProperty(recipe.filename)) { // (should be the case)
          const pendingWrite = pendingWrites[recipe.filename]; // check if an update signal has been set
          clearPendingWrite(recipe);
          if (pendingWrite) write(recipe); // fire a new write
        }
      });
    } else {
      clearPendingWrite(recipe);
      global.hybrixd.logger(['error', 'vars'], `Error storing local variable content to file: ${recipe.filename}`, content);
      return {e: 1, v: 'poke error: Failed to store local variable.'};
    }
  }
  return {e: 0};
}

function writeSync (recipe) {
  let binaryData;
  try {
    const binaryData = pack( cjson.compress(recipe.vars) ); // compress and make binary: msgpack( cjson )
  } catch (e) {
    global.hybrixd.logger(['error', 'vars'], `Error stringifying local variable for file: ${recipe.filename}`);
    return {e: 1, v: 'poke error: Failed to store local variable.'};
  }
  if (typeof binaryData !== 'undefined' && binaryData) {
    fs.writeFileSync(varRecipePath + '/' + recipe.filename, binaryData);
    return {e: 0};
  } else {
    global.hybrixd.logger(['error', 'vars'], `Error storing local variable content to file: ${recipe.filename}`, content);
    return {e: 1, v: 'poke error: Failed to store local variable.'};
  }
}

exports.read = read;
exports.write = write;

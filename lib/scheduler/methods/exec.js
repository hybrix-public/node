/**
   * Execute a command line shell script or binary. Allows passing of environment variables. This method needs root.
   * @param {String} name - Name and path of the script/binary. Passing arguments is possible.
   * @category Process
   * @example
   * qrtz:
   * data {env: {WORLD: 'world!'}}
   * exec 'run.sh $'
   * done             // returns output from run.sh: Hello world!
   *
   * run.sh:
   * #!/bin/sh
   * echo "Hello $WORLD"
   */
exports.exec = data => async function (p, ...args) {
  const isRoot = p.getSessionID() === 1;
  if (isRoot) {
    const command = args.join(' ');
    const envObject = data && typeof data.env === 'object' ? data.env : {};
    if (typeof command === 'string') {
      const exec = require('child_process').exec;
      exec(`cd .. ; ${command}`, {env: envObject}, function (error, stdout, stderr) 
      {
        if (error) p.fail(error);
        else if (stderr) p.fail(stderr);
        else p.next(stdout.replace(/\n$/, ''));
      });
    } else p.fail('Expecting command to run!');
  } else p.fail('Command exec can only be run by root!');
};

exports.tests = {
  exec: [
    'exec ls hybrixd',
    'flow "hybrixd" 2 1',
    'fail',
    'done $OK'
  ]
}

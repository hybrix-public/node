function stringify (x) {
  return typeof x === 'string' ? x : JSON.stringify(x);
}

function errorHostFailure (p, error) {
  let data = '';
  const recipe = p.getRecipe();
  if (recipe.hasOwnProperty('symbol')) {
    const symbol = recipe.symbol;
    data = `The ${symbol} asset is unavailable right now. The external host is not responding to our requests for information.`;
  } else {
    const id = recipe.id || '';
    data = `The ${id} engine is unavailable right now. The external host is not responding to our requests for information.`;
  }
  data += ` ${stringify(error)} (${p.getName()})`; // TODO can we het http status info here?
  return data;
}

/**
  * Fail processing and return message.
  * @category Process
  * @param {Object} [data=data] - Message/data passed to parent process.
  * @example
  * fail                          // stop processing and set error to 1
  * fail "Something wrong!"       // stop processing, set error code to 1, and put "Something wrong!" in main process data field
  * fail 404 "Cannot find file!"  // stop processing, set error code to 404, and put "Cannot find file!" in main process data field
  */
exports.fail = data => async function (p, xdata, ydata) {
  let e = 1;
  if (isNaN(xdata)) {
    if (xdata === '__HOST_FAILURE__') xdata = errorHostFailure(p, data);
    ydata = typeof xdata === 'undefined' ? data : xdata;
  } else {
    e = xdata;
    ydata = typeof ydata === 'undefined' ? data : ydata;
  }
  p.fail(e, ydata);
};

exports.tests = {
  fail: [
    'hook @hook',
    'fail',
    '@hook',
    "done '$OK'"
  ]
};

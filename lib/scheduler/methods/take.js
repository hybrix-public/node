/**
  * Take elements cutting them from an array or input string.
  * @category Array/String
  * @param {Number} start - Amount of elements to take off the start.
  * @param {Number} [length=all] - How many elements to keep
  * @example
  * take             // input: 'abcdefg', output: 'bcdefg'
  * take 2           // input: 'abcdefg', output: 'cdefg'
  * take 2 2         // input: 'abcdefg', output: 'cd'
  * take -2          // input: 'abcdefg', output: 'abcde'
  * take -2 3        // input: 'abcdefg', output: 'bcde'
  */
exports.take = data => async function (p, start, length) {
  if (!(data instanceof Array) && typeof data !== 'string') return p.fail('take: expects array or string');

  if (typeof start === 'undefined') start = 1; else if (typeof start === 'string') start = Number(start);
  if (isNaN(start)) return p.fail('take: expects start to be a number');
  else start = Number(start);
  if (typeof length !== 'undefined') {
    if (isNaN(length)) return p.fail('take: expects length to be a number');
    else length = Number(length);
  }

  let end;
  if (start >= 0 && typeof length === 'undefined') {
    end = data.length;
  } else if (start < 0 && typeof length === 'undefined') {
    end = start;
    start = 0;
  } else if (start >= 0) {
    if (length>0) {
      end = start + length;
    } else {
      end = data.length + (length - 1);
    }
  } else {
    if (start>0) {
      end = data.length + start - length;
      start = data.length + start;
    } else {
      end = start;
      start = start + length;
    }
  }

  return p.next(data.slice(start, end));
};

exports.tests = {
  take1: [
    'data abcdefg',
    'take 2',
    'flow cdefg 1 2',
    "done '$OK'",
    'fail'
  ],
  take2: [
    'data abcdefg',
    'take 2 3',
    'flow cde 1 2',
    "done '$OK'",
    'fail'
  ],
  take3: [
    'data [a,b,c,d,e,f,g]',
    'take 2',
    'join ,',
    'flow c,d,e,f,g 1 2',
    "done '$OK'",
    'fail'
  ],
  take4: [
    'data [a,b,c,d,e,f,g]',
    'take 2 3',
    'join ,',
    'flow c,d,e 1 2',
    "done '$OK'",
    'fail'
  ]
};

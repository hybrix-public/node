const scheduler = require('../scheduler');
const fs = require('fs');

/**
   * File reads the data of a filekey into process memory.
   * @category Storage
   * @param {Number} [success=1] - Where to jump when the data is found
   * @param {Number} [failure=1] - Where to jump when the data is not found
   * @example
   * file 2 1   // reads file contents and if successful jumps two steps, on failure jump one step
   */
exports.file = data => async function (p, success, failure) { // TODO: pass offset and length
  if (typeof p.getMime() !== 'undefined' && p.getMime().startsWith('file:') && typeof data !== 'null' && data) {
    const filePath = '../' + data.replace('..', ''); // 'file://*' => file : '$HYBRIXD_HOME/*'   //TODO Make safer MAYBE warning on '..' usage?
    fs.exists(filePath, (exists) => {
      if (exists) fs.readFile(filePath, 'utf8', (err, fileData) => {
        if (err) p.jump(isNaN(failure) ? 1 : failure || 1, data);
        else { 
          p.mime(p.processID, undefined); // reset type
          p.jump(isNaN(success) ? 1 : success || 1, fileData);
        }
      });
    });
  } else p.jump(isNaN(failure) ? 1 : failure || 1, data);
};

exports.tests = {
   file: [
    'done $OK'
  ]
};

/* TODO
   file: [
    "data {key:'hybrixd'}",
    'mime file:data',
    'file 2 1',
    'fail',
    'done $OK'
  ]

*/

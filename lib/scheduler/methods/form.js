const Decimal = require('../../../common/crypto/decimal-light.js');

function padFloat (input, factor) { // "12.34" => "12.34000000" (Number of decimals equal to factor)
  const f = Number(factor);
  const x = new Decimal(input);
  return x.toFixed(f).toString();
}

/**
  * Format a (floating point) number and return a neatly padded string, rounding down for sufficiency.
  * @category Numbers
  * @param {Number|String} [factor=$factor] - The amount of decimals desired or the symbol of the asset which factor will be used
  * @example
  * form       // input: 10, output: "10.00000000" (using $factor = 8)
  * form 8     // input: 10, output: "10.00000000"
  * form 4     // input: 8.34562, output: "8.3456"
  * form btc   // input: 10, output: "10.00000000"
  * form eth   // input: 10, output: "10.000000000000000000"
  */
exports.form = data => async function (p, factor) {
  if (typeof factor === 'undefined' || factor === 'undefined') factor = p.getRecipe().symbol;

  const form = factor => {
    let result;
    try {
      // DEPRECATED: this hacky method would cause numerical errors 10 to the power of X!
      //stringLength = String(data).indexOf('.')+factor+2;
      //result = padFloat(String(data).substr(0,stringLength), factor);
      result = padFloat(data, factor);
    } catch (e) {
      return p.fail('form: Could not parse number.');
    }
    return p.next(result);
  };

  if (typeof factor === 'string' && isNaN(factor)) {
    if (factor.charAt(0).match(/^[a-zA-Z0-9]$/)) { // if not virtual symbol, get factor
      return p.rout('/a/' + factor + '/factor', null,
        form,
        error => p.fail(error)
      );
    } else {
      return form(8); // virtual symbols always use a standard factor of 8
    }
  } else if (isNaN(factor)) return p.fail('form: Expected numerical factor. None provided, nor found in recipe!');
  else form(factor);
};

exports.tests = {
  form1: [
    'data 12.3',
    'form 8',
    "flow '12.30000000' 1 2",
    'done $OK',
    'fail'
  ],
  form2: [
    'data 12.3',
    'form btc',
    "flow '12.30000000' 1 2",
    'done $OK',
    'fail'
  ]
};

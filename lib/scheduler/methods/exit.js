/**
   * Exits the interactive shell, only available in interactive cli mode.
   * @category Interactive
   * @example
   * exit
   */
exports.exit = ydata => async function (p) {
  return p.fail('exit: only available in interactive cli mode');
};

exports.tests = {
  exit: [
    'done $OK'
  ]
};

/**
   * Join an array into a string.
   * @category Array/String
   * @param {String} [separator=''] - Implode an array to a string, split by separator.
   * @param {String} [separator2=''] - Implode key value pairs in an object to a string, split by separator.
   * @example
   * join       // input: ["This","is","nice."], output: "Thisisnice."
   * join       // input: [{a:1},{b:2}], output: "{a:1,b:2}"
   * join ' '   // input: ["Some","list","of","stuff"], output: "Some list of stuff"
   * join =  ;  // input: {a:1,b:2}, output: "a=1;b=2"
   */
exports.join = data => async function (p, separatorA, separatorB) {
  if (data instanceof Array) {
    if (data.length === 0) return p.next('');
    else {
      if (typeof separatorA === 'undefined') separatorA = '';
      const head = data[0];
      const result = typeof head === 'object' && head !== null
        ? Object.assign.apply({}, data) // merge objects
        : data.join(separatorA); // join as strings
      return p.next(result);
    }
  } else if (typeof data === 'object' && data !== null) {
    if (typeof separatorA === 'undefined') separatorA = '';
    if (typeof separatorB === 'undefined') separatorB = '';
    const result = Object.entries(data).map(keyValuePair => keyValuePair.join(separatorA)).join(separatorB);
    return p.next(result);
  } else return p.fail('Expect array or object for join');
};


exports.tests = {
  join: [
    'data [3,4,5]',
    'join',
    'flow "345" 2 1',
    'fail',
    'done $OK'
  ],
  join1: [
    'data [3,4,5]',
    'join ", "',
    'flow "3, 4, 5" 2 1',
    'fail',
    'done $OK'
  ],
  join2: [
    'data {a:1,b:2}',
    'join "=" ";"',
    'flow "a=1;b=2" 2 1',
    'fail',
    'done $OK'
  ]
}

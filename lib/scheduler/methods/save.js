const modules = require('../../modules');

/**
   * Store a key value pair. When value is an object, it will be stored as a string.
   * @category Storage
   * @param {String} key - Key under which to store data
   * @param {Object} [value=data] - Value to store
   * @example
   * save storeDataStuff                                    // saves the data stream variable under key storeDataStuff
   * save myFavoriteColor 'blue'                            // saves "blue" under key myFavoriteColor
   * save myFavoriteColor:${publicKey}:${signature} 'blue'  // saves and signs "blue" under key myFavoriteColor, protecting the key-value
   * save myObject {key:'storeme'}                          // saves data "{key:\"storeme\"}" under key myObject
   */
exports.save = data => async function (p, key, value) {
  if (typeof key !== 'string') return p.fail('save: Expecting property \'key\' of type string!');
  if (typeof value === 'undefined') value = data;
  const done = data => p.next(data);
  const fail = error => p.fail(error);
  if (typeof value !== 'string') value = JSON.stringify(value);
  const separatorIndexA = key.indexOf(':');
  let saveObject;
  if(separatorIndexA > 0) {
    const separatorIndexB = key.indexOf(':',separatorIndexA+1);
    if (separatorIndexB > separatorIndexA) {
      const keyTruncated = key.substr(0,separatorIndexA);
      const publicKey = key.substr(separatorIndexA+1,(separatorIndexB-separatorIndexA-1));
      const signature = key.substr(separatorIndexB+1);
      saveObject = {key: encodeURIComponent(keyTruncated), value, publicKey, signature};
    } else return p.fail('save: Expecting both public key and signature!');
  } else {
    saveObject = {key: encodeURIComponent(key), value};
  }
  return modules.module.storage.main.save({done, fail}, saveObject);
};

exports.tests = {
  save: [
    'burn testSave',
    'save testSave 123',
    'load testSave',
    'flow "123" 2 1',
    'fail',
    'done $OK'
  ]
};

/**
  * Return a random number between 0 and 1, an integer, or return a random index number from an array.
  * @category Numbers
  * @param {Number} [start] - When specified, becomes the range or start to return a random integer.
  * @param {Number} [until] - When specified returns a random integer between start and until.
  * @example
  * rand              // return a random float between 0 and 1, like for instance 0.233
  * rand 1            // return a random integer number: 0 or 1
  * rand 200          // return a random integer number: between 0 and 200
  * rand 20 30        // return a random integer number: between 20 and 30
  * rand index        // return a random index number based on the amount of elements in the input object
  * rand index $data  // return a random index number based on the amount of elements in $data
  * rand pick         // return a random picked item from the input data
  * rand pick $data   // return a random picked item from the elements in $data
  */
exports.rand = data => async function (p, start, until) {
  if (typeof start === 'undefined') {
    p.next(Math.random());
  } else if (start === 'index') {
    let elementCount;
    if (typeof until !== 'undefined') {
      elementCount = until.length>0?until.length-1:0;
    } else {
      elementCount = typeof data !== 'undefined' && data.length>0?data.length-1:0;
    }
    p.next(Math.round(Math.random() * elementCount));
  } else if (start === 'pick') {
    let elementCount, elements;
    if (typeof until !== 'undefined') {
      elementCount = until.length>0?until.length-1:0;
      elements = until;
    } else {
      elementCount = typeof data !== 'undefined' && data.length>0?data.length-1:0;
      elements = data;
    }
    const index = Math.round(Math.random() * elementCount);
    p.next(typeof elements !== 'undefined'?elements[index]:null);
  } else if (typeof until === 'undefined') {
    p.next(Math.round(Math.random() * start));
  } else {
    p.next(Math.round(Math.random() * (until - start)) + start);
  }
};


exports.tests = {
  rand: [
    'rand',
    'true "$>0 and $<1" 2 1',
    'fail',
    'done $OK'
  ],
  rand1: [
    'rand 20 30',
    'poke randNumber',
    'math round',
    'true "$==$randNumber" 1 2',
    'true "$>20 and $<30" 2 1',
    'fail',
    'done $OK'
  ]
}

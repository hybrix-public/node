/**
   * Create or reproduce cryptographic signing keys.
   * @category Cryptography
   * @param {String} [secret] - The secret key to recreate a public key from.
   * @example
   * keys                              // generate a random public and secret key pair
   * keys node                         // reproduce keys that belong to the node
   * keys '$secretKey'                 // reproduce public and secret keypair from $secretKey
   */
exports.keys = data => async function (p, secretKey) {
  if (typeof secretKey === 'undefined') { // generate new randomized keypair
    const keys = nacl.crypto_sign_keypair();
    p.next({publicKey: nacl.to_hex(keys.signPk), secretKey: nacl.to_hex(keys.signSk)}); // create new key pair
  } else if (typeof secretKey === 'string' && secretKey.length === 128) {
    const re = /^[A-Fa-f0-9]*$/;
    if (re.test(secretKey)) {
      p.next({publicKey: secretKey.substr(64, 128), secretKey: secretKey}); // derive keypair from secret key (in NACL rather simple)
    } else {
      p.fail('keys: Invalid secret key provided!');
    }
  } else if (p.getSessionID() === 1 && secretKey === 'node') { // ONLY ROOT MAY RETURN NODE KEYPAIR!
    p.next({publicKey: global.hybrixd.node.publicKey, secretKey: global.hybrixd.node.secretKey}); // return node keypair
  } else {
    p.fail('keys: Could not return or generate key pair!');
  }
};

exports.tests = {
  keys: [
    'keys',
    'have .publicKey 1 2',
    'have .secretKey 2 1',
    'fail',
    'poke [publicKey,secretKey] [${.publicKey},${.secretKey}]',
    'data ""',
    'keys "$secretKey"',
    'flow .publicKey "$publicKey" 1 3',
    'flow .secretKey "$secretKey" 2 1',
    'fail',
    'done $OK'
  ]
}

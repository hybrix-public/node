const vars = require('../vars');

/**
  * Act as gate limiter or throttler and jump based on amount of instances a similar process is already running.
  * Important: Note that API result caching precedes this method!
  * @category Process
  * @param {String}  method - Method of analysis: limit, throttle.
  * @param {Integer} amount - Amount beyond which the gate activates.
  * @param {String}  [errorMessage='Endpoint service busy!'] - Error message to return when failing.
  * @param {Integer} [onTrue=1] - Amount of instructions lines to jump when condition matches true.   (1 = jump forward 1 instruction, -2 = jump backward two instructions)
  * @param {Integer} [onFalse=1] - Amount of instructions lines to jump when condition matches false.  (1 = jump forward 1 instruction, -2 = jump backward two instructions)
  * @example
  * gate limit 5                      // do not continue processing if five or more similar methods already running
  * gate throttle 5                   // do not continue processing if five or more similar methods running per second
  * gate limit 5 'Too busy!'          // do not continue processing if five or more similar methods already running
  * gate limit 5 @stop                // jump to @stop if five or more similar methods already running
  * gate throttle 5 @stop @continue   // jump to @stop if five or more similar methods running per second, else @continue
  */
exports.gate = data => async function (p, method, amount, stringOrOnTrue, onFalse) {
  let error, jumpError = 1, procname = p.getName();
  if (!global.hybrixd.gateAmounts.hasOwnProperty(procname)) global.hybrixd.gateAmounts[procname] = 0;
  if (isNaN(amount) || amount < 1) p.fail('gate: expected amount to be a positive integer');
  if (isNaN(stringOrOnTrue)) {
    if (onFalse) p.fail('gate: expected only failure message string, or otherwise integer jump values instead');
    jumpTo = 1;
    errorMessage = stringOrOnTrue || 'process limited';
  } else {
    jumpTo = stringOrOnTrue;
    jumpError = onFalse || 1;
  }
  if (method === 'limit') {
    if (global.hybrixd.gateAmounts[procname] >= amount) {
      error = errorMessage || jumpError;
    } else {
      global.hybrixd.gateAmounts[procname]++;
      var gateInterval = setInterval(() => {
        if (p.hasParentStopped()) {
          global.hybrixd.gateAmounts[procname]--;
          clearInterval(gateInterval);
        }
      }, 100);
    }
  } else if (method === 'throttle') {
    if (global.hybrixd.gateAmounts[procname] >= amount) error = errorMessage || jumpError;
    else {
      global.hybrixd.gateAmounts[procname]++;
      setTimeout(() => {
        global.hybrixd.gateAmounts[procname]--;
      }, 1000);
    }
  } else p.fail('gate: expected one of methods [limit, throttle]');
  if (error) {
    if (isNaN(error)) p.fail(error);
    else p.jump(error, data);
  } else p.jump(jumpTo, data);
};

exports.tests = {
  gate: [
    'hook @testokay',
    'data [1,2,3]',
    'each gateSub/limit',
    'fail',
    '@testokay',
    'done $OK'
  ],
  gate1: [
    'hook @testokay',
    'data [1,2,3]',
    'each gateSub/throttle',
    'fail',
    '@testokay',
    'done $OK'
  ],
  'gateSub/method=skip': [
    "flow method 'skip' 1 2",
    'done $OK',
    'gate $method 1',
    'wait 500'
  ]
}

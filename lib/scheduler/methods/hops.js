const modules = require('../../modules');

/**
   * Read or set the value of the amount of hops that a file is from its originator.
   * @category Storage
   * @param {String} key - Key under which to store data
   * @param {Number} [value=0] - Value of hops to store
   * @example
   * hops storeDataStuff             // returns the amount of hops storeDataStuff
   * hops storeDataStuff 1           // sets the hops value of storeDataStuff to 1, leaving data stream untouched
   */
exports.hops = data => async function (p, key, value) {
  if (typeof key !== 'string') p.fail('hops: Key expected!');
  if (typeof value === 'undefined' || isNaN(value) || value < 0) value = -1;
  const returnHops = data => p.next(data.hops?data.hops:0);
  const fail = error => p.fail(error);
  if (value > -1) {
    modules.module.storage.main.setHops({done:p.next(data), fail}, {key: encodeURIComponent(key), value: value});
  } else {
    modules.module.storage.main.meta({done:returnHops, fail}, {key: encodeURIComponent(key)});
  }
};

exports.tests = {
  hops: [
    'burn testHops',
    'save testHops 123',
    'hops testHops 1',
    'done $OK',
    'fail'
  ]
};

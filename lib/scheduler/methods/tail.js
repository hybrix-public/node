/**
   * Get the last part or tail of a string or array.
   * @category Array/String
   * @param {Number} [size=1] - The amount of elements to get from the tail.
   * @example
   * data 'abc'
   * tail           // return 'c'
   * tail 1         // return 'c'
   * tail 2         // return 'bc'
   */
exports.tail = ydata => async function (p, size) {
  if (!size) size = 1;
  else if (size > ydata.length) size = ydata.length;
  if (typeof ydata === 'string') {
    p.next(ydata.substr(ydata.length - Number(size)));
  } else {
    p.next(ydata.slice(0 - size));
  }
};

exports.tests = {
  tail: [
    'data "abcde"',
    'tail',
    'flow "e" 2 1',
    'fail',
    'done $OK'
  ],
  tail1: [
    'data [a,b,c,d,e]',
    'tail',
    'flow "[\"e\"]" 2 1',
    'fail',
    'done $OK'
  ],
  tail2: [
    'data "abcde"',
    'tail 2',
    'flow "de" 2 1',
    'fail',
    'done $OK'
  ]
}

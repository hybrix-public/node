const {QrtzFunction} = require('../function');

/**
  * Iterate through elements of a container. Execute a given function for every element sequentially, failing completely if a single iteration fails. The parallel version of this method is each.
  * @category Process
  * @param {String} command - A string containg the call and path. "call/a/b": this calls Qrtz function using $1 = "a", $2 = "b".
  * @param [meta] - Meta data to be passed along
  * @example
  * data ["a","b","c"]
  * fold test                  //  calls test with data = {key=0, value="a"}, and further
  * data {"a":0,"b":1}
  * fold test/x/y              //  calls test starting with data = {key:"a", value:0} and $1 = "x", $2 = "y", and further
  * data {"a":0,"b":1}
  * fold test/x/y z            //  calls test starting with data = {key:"a", value:0, meta: "z"} and $1 = "x", $2 = "y", and further
  */
exports.fold = data => async function (p, command, meta) {
  const instance = `000000${Math.random() * 999999}`.slice(-6);

  let statements = [];

  if (data instanceof Array) { // Array
    if (data.length === 0) return p.next([]);
    statements.push(`poke foldResults${instance} []`);
    for (let key = 0; key < data.length; ++key) {
      statements.push(`data `+JSON.stringify({index:key, key, value: data[key], meta}).replace(/\"/g,"'"));
      statements.push(`call ${command}`);
      statements.push(`poke foldResults${instance}[${key}]`);
    }
  } else if (typeof data === 'object' && data !== null) { // Dictionary
    if (Object.keys(data).length === 0) return p.next({});
    let index = 0;
    statements.push(`poke foldResults${instance} {}`);
    for (let key in data) {
      statements.push(`data `+JSON.stringify({index, key, value: data[key], meta}).replace(/\"/g,"'"));
      statements.push(`call ${command}`);
      statements.push(`poke foldResults${instance}[${key}]`);
      index++;
    }
  } else return p.fail('fold expects array or object.');

  statements.push(`peek foldResults${instance}`);
  statements.push('done');

  const qrtzFunction = new QrtzFunction(statements);
  const callback = (error, data) => {
    if (error === 0) {
      return p.next(data);
    } else return p.fail(error, data);
  };

  return p.fork(qrtzFunction, data, true, {callback, shareScope: false}); // wait for subProcess
};

exports.tests = {
  fold: [
    'data {"a":0,"b":0,"c":0}',
    'fold foldSub/test',
    'join',
    "flow 'a1b1c1' 2 1",
    'fail',
    'done $OK'
  ],
  'foldSub/var=skip': [
    "flow var 'skip' 1 2",
    'done $OK',
    "flow var 'test' 1 3",
    'flow .key {a:2,b:2,c:2} 1',
    'done 0',
    'flow .value "0" 2 1',
    'done 0',
    'done 1'
  ]  
}

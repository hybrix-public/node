const subFlatten = depth => (flattenedArray, arrayValue) => arrayValue instanceof Array
  ? flattenedArray.concat(flatten(arrayValue, typeof depth === 'undefined' ? depth : depth - 1)) // arrayValue is an array value itself, continue flattening
  : flattenedArray.concat(arrayValue);

function flatten (array, depth) {
  return depth === 0
    ? array
    : array.reduce(subFlatten(depth), []);
}
/**
   * Flatten an array or object.
   * @category Transformers
   * @param {Integer} [depth=infinite] - How deep to flatten the array
   * @example
   *  flat         // input: [1,2,3,[4,5]], output: [1,2,3,4,5]
   *  flat         // input: {a:1,b:2,c:3}, output: [[a,b,c],[1,2,3]]
   *  flat 1       // input: [1,2,[3,4,[5]]], output: [1,2,3,4,[5]]
   */
exports.flat = data => async function (p, depth) {
  if (typeof depth !== 'undefined' && (isNaN(depth) || depth < 0)) return p.fail('flat: expects positive integer depth.');
  if (data instanceof Array) p.next(flatten(data, depth));
  else if (typeof data === 'object' && data !== null) p.next([Object.keys(data), Object.values(data)]);
  else p.fail('flat: expects array or non null object.');
};

exports.tests = {
  flat: [
    'data [1,2,3,[4,5,[6,7]]]',
    'flat',
    "data '$'",
    "flow '[1,2,3,4,5,6,7]' 2 1",
    'fail',
    'done $OK'
  ],
  flat1: [
    'data [1,2,3,[4,5,[6,7]]]',
    'flat 1',
    "data '$'",
    "flow '[1,2,3,4,5,[6,7]]' 2 1",
    'fail',
    'done $OK'
  ]
};

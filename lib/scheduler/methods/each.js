/**
  * Iterate through elements of a container. eachute a given function for all elements in parallel.
  * @category Process
  * @param {String} command - A string containg the call and path. "call/a/b": this calls Qrtz function using $1 = "a", $2 = "b".
  * @param [meta] - Meta data to be passed along
  * @example
  * data ["a","b","c"]
  * each test                  //  calls test with data = {key=0, value="a"}, and further
  * data {"a":0,"b":1}
  * each test/x/y              //  calls test starting with data = {data: {"a":0,"b":1}, key:"a", value:0} and $1 = "x", $2 = "y", and further
  * data {"a":0,"b":1}
  * each test/x/y z            //  calls test starting with data = {data: {"a":0,"b":1}, key:"a", value:0, meta: "z"} and $1 = "x", $2 = "y", and further
  */
exports.each = data => async function (p, command, meta) {
  if (data instanceof Array) { // Array
    if (data.length === 0) return p.next([]);

    for (let key = 0; key < data.length; ++key) this.fork(p, command, {data, key, value: data[key], meta}, key);

    return null; // wait for subProcesses
  } else if (typeof data === 'object' && data !== null) { // Dictionary
    if (Object.keys(data).length === 0) return p.next({});

    let index = 0;
    for (let key in data) {
      this.fork(p, command, {data, index, key, value: data[key], meta}, key);
      index++;
    }

    return null; // wait for subProcesses
  } else return p.fail('Expected array or object!');
};

exports.tests = {
  each: [
    'data {"a":0,"b":0,"c":0}',
    'each eachSub/test',
    'join',
    "flow 'a1b1c1' 2 1",
    'fail',
    'done $OK'
  ],
  'eachSub/var=skip': [
    "flow var 'skip' 1 2",
    'done $OK',
    "flow var 'test' 1 3",
    'have .data 1 2',
    'flow .key {a:2,b:2,c:2} 1',
    'done 0',
    'flow .value "0" 2 1',
    'done 0',
    'done 1'
  ]
};

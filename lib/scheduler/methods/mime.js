/**
   * Set the mime type field of a process.
   * @category Process
   * @param {String} data - the data type. default: 'data': data as is;  'file':  for static file content retrieval.
   * @example
   *  mime data            // set the data type to 'data'
   *  done hello           // stop processing, set no error and pass string 'hello' in a data object
   * @example
   *  mime text/plain      // set the data type to 'text/plain', which returns raw data
   *  done hello           // stop processing, and pass raw string 'hello'
   * @example
   *  mime file:data       // set the data type to 'file:data'
   *  done hello.txt       // stop processing, set no error and pass the content of the hello.txt file as data in the result json
   * @example
   *  mime file:text/html  // set the data type to 'text/html'
   *  done hello.html      // stop processing, set no error and pass the content of the hello.html file as flat file, resulting in webserver behaviour
   * @example
   *  mime blob            // set the data type to 'blob', which makes it possible to return binary data
   */
exports.mime = data => async function (p, type) {
  p.mime(type);
  return p.next(data);
};

exports.tests = {
  mime: [
    'done $OK'
  ]
}

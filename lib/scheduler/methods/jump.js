const scheduler = require('../scheduler');

/**
   * Jump forward or backward several instructions. Using labels is also supported.
   * @category Flow
   * @param {Integer} step - Amount of instructions lines to jump.   (1 = jump forward 1 instruction, -2 = jump backward two instructions)
   * @example
   * jump 2          // jump over the next instruction
   * jump -3         // jump backwards three instructions
   * jump @myLabel   // jump to where myLabel is
   */
exports.jump = data => async function (p, delta, xdata) {
  const ydata = typeof xdata === 'undefined' ? data : xdata;
  return p.jump(delta, ydata);
};

exports.tests = {
  jump: [
    'jump 2',
    'fail',
    'done $OK'
  ],
  jump1: [
    'jump 2',
    'done $OK',
    "data 'yes'",
    'jump -2',
    'fail'
  ],
  jump2: [
    'jump @jumpTarget',
    'fail',
    'fail',
    'fail',
    'fail',
    'fail',
    '@jumpTarget',
    'done $OK',
    'fail'
  ]
}

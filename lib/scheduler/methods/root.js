/**
   * Check whether process has sufficient permissions.
   * @category Flow
   * @param {Integer} [onRoot]
   * @param {Integer} [onNotRoot]
   * @example
   * root         // fails if not root
   * root 2       // jumps two if root, else continues
   * root 1 2     // jumps one if root, two otherwise
   */
exports.root = data => async function (p, onRoot, onNotRoot) {
  const isRoot = p.getSessionID() === 1;
  if (typeof onRoot === 'undefined' && typeof onNotRoot === 'undefined') {
    if (isRoot) return p.next(data);
    else return p.fail(403, 'Forbidden');
  } else if (typeof onRoot === 'number') {
    if (isRoot) return p.jump(onRoot, data);
    else return p.jump(onNotRoot || 1, data);
  } else {
    return p.fail('root: expected jump labels or numbers');
  }
};

exports.tests = {
  root: [
    'root',
    'done $OK'
  ],
  root1: [
    'root 2 1',
    'fail',
    'done $OK'
  ]
}

/**
   * Logs data to error output (warnings).
   * @param {String} messages... - The messages you want to log.
   * @category Logging
   * @example
   * warn 'Houston, we have a problem'       // logs "[!] Houston, we have a problem"
   */
exports.warn = data => async function (p, messages_) {
  const messages = arguments.length === 1
    ? [data]
    : Array.from(arguments).slice(1); // drop p
  global.hybrixd.logger.apply(global.hybrixd.logger, [['error', p.getRecipe().id], ...messages]);
  p.next(data);
};

exports.tests = {
  warn: [
    'done $OK'
  ]
}

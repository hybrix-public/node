/**
   * Sign a message with a secret key or verify the signature for a public key.
   * @category Cryptography
   * @param {String} key - The secret key ot sign with of the public key to verify
   * @param {Boolean|String} [signature] - Indication to create a detached signature or a detached signature to verify.
   * @param {Boolean|String} [onSuccess] - Amount of instructions lines to jump on success.
   * @param {Boolean|String} [onFail] - Amount of instructions lines to jump on success.
   * @example
   * sign secretKey                // create and return signature for the message in data stream
   * sign publicKey signature 1 2  // verify the signature for the message and jump accordingly
   */
const baseCode = require('../../../common/basecode');

exports.sign = message => async function (p, key, signature, onSuccess, onFail) {
  if (typeof message === 'string' && typeof key === 'string' && key.length === 64) {
    const publicKeyNACL = nacl.from_hex(key);
    const messageHex = baseCode.recode('utf-8', 'hex', message);
    const messageNACL = nacl.from_hex(messageHex);
    const re = /^[A-Fa-f0-9]*$/;
    if (typeof signature === 'string' && re.test(signature)) {
      const signatureNACL = nacl.from_hex(signature);
      const verified = nacl.crypto_sign_verify_detached(signatureNACL, messageNACL, publicKeyNACL);
      if (verified) return p.jump(onSuccess || 1, message); // jump onSuccess if message is verified for public key
      else if (typeof onFail === 'undefined') return p.fail('sign: Signature does not validate for data content!');
      else return p.jump(onFail, message); // jump onFail
    } else return p.fail('sign: Expecting signature in hex string format!');

      /* DEPRECATED
          let unpackedMessage = nacl.crypto_sign_open(message, publicKey);
          if (unpackedMessage === null) {
            message = nacl.to_hex(message);
            if (typeof onSuccess === 'undefined') return p.fail('sign: unverified signature');
            else return p.jump(onSuccess, message); // jump onFail
          } else {
            unpackedMessage = nacl.to_hex(unpackedMessage);
            unpackedMessage = baseCode.recode('hex', 'utf-8', unpackedMessage);
            return p.jump(signature || 1, unpackedMessage); // jump onSuccess if message can be decoded by public key
          }
        }
      */

  } else if (typeof message === 'string' && typeof key === 'string' && key.length === 128) { // sign message
    const secretKeyNACL = nacl.from_hex(key);
    const messageHex = baseCode.recode('utf-8', 'hex', message);
    const messageNACL = nacl.from_hex(messageHex);
    const messageSign = nacl.crypto_sign_detached(messageNACL, secretKeyNACL);
    const messageResult = nacl.to_hex(messageSign);
    return p.next(messageResult);
  } else return p.fail('sign: Sign error! '+message+' '+key);
};

exports.tests = {
  sign: [
    '#TODO: this method needs work!',
    'done $OK',
    'keys',
    'poke {secretKey:secretKey,publicKey:publicKey}',
    'data message',
    'sign $secretKey',
    'sign $publicKey $ 2 1',
    'fail',
    'done $OK'
  ]
};

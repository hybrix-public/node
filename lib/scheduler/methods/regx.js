/**
   * Test the data against the regex pattern
   * @category Array/String
   * @param {String} [variable=data] - the variable to perform the regex on
   * @param {String} pattern - the regex pattern
   * @param {Array} [flags=[]] - optional flags like ig (case insensitive, global search)
   * @param {Integer} [onMatch=1]
   * @param {Integer} [onNoMatch=1]
   * @example
   * data 'applepie'
   * regx '^apple' 1 2                  // match when start of the string contains 'apple'
   * regx variable '^apple' 1 2        // match when start of variable contains 'apple'
   * regx variable '^apple' ['i'] 1 2  // match when start of variable contains 'apple' or 'Apple' (case insensitive)
   * done 'Match!'
   * done 'No Match'
   */
exports.regx = data => async function (p, a, b, c, d, e) {
  let value, pattern, flags, onMatch, onNoMatch;
  if (typeof a === 'string' && typeof b === 'string') { // use a var
    [pattern, flags, onMatch, onNoMatch] = [b, c, d, e];
    const result = p.peek(a);
    if (result.e) return p.fail(`regx: could not peek variable '${a}'`);
    value = result.v;
  } else {
    [pattern, flags, onMatch, onNoMatch] = [a, b, c, d];
    value = data;
  }
  if (!isNaN(flags)) {
    [onMatch, onNoMatch] = [flags, onMatch];
    flags = '';
  } else {
    flags = flags.join();
  }

  const pattern2 = pattern; // TODO escape regex characters
  let regex;
  try {
    regex = new RegExp(pattern2, flags);
  } catch (e) {
    return p.fail(`regx: '${e}'`);
  }
  if (regex.test(value)) return p.jump(onMatch || 1, data);
  else return p.jump(onNoMatch || 1, data);
};

exports.tests = {
  regx1: [
    'data applepie',
    'regx ^apple 1 2',
    "done '$OK'",
    'fail'
  ],
  regx2: [
    'data applepie',
    'poke tmp',
    'regx tmp ^apple 1 2',
    "done '$OK'",
    'fail'
  ]

};

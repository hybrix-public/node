const {pushToApiQueue} = require('./curl.js');
const INTERVAL = 500;

function call (data, p, host, xpath, method, interval, strict, progReport, onSuccess, onFailure) {
  if (p.hasStopped()) return;
  pushToApiQueue(data, p, host, xpath, method, {}, {ignoreError: true, timeout: p.getTimeOut()}, response => {
    if (p.hasStopped()) return;
    if (typeof response === 'object') {
      if (response.id === 'id') {
        if (response.hasOwnProperty('timeout') && p.hasOwnProperty('setTimeout')) p.setTimeOut(response.timeout);
        if (progReport && response.hasOwnProperty('progress') && p.hasOwnProperty('prog')) p.prog(response.progress);
        setTimeout(() => call(null, p, host, `/p/${response.data}`, 'GET', interval, strict, progReport, onSuccess || 1, onFailure || 1), interval);
      } else if (response.error === 0) {
        if (response.stopped) return p.jump(onSuccess || 1, response.data); else {
          if (progReport && response.hasOwnProperty('progress') && p.hasOwnProperty('prog')) p.prog(response.progress);
          setTimeout(() => call(null, p, host, `/p/${response.id}`, 'GET', interval, strict, progReport, onSuccess || 1, onFailure || 1), interval);;
        }
      } else if (typeof onFailure === 'undefined') {
        p.help(response.help);
        return p.stop(response.error, null);
      } else return p.jump(onFailure || 1, response.data);
    } else if (strict) p.jump(onFailure || 1, response.data);
    else return p.jump(onSuccess || 1, response);
  },
  error => p.fail(error)
  );
}

/**
   * Route a path through hybrixd and pass the result to the next step. /api/help for API information.
   * @param {String} path - Provide the routing path
   * @param {String} [host] - Provide the host, defaults to self, or object to set interval and host
   * @param {String} [onSuccess=1] - Amount of steps to jump on success
   * @param {String} [onFailure] - Amount of steps to jump on failure
   * @example
   * rout '/asset/dummy/balance/__dummyaddress__'    // retrieve the balance for the dummy asset
   * rout '/asset/dummy/fee' @success @failure       // retrieve the fee for the dummy asset, on success jump to @success, else jump to @failure
   */
exports.rout = data => async function (p, xpath, ...args) {
  let obj = {}, host, interval, method, strict, progReport, onSuccess, onFailure;
  if (args.length === 0) onSuccess = 1;
  else if (typeof args[0] === 'object') {
    [obj, onSuccess, onFailure] = args;
  } else if (isNaN(args[0]) && (typeof args[0] === 'string' || args[0] instanceof Array)) {
    if (typeof args[1] === 'object') {
      [host, obj, onSuccess, onFailure] = args;
    } else [host, onSuccess, onFailure] = args;
  } else [onSuccess, onFailure] = args;

  if (typeof obj.host !== 'undefined') host = obj.host;
  if (typeof obj.method !== 'undefined') method = obj.method;
  if (typeof obj.interval !== 'undefined') interval = obj.interval;
  if (typeof obj.progress !== 'undefined') progReport = obj.progress;
  if (typeof obj.strict !== 'undefined') strict = obj.strict;

  if (typeof onSuccess === 'undefined') onSuccess = 1;
  if (isNaN(onSuccess)) return p.fail('rout: Expected jump for onSuccess.');
  if (isNaN(onFailure) && typeof onFailure !== 'undefined') return p.fail('rout: Expected jump for onFailure.');

  if (typeof interval !== 'undefined') {
     if (interval < 100 || interval > 60000) return p.fail('rout: Interval must be at least 100ms and at most 60000ms');
  } else interval = INTERVAL;

  if (host) return call(data, p, host, xpath, method, interval, strict, progReport, onSuccess || 1, onFailure || 1);
  else {
    return p.rout(xpath, data,
      data => p.jump(onSuccess || 1, data),
      error => {
        if (typeof onFailure === 'undefined') return p.fail(error);
        else return p.jump(onFailure || 1, error);
      },
      true);
  }
};

exports.tests = {
  rout0: [
    'rout /a/dummy/factor 1 3',
    'flow {8:1} 2',
    "done '$OK'",
    'fail'
  ],
  rout1: [
    'rout /a/non_existing_asset/factor 2 1',
    "done '$OK'",
    'fail $NOT_OK:$0:$'
  ],
  rout2: [
    'rout /s/web-wallet/non_existing_file 2 1',
    "done '$OK'",
    'fail'
  ],
  rout3: [
    '# fonts is a directory should fail but not crash',
    'rout /s/web-wallet/fonts 2 1',
    "done '$OK'",
    'fail'
  ]
};

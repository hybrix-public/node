const crypto = require('crypto');
const djb2 = require('../../../common/crypto/hashDJB2');
const sha256 = require('../../../common/node_modules/js-sha256');

/**
  * Create a sha256 hash from an input string.
  * @category Cryptography
  * @param {String} [method='sha256'] - Method to use for hashing, defaults to SHA256. Available: md5, djb2, sha244, sha256
  * @example
  *  hash          // input: "this_is_my_data", returns hash: 2f4e433b8ad7290a14683ed94b34b96daf129eb704d65ed0cdc1a9fec3d73084
  *  hash sha256   // input: "this_is_my_data", returns hash: 2f4e433b8ad7290a14683ed94b34b96daf129eb704d65ed0cdc1a9fec3d73084
  *  hash sha244   // input: "this_is_my_data", returns hash: 5c9a89e377c61881a6eac897e7308ceab6ad5587755db1d5ce82b2b7
  *  hash djb2     // input: "this_is_my_data", returns hash: 49B87777
  */
exports.hash = ydata => async function (p, method) {
  let hash;
  switch (method) {
    case 'md5':
      hash = crypto.createHash('md5').update(ydata).digest('hex');
      break;
    case 'djb2':
      hash = djb2.hash(ydata);
      break;
    case 'sha244':
      hash = sha256.sha224(ydata);
      break;
    default:
      hash = sha256.sha256(ydata);
      break;
  }
  p.next(hash);
};

exports.tests = {
  hash: [
    "data 'this_is_my_data'",
    'hash',
    "flow '2f4e433b8ad7290a14683ed94b34b96daf129eb704d65ed0cdc1a9fec3d73084' 1 2",
    'done $OK',
    'fail'
  ],
  hash1: [
    "data 'this_is_my_data'",
    'hash sha256',
    "flow '2f4e433b8ad7290a14683ed94b34b96daf129eb704d65ed0cdc1a9fec3d73084' 1 2",
    'done $OK',
    'fail'
  ],
  hash2: [
    "data 'this_is_my_data'",
    'hash sha244',
    "flow '5c9a89e377c61881a6eac897e7308ceab6ad5587755db1d5ce82b2b7' 1 2",
    'done $OK',
    'fail'
  ],
  hash3: [
    "data 'this_is_my_data'",
    'hash djb2',
    "flow '49B87777' 1 2",
    'done $OK',
    'fail'
  ]
}

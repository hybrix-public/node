/**
   * Quits the interactive shell, only available in interactive cli mode.
   * @category Interactive
   * @example
   * quit
   */
exports.quit = ydata => async function (p) {
  return p.fail('quit: only available in interactive cli mode');
};

exports.tests = {
  quit: [
    'done $OK'
  ]
}

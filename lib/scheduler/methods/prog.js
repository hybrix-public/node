/**
  * Set progress of parent process directly, or incrementally in a set amount of time.
  * @category Process
  * @param {Number} [start] - Current progress amount, specified as a number between 0 and 1, or the starting value.
  * @param {Number} [end] - When specified, sets progress from the start value, finishing at the end value.
  * @param {Number} [second] - Amount of seconds to automatically set progress.
  * @example
  * prog           // return progress data
  * prog 0.5       // progress is set to 0.5 (which means 50%)
  * prog 2 5       // progress is set to 0.4 (which means 40%, or step 2 of 5)
  * prog 0 1       // progress is automatically set ranging from 0 to 1, in a matter of 5 seconds
  * prog auto      // restore automatic progress reporting (alternative notation)
  */

exports.prog = data => async function (p, start, finish, seconds) {
  if (typeof start === 'undefined') {
    const step = p.getStep()+1;
    const steps = p.getSteps();
    const progress = step/steps;
    data = {progress, step, steps};
  } else if (start == 'auto') {
    p.progTimeouts = undefined;
    p.setAutoProg(true);
  } else {
    if (typeof seconds === 'undefined') {
      p.progTimeouts = undefined;
      p.prog(start, finish);
    } else {
      seconds = Math.min(seconds, p.getTimeOut());
      finish = Math.min(finish, 1);

      p.prog(start);

      p.progTimeouts = [];
      for (let i = 0; i <= (seconds * 2); i = i + 0.5) {
        let progressvalue = start + (((finish - start) / (seconds * 2)) * i);
        if (progressvalue >= 1) { progressvalue = 0.999; }
        p.progTimeouts[i] = setTimeout(function () {
          p.prog(this.progressvalue);
        }.bind({progressvalue}), i * 500);
      }
    }
  }
  p.next(data);
};

exports.tests = {
  prog: [
    '# this method cannot be tested automatically',  
    'done $OK'
  ]
}

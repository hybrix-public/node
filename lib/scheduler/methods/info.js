/**
   * Logs data to error output (information).
   * @param {String} messages... - The messages you want to log.
   * @category Logging
   * @example
   * info 'An informative message'       // logs "[i] An informative message"
   */
exports.info = data => async function (p, messages_) {
  const messages = arguments.length === 1
    ? [data]
    : Array.from(arguments).slice(1); // drop p
  global.hybrixd.logger.apply(global.hybrixd.logger, [['info', p.getRecipe().id], ...messages]);
  return p.next(data);
};

exports.tests = {
  info: [
    'done $OK'
  ]
}

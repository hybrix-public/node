const math = require('../math');
const vars = require('../vars');

/**
   * Loop back to position or label based on criteria.
   * @category Flow
   * @param {String} position         - Position to loop back to.
   * @param {String} variable         - Variable to store the loop count.
   * @param {String} condition        - Condition that keeps the loop going. If given an array or object, iterates over the amount of elements.
   * @param {String} [initValue=1]    - Value to initialize the loop.
   * @param {String} [stepOperation=+1] - Optional: How the loop variable is iterated.
   * @example
   * loop @loopStart loopCount <5          // loops back to label loopStart five times
   * loop @loopStart loopCount <10 0 +2    // loops back to label loopStart ten times, starting from zero, iterating by +2
   */
exports.loop = data => async function (p, jumpTo, loopVar, condition, initVal, stepOperation) {
  // TODO make position optional (could default to -1)
  // TODO make variable optional (could create a tmp variableName)

  // TODO check if jumpTo is positive integer
  // TODO check if loopVar is string
  // TODO check if condition is string (starting with comparison operator)
  // TODO check if initVal is number
  // TODO check if stepOperator starts with operator
  if (isNaN(jumpTo)) p.fail('loop: illegal jump location');
  if (typeof initVal === 'undefined') initVal = '1';
  if (typeof stepOperation === 'undefined') stepOperation = '+1';

  let loopVal = vars.peek(p, loopVar);
  if (loopVal.e > 0) loopVal = initVal;
  else {
    const loopStepResult = math.calc(loopVal.v + stepOperation, p);
    if (loopStepResult.error) return p.fail(loopStepResult.error);
    loopVal = loopStepResult.data;
  }
  let loopCondition;
  if(typeof condition === 'string' || !isNaN(condition)) {
    loopCondition = math.calc(loopVal + condition, p);
  } else {
    loopCondition = math.calc(`${loopVal}<${condition.length}`);
  }
  if (loopCondition.error) return p.fail(loopCondition.error);
  if (loopCondition.data === true) {
    p.poke(loopVar, loopVal);
    return p.jump(jumpTo, data);
  } else {
    p.poke(loopVar, initVal);
    return p.next(data);
  }
};

exports.tests = {
  loop: [
    'poke varCountA 0',
    'poke varCountB 0',
    'poke loopCount 0',
    '@loopStart',
    'with varCountA math +1',
    'with varCountB math +$loopCount',
    'loop @loopStart loopCount <8 0 +2',
    'true "$varCountA==4" 1 2',
    'true "$varCountB==12" 2 1',
    'fail',
    'done $OK'
  ]
}

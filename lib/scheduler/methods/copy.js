const vars = require('../vars');

/**
   * Copy a variable to another variable. The data stream is left untouched.
   * @category Variables/Objects
   * @param {String} source - Variable to copy.
   * @param {String} target - Variable(s) to store copies in.
   * @example
   * copy variableA variableB                  // copy variableA to variableB
   * copy variableA variableB variableC        // copy variableA to variableB and variableC
   */
exports.copy = data => async function (p, variable, properties) {
  for (let i = 2; i < Object.keys(arguments).length; i++) {
    const result = vars.peek(p, variable);
    if (result.e > 0) {
      result.v = undefined;
    }
    vars.poke(p, arguments[i], result.v);
  }
  p.next(data);
};

exports.tests = {
  copy: [
    "poke copyA 'testData'",
    'copy copyA copyB copyC',
    "flow copyB 'testData' 1 2",
    "flow copyC 'testData' 2 1",
    'fail',
    'done $OK'
  ]
}

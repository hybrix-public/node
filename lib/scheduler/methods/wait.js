/**
   * Wait a specified amount of time before continuing the process.
   * @param {Number} millisecs - Amount of milliseconds to wait.
   * @category Process
   * @example
   * wait 2000            // wait for two seconds, before continuing process
   */
exports.wait = data => async function (p, millisecs) {
  if (millisecs) { // if no millisecs are given, this proces will wait indefinitely
    p.setTimeOut(p.getTimeOut() + millisecs);
    setTimeout((p) => {
      return p.next(data);
    }, millisecs, p);
  }
  return;
};

exports.tests = {
  wait: [
    "data passthrough",
    'wait 1000',
    'flow passthrough 2 1',
    'fail',
    'done $OK'
  ]
}

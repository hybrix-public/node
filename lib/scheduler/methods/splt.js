/**
   * Explode a string into an array, split by separator.
   * @category Array/String
   * @param {String} [separator=' '] - Separator character to use for splitting.
   * @param {String} [elements=undefined] - Elements to select from splitted string.
   * @example
   * splt             // input: "This is nice.", output: ["This","is","nice."]
   * splt ','         // input: "Some,list,of,stuff", output: ["Some","list","of","stuff]
   * splt ',' 1       // input: "Some,list,of,stuff", output: "list"
   * splt ',' [1,2]   // input: "Some,list,of,stuff", output: ["list","of"]
   */
exports.splt = ydata => async function (p, separator, elements) {
  if (typeof separator === 'undefined') { separator = ''; }
  let splitted;
  if (typeof ydata === 'string') {
    splitted = ydata.split(separator);
  } else {
    splitted = [];
  }
  let result = [];
  if (typeof elements !== 'undefined') {
    if (!isNaN(elements)) {
      result = splitted[elements];
      if (typeof result === 'undefined' || result === null) {
        result = '';
      }
    }
    if (elements instanceof Array) {
      for (let key = 0; key < elements.length; key++) {
        result.push(splitted[elements[key]]);
      }
    }
  } else {
    result = splitted;
  }
  p.next(result);
};

exports.tests = {
  splt: [
    'data "1,2,3,4"',
    'splt ,',
    'data "$"',
    'flow "[\"1\",\"2\",\"3\",\"4\"]" 2 1',
    'fail',
    'done $OK'
  ],
  splt1: [
    'data "1,2,3,4"',
    'splt , 1',
    'data "$"',
    'flow "2" 2 1',
    'fail',
    'done $OK'
  ],
   splt2: [
    'data "1,2,3,4"',
    'splt , [1,2]',
    'data "$"',
    'flow "[\"2\",\"3\"]" 2 1',
    'fail',
    'done $OK'
  ] 
}

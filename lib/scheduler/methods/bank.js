const fs = require('fs');
const util = require('util');

const router = require('../../../lib/router/router');
const Hybrix = require('../../../interface/hybrix-lib.nodejs.js');
const spawnDeterministic = '../modules/deterministic/spawnDeterministic.js';

const rout = sessionID => (query, data, callback) => {
  router.rout({url: query, data, sessionID}, result => callback(JSON.stringify(result)), error => callback(null));
};

const hostname = 'http://127.0.0.1:1111/'; // dummy value as we are using rout to rout internally
const fsExists = util.promisify(fs.exists);
const fsReadFile = util.promisify(fs.readFile);

const handleLogin = async function (p, login, data, method, dataCallback, errorCallback) {
  if (typeof login !== 'object' || login === null) return errorCallback('bank: expected login to be an object!');
  if (typeof data !== 'object' || data === null) return errorCallback('bank: expected data to be an object!');

  const loginActions = [{host: hostname}, 'addHost'];
  if (login.hasOwnProperty('auth')) {
    if (login.auth === 'node') {
      const permissions = p.getRecipe().permissions;
      if (p.getSessionID() === 1 || (typeof permissions === 'object' && permissions !== null && permissions.nodeKeys === true)) {
        if (p.getSessionID() !== 1 && ['getKeys', 'getPrivateKey'].includes(method)) return errorCallback(`bank: method '${method}' not allowed for node authentication!`);
        loginActions.push({publicKey: global.hybrixd.node.publicKey, secretKey: global.hybrixd.node.secretKey}, 'session');
      } else return errorCallback('bank: no permission for authentication method \'node\'!');
    } else return errorCallback('bank: unknown authethication method');
  } else if (login.hasOwnProperty('username') && login.hasOwnProperty('password')) {
    loginActions.push(login, 'session');
  } else if (login.hasOwnProperty('publicKey') && login.hasOwnProperty('secretKey')) {
    loginActions.push(login, 'session');
  } else if (login.hasOwnProperty('accountFile')) {
    const existsAccountFile = await fsExists(login.accountFile);
    if (existsAccountFile) {
      try {
        const content = await fsReadFile(login.accountFile);
        const loginData = JSON.parse(content);
      } catch (e) {
        return errorCallback('bank: Failed to parse accountFile!');

        /* TODO importAccount = importAccount.replace(' ', ',').replace('|', ',').replace('\n', ',').split(',');
        objectOrUsernameOrDirective = importAccount[0];
        password = importAccount[1]; */
      }
      loginActions.push(loginData, 'session');
    } else return errorCallback('bank: accountFile does not exist!');
  } else if (login.hasOwnProperty('importKey') || login.hasOwnProperty('importKeyFile')) {
    if (!data.hasOwnProperty('symbol')) return errorCallback('bank: expected data to have a symbol property!');
    let privateKey;
    if (login.hasOwnProperty('importKey')) privateKey = login.importKey;
    else {
      if (fs.existsSync(login.importKeyFile)) {
        privateKey = await fsReadFile(privateKey);
      } else return errorCallback('bank: importKeyFile does not exist!');
    }
    const symbol = data.symbol;
    loginActions.push(
      {username: 'DUMMYDUMMYDUMMY0', password: 'DUMMYDUMMYDUMMY0'}, 'session',
      {symbol}, 'addAsset', // first add asset so we can inject the private keys
      {symbol: symbol, privateKey}, 'setPrivateKey'
    );
  } else {
    return errorCallback('bank: login object does not contain valid login details!');
  }

  if (loginActions) {
    dataCallback(loginActions);
  } else {
    errorCallback('bank: no actions specified!');
  }
}

const sequentialActions = async function (p, loginSteps, data, method, onSuccess, onFailure) {
	
  const hybrixd = new Hybrix.Interface({
    local: {rout: rout(p.getSessionID())},
    storage: {key:'hybrixd'},
    deterministicExecutable: spawnDeterministic, // to sandbox and parallelize we want to create a seperate executable for each deterministic process
    cachedClientModules, // cache the clientModules accross sessions (not a problem as those are deterministic)
    cachedClientBlobs
  });

  if (!hybrixd.hasOwnProperty(method)) p.fail(`bank: method '${method}' is unknown`);
  hybrixd.sequential(
    [
      ...loginSteps, // handle the login and setting of keys if applicable
      data, method //  execute the method
    ],
    data => {
      p.jump(onSuccess||1, data);
    },
    error => {
      p.jump(onFailure||1, 'bank: ' + JSON.stringify(error));
    },
    progress => p.prog(progress)
  );
}

// Cache for deterministic module, no need for sandbox as they are deterministic
const cachedClientModules = {}
const cachedClientBlobs = {}

/**
   * Banking functions to control hybrix accounts from Qrtz.
   * bank uses the hybrix-jslib methods to check balances, manage keys and perform transactions.
   * See https://api.hybrix.io/help/hybrix-jslib#reference for all methods.
   *
   * Note: if you want to use deterministic transactions client-side, please use the
   * hybrix-jslib for this purpose.
   * For security, this command can only be used as sessionID root.
   * @param {String} method - The propertiesOrKey of the hybrix account.
   * @param {Object} [login] -
   * @param {Object} [login.username] - Use a username password pair to create a session
   * @param {Object} [login.password] -  Use a username password pair to create a session
   * @param {Object} [login.publicKey] - Use a publicKey secretKey pair to create a session
   * @param {Object} [login.secretKey] - Use a publicKey secretKey pair to create a session
   * @param {Object} [login.accountFile] - Import username password object from a json file
   * @param {Object} [login.importKey] - Use a privateKey for the given asset to import directly instead of using a hybrix session
   * @param {Object} [login.importKeyFile] - Import privateKey from a file for the given asset to import directly instead of using a hybrix session
   * @param {Object} [login.auth] - Use {auth:node} to login with node keys. Requires permissions: {nodeKeys:true}
   * @param {Int} [onSuccess=1] -
   * @param {Int} [onFailure] -
   * @example
   * bank createAccount                              // create new account credentials
   *
   * data {symbol:btc}
   * bank getBalance $login                          // Get the account balance of Bitcoin
   *
   * data {symbol:nxt}
   * bank getPublicKey $login                        // Get the account public key of NXT
   *
   * data {symbol:flo}
   * bank getSecretKey $login                        // Get the account secret/private key of Florincoin
   *
   * data {symbol:eth}
   * bank getKeys $login                             // Get the account keys in an object of Ethereum
   *
   * data {symbol:doge, amount: 0.1, target}
   * bank transaction $login                         // Send 0.1 Dogecoin to address $target
   *
   * data {symbol:lsk, amount: 0.1, target}
   * bank rawTransaction $login                      // Return the raw transaction bytecode for sending 0.1 Lisk to address $target, without pushing it to the network.
**/
exports.bank = data => async function (p, ...args) {
  if(args.length>2 && typeof args[2] === 'object') {
    [method, login, data, onSuccess, onFailure] = args;
  } else if(args.length>1 && isNaN(args[1])) {
    [method, login, onSuccess, onFailure] = args;
  } else {
    [method, onSuccess, onFailure] = args;
  }

  if (typeof method !== 'string') return p.fail('Expected a string method!');

  const loginSteps = [];
  if (method === 'createAccount') {
    sequentialActions(p, loginSteps, data, method, onSuccess, onFailure);
  } else {
    if (typeof login === 'undefined') return p.fail('Expected a login object');
    handleLogin(p, login, data, method,
      loginActions => {
        loginSteps.push(...loginActions);
        sequentialActions(p, loginSteps, data, method, onSuccess, onFailure);
      },
      loginError => {
        p.fail(loginError);
      }
    );
  }
};

exports.tests = {
  bank: [
    'data {symbol:dummy}',
    "bank getBalance {username:'DUMMYDUMMYDUMMY0',password:'DUMMYDUMMYDUMMY0'} 1 2",
    'ship 1 1 2 2',
    'done $OK',
    'fail'
  ],
  bank1: [
    'data {symbol:dummy, amount: 1, target: _dummyaddress_}',
    "bank transaction {username:'DUMMYDUMMYDUMMY0',password:'DUMMYDUMMYDUMMY0'} 1 2",
    "flow 'TX01' 1 2",
    'done $OK',
    'fail'
  ]
};

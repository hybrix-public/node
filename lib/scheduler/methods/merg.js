const math = require('../math');

/**
   * Merge objects.
   * @category Transformers
   * @param {String|Object} [mergeOperator] - Operator to perform merge with
   * @example
   *  merg                 // input: [{a,1},{b:2}], output: {a:1,b:2}
   *  merg                 // input: [{a:1,b:2},{a:2}], output: {a:2,b:2}
   *  merg +               // input: [{a:1},{a:2}], output: {a:3}
   *  merg {a:+,b:-}       // input: [{a:1,b:1},{a:2,b:2}], output: {a:3,b:-1}
   *  merg                 // input: ['A','B'] ['string',{a:'hello',b:'world'}], output: {'A':'string','B':{a:'hello',b:'world'}}
   */
exports.merg = data => async function (p, mergeOperator) {
  if (Array.isArray(data) === false) return p.fail('merg: expects array of objects');
  const mergedObject = {};
  if (data.length === 2 && (data[0] instanceof Array) && (data[1] instanceof Array)) {
    for (let key = 0; key < data[0].length; key++) {
      if(typeof data[0][key] !== 'string') return p.fail('merg: invalid key assignment when merging arrays to object');
      mergedObject[data[0][key]] = data[1][key];
    }
  } else {
    for (let object of data) {
      if (Array.isArray(object) === false && (typeof object !== 'object' || object === null)) return p.fail('merg: expects array of objects, or array of two arrays');
      if (mergeOperator) {
        for (let key in object) {
          if (!mergedObject.hasOwnProperty(key)) mergedObject[key] = object[key];
          else {
            let mergeSubOperator;
            if (typeof mergeOperator === 'string') mergeSubOperator = mergeOperator;
            else if (typeof mergeOperator === 'object' && mergeOperator !== null) {
              if (mergeOperator.hasOwnProperty(key)) mergeSubOperator = mergeOperator[key];
              else {
                mergedObject[key] = object[key]; // default operation is overwrite
                continue;
              }
            } else return p.fail(`merg: expected string or object mergeOperator`);

            const mergeOperation = mergedObject[key] + mergeSubOperator + object[key];
            const result = math.calc(mergeOperation, p);
            if (result.error) return p.fail(result.error);
            mergedObject[key] = result.data;
          }
        }
      } else Object.assign(mergedObject, object);
    }
  }
  return p.next(mergedObject);
};

exports.tests = {
  merg: [
    'data [{a:1,b:2},{a:2}]',
    'merg',
    'data "$"',
    'flow "{\"a\":2,\"b\":2}" 2 1',
    'fail',
    'done $OK'
  ],
  merg1: [
    'data [{a:1,b:1},{a:2,b:2}]',
    'merg {a:+,b:-}',
    'data "$"',
    'flow "{\"a\":3,\"b\":-1}" 2 1',
    'fail',
    'done $OK'
  ]
}

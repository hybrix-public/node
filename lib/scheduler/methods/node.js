/**
   * Return information about the hybrix node
   * @category Cryptography
   * @param {String} [request] - The information you want to request
   * @example
   * node                    // returns the public key / node ID
   */
exports.node = () => async function (p, request) {
  let info = global.hybrixd.node.publicKey;
  p.next(info);
};

exports.tests = {
  node: [
    'done $OK'
  ]
}

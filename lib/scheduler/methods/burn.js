const modules = require('../../modules');

/**
   * Delete a storage item
   * @category Storage
   * @param {String} key - key for the item to delete
   * @example
   * burn myFavoriteColor 1 2                        // deletes the item under key 'myFavoriteColor' - if successful jump 1, else 2
   * burn myFavoriteColor:${publicKey}:${signature}  // deletes the item under key 'myFavoriteColor' only if the key is signed by the originator
   */
exports.burn = data => async function (p, key, success, failure) {
  if (typeof key !== 'string') return p.fail('burn: Expecting property \'key\' of type string!');
  const done = data => p.next(data);
  const fail = error => p.fail(error);
  const separatorIndexA = key.indexOf(':');
  let burnObject;
  if(separatorIndexA > 0) {
    const separatorIndexB = key.indexOf(':',separatorIndexA+1);
    if (separatorIndexB > separatorIndexA) {
      const keyTruncated = key.substr(0,separatorIndexA);
      const publicKey = key.substr(separatorIndexA+1,(separatorIndexB-separatorIndexA-1));
      const signature = key.substr(separatorIndexB+1);
      burnObject = {key: encodeURIComponent(keyTruncated), publicKey, signature};
    } else return p.fail('burn: Expecting both public key and signature!');
  } else {
    burnObject = {key: encodeURIComponent(key)};
  }
  modules.module.storage.main.burn({done, fail}, burnObject);
};

exports.tests = {
  burn: [
    'save testBurn',
    'burn testBurn',
    'seek testBurn 2 1',
    'done $OK',
    'fail'
  ]
};

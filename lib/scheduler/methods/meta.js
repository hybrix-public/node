const modules = require('../../modules');

/**
   * Retrieve the storage metadata based on a key.
   * @category Storage
   * @param {String} key - Key for which to get storage metadata.
   * @example
   * meta myFavoriteColor  // retrieve the meta data for myFavoriteColor
   */
exports.meta = data => async function (p, input, success, failure) {
  const key = input || data;
  const done = data => { p.jump(isNaN(success) ? 1 : success || 1, data); };
  const fail = () => { p.jump(isNaN(failure) ? 1 : failure || 1, data); };
  modules.module['storage'].main.meta({done, fail}, {key: encodeURIComponent(key)}); // TODO ugly global hack
};

exports.tests = {
  meta: [
    'burn testMeta',
    'save testMeta 123',
    'meta testMeta',
    'flow .hash "a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3" 1 2',
    'flow .size "3" 2 1',
    'fail',
    'done $OK'
  ]
};

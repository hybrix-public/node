/**
   * Flip reverse the contents of an array or string.
   * @category Array/String
   * @example
   * flip   // input: "hello world", output: 'dlrow olleh'
   * flip   // input: ['a','b','c'], output: ['c','b','a']
   */
exports.flip = ydata => async function (p) {
  let str = false;
  if (typeof ydata === 'string') {
    str = true;
    ydata = ydata.split('');
  }
  if (ydata instanceof Array) {
    ydata = ydata.reverse();
    if (str) ydata = ydata.join('');
    p.next(ydata);
  } else p.fail('flip: expects array or string');
};

exports.tests = {
  flip: [
    'data abcde',
    'flip',
    "flow 'edcba' 2 1",
    'fail',
    'done $OK'
  ],
  flip1: [
    'data [1,2,3,4,5]',
    'flip',
    "data '$'",
    "flow '[5,4,3,2,1]' 2 1",
    'fail',
    'done $OK'
  ]
};

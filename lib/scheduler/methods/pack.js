const LZString = require('../../../common/crypto/lz-string');

/**
  * Compress and serialize data into an URL-safe format.
  * @category Transformers
  * @example
  * pack       // serializes and compresses the data stream into an URL-safe format
  */
exports.pack = data => async function (p) {
  const ydata = LZString.compressToEncodedURIComponent(JSON.stringify(data));
  p.next(ydata);
};

exports.tests = {
  pack: [
    'data "abcdefgabcdefg"',
    'pack',
    'flow "EQQwRgxgJgpgZgc3NeDhA" 2 1',
    'fail',
    'done $OK'
  ]
}

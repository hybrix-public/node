/**
   * Provides help on provided qrtz command, only available in interactive cli mode.
   * @category Interactive
   * @param {String} [func] - function to find help for
   * @example
   * help done
   */
exports.help = ydata => async function (p) {
  return p.fail('help: only available in interactive cli mode');
};

exports.tests = {
  help: [
    'done $OK'
  ]
}

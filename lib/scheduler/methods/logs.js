/**
   * Logs data to log output (progress).
   * @param {String} [messages...] - The messages you want to log.
   * @category Logging
   * @example
   * logs 'My progress'       // logs "[.] My progress"
   */
exports.logs = data => async function (p, messages_) {
  const messages = arguments.length === 1
    ? [data]
    : Array.from(arguments).slice(1); // drop p
  global.hybrixd.logger.apply(global.hybrixd.logger, [['module', p.getRecipe().id], ...messages]);
  return p.next(data);
};

exports.tests = {
  logs: [
    'done $OK'
  ]
}

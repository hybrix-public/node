exports.createLink = createLink;
exports.call = call;

const ports = {
  socks: 1080,
  tor: 9050,
  i2p: 4444,
  sockstls: 1080,
  tortls: 9050,
  i2ptls: 4444
};

exports.ports = ports;

const conf = require('../../conf/conf');
const { Client } = require('../rest');
const { SocksProxyAgent } = require('socks-proxy-agent');

function call (link, host, qpath, args, method, dataCallback, errorCallback) {
  const methodLowerCase = (typeof method === 'string') ? method.toLowerCase() : 'get';
  const [prefix, hostNameAndPort] = host.split('://');
  targetPrefix = prefix.substr(-3) === 'tls' ? 'https' : 'http';
  const target = `${targetPrefix}://${hostNameAndPort}${qpath}`;
  link[methodLowerCase](target, args || {}, dataCallback, errorCallback);
}

function createLink (APIrequest, host, APIhosts, dataCallback, errorCallback) {
  const options = {};
  if (APIrequest.user) options.user = APIrequest.user;
  if (APIrequest.pass) options.password = APIrequest.pass;

  const [prefix, hostNameAndPort] = host.split('://');
  const proxyHostName = conf.get('socks.proxyhost');
  const proxyPort = conf.get('socks.proxyport', true) || ports[prefix]; // use silent for fallback
  const proxyUser = conf.get('socks.proxyuser', true) || undefined; // silent ignore unset configuration
  const proxyPass = conf.get('socks.proxypass', true) || undefined; // silent ignore unset configuration
  const socksConfig = {
    hostname: proxyHostName,
    port: proxyPort,
    userId: proxyUser,
    password: proxyPass
  };
  options.agent = new SocksProxyAgent(socksConfig);

  const link = new Client(options);
  dataCallback(link);
}

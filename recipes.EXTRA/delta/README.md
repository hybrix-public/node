# Delta Recipes

This directory contains example delta recipes which can override standard
hybrix recipes by placing them in: HYBRIXD_ROOT/var/delta

Overrides make it possible to use third-party blockchain service providers
which makes running a public node much easier.


// (C) 2019 Internet of Coins / Joachim de Koning
// hybrixd module - evm-interpreter/module.js
// Module containing an Ethereum Virtual Machine (EVM) interpreter and decompiler,
// along with several other utils for programmatically extracting information from bytecode.

// required libraries
const { EVM } = require('evm');
const EthereumCommon = require("ethereumjs-common").default;
const EthereumTx = require('ethereumjs-tx').Transaction;
const txDecoder = require('ethereum-tx-decoder');
const ethers = require('ethers');

const fs = require('fs');

const id = 'evm-interpreter';

// exports
exports.get = get;
exports.parse = parse;
exports.decompile = decompile;
exports.decode = decode;

// get functions
function get (proc) {
  let output;
  try {
    const func = proc.command[1];
    const bytecode = proc.command[2];
    const evm = typeof bytecode !== 'string' ? null : new EVM(bytecode);
    switch (func) {
      case 'opcodes':
        output = evm.getOpcodes();
        break;
      case 'functions':
        output = evm.getFunctions();
        break;
      case 'events':
        output = evm.getEvents();
        break;
      case 'destinations':
        output = evm.getJumpDestinations();
        break;
      case 'swarmhash':
        output = evm.getSwarmHash();
        break;
      case 'gas':
        const opCodes = evm.getOpcodes();
        output = 0;
        for (let opCode of opCodes) {
          if (opCode.fee) output = output + opCode.fee;
        }
        break;
      case 'gastable':
        const filePath = 'modules/' + id + '/gastable.json';
        if (fs.existsSync('../' + filePath)) output = JSON.parse(fs.readFileSync('../' + filePath));
        break;
      default:
        return proc.fail('Unknown method' + func);
    }
  } catch (e) {
    return proc.fail(e);
  }

  return typeof output !== 'undefined' && output !== null
    ? proc.done(output)
    : proc.fail(output);
}
// other functions
function parse (proc) {
  const bytecode = proc.command[1];
  let result;
  try {
    const evm = typeof bytecode !== 'string' ? null : new EVM(bytecode);
    result = evm.parse();
  } catch (e) {
    return proc.fail(e);
  }
  return proc.done(result);
}

function decompile (proc) {
  const bytecode = proc.command[1];
  let result;
  try {
    const evm = typeof bytecode !== 'string' ? null : new EVM(bytecode);
    result = evm.decompile();
  } catch (e) {
    return proc.fail(e);
  }
  return proc.done(result);
}

function decode (proc) {
  const serializedTransaction = proc.command[1];
  let result;
  try {
    
    // TODO
    // ./hybrixd /e/evm-interpreter/decode/0xf8aa27840ee6b28083029040940005b4cf1bbf817c5d10fce740a1a2474883134e80b844a9059cbb0000000000000000000000006cabea6cbe4c1e845b6954eca970854a0575b6ca000000000000000000000000000000000000000000000000000000000000000181d3a04ea60c5bbf414c4562c9443120e20440a65a6d3bcafd871eb70c164d370d4fd6a040299eb5444d8778fd184ae6f2f046f108883ddfa05f0465cc94d826b4bdd3f3
    // decode the raw tx and get the chainId!
    // more sparse info: https://www.npmjs.com/package/ethereum-tx-decoder
    
    const parsedTx = ethers.utils.parseTransaction(serializedTransaction);
    
    // For non-mainnet EIP155 chains, we have to create a custom network in order to instantiate
    // the validating ethereumjs-tx Transaction object
    // For EIP155 chains, v = CHAIN_ID * 2 + 35, meaning it can never be <35
    // Non-EIP155 chains use v = {27,28}
    const useCustomEip155Chain = (parsedTx.chainId !== 1) && (parsedTx.v > 28);
    // Not sure how to get `networkId` so I'm just going to use the `chainId` value for both.
    // see: https://medium.com/@pedrouid/chainid-vs-networkid-how-do-they-differ-on-ethereum-eec2ed41635b
    const customNetwork = EthereumCommon.forCustomChain('mainnet', { 
      name: 'altNetwork', 
      networkId: parsedTx.chainId,
      chainId: parsedTx.chainId,
    }, 'byzantium');
        
    let tx;
    if (true == useCustomEip155Chain)
      tx = new EthereumTx(serializedTransaction, { common: customNetwork });
    else
      tx = new EthereumTx(serializedTransaction);
    
    result = {
      chainId: `0x${parseInt(parsedTx.chainId, 16) || 1}`,
      nonce: `0x${parseInt(tx.nonce.toString('hex'), 16) || 0}`,
      gasPrice: `0x${parseInt(tx.gasPrice.toString('hex'), 16)}`,
      gasLimit: `0x${parseInt(tx.gasLimit.toString('hex'), 16)}`,
      source: `0x${tx.from.toString('hex')}`,
      target: `0x${tx.to.toString('hex')}`,
      amount: `0x${parseInt(tx.value.toString('hex'), 16) || 0}`,
      data: tx.data.length === 0 ? null : `0x${tx.data.toString('hex')}`
    };
  } catch (e) {
    return proc.fail(e);
  }
  return proc.done(result);
}

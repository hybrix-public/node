const apiLocation = window.location.href.startsWith(window.location.origin + '/api')
  ? window.location.origin + '/api'
  : window.location.origin;
const SCRIPT_A = document.createElement('SCRIPT');
SCRIPT_A.src = apiLocation + '/s/ui/ui.js';
document.head.appendChild(SCRIPT_A);
const SCRIPT_B = document.createElement('SCRIPT');
SCRIPT_B.src = apiLocation + '/s/ui/lz-string.min.js';
document.head.appendChild(SCRIPT_B);
const SCRIPT_C = document.createElement('SCRIPT');
SCRIPT_C.src = apiLocation + '/s/ui/sha256.min.js';
document.head.appendChild(SCRIPT_C);
const LINK = document.createElement('LINK');
LINK.rel = 'stylesheet';
LINK.href = apiLocation + '/s/ui/ui.css';
document.head.appendChild(LINK);

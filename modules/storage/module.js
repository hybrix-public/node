// (C) 2015 Internet of Coins / hybrix / Joachim de Koning
// hybrixd module - storage/module.js
// Module to provide storage

// IDEAS:
//  -->>> return ipfs.files.add(Buffer.from(content), { onlyHash: true })

// required libraries in this context
const storage = require('../../common/storage-fileBased');

function cron (proc) {
  //storage.autoClean();
  //proc.pass('autoclean in progress');
}

function load (proc, data) {
  storage.load({key: data.key, type: data.readFile?'string':'path'}, proc.done, proc.fail);
}

function save (proc, data) {
  storage.save({key: data.key, value: data.value, publicKey: data.publicKey, signature: data.signature, local: data.local}, proc.done, proc.fail);
}

function burn (proc, data) {
  storage.burn({key: data.key, publicKey: data.publicKey, signature: data.signature, local: data.local}, proc.done, proc.fail);
}

function seek (proc, data) {
  storage.seek({key: data.key}, proc.done, proc.fail);
}

function meta (proc, data) {
  storage.meta({key: data.key}, proc.done, proc.fail);  // previously used for POW: , sessionID: proc.sessionID
}

function list (proc, data) {
  storage.list({key: data.key}, proc.done, proc.fail);
}

function setHops (proc, data) {
  storage.setHops({key: data.key, value: data.value}, proc.done, proc.fail);
}

/* DEPRECATED
function hash (proc, data) {
  const key = proc.command && proc.command[1] ? proc.command[1] : data.key;
  storage.hash({key}, proc.done, proc.fail);
}

function work (proc, data) {
  const key = proc.command && proc.command[1] ? proc.command[1] : data.key;
  const pow = proc.command && proc.command[2] ? proc.command[2] : data.pow;
  storage.provideProof({key, pow}, proc.done, proc.fail);
}
*/

// exports
exports.cron = cron;
exports.save = save;
exports.load = load;
exports.burn = burn;
exports.seek = seek;
exports.meta = meta;
exports.list = list;
exports.setHops = setHops;
//exports.work = work;

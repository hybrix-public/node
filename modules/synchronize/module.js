const fs = require('fs');
const util = require('util');
const storage = require('../../common/storage-fileBased');
let STORAGECONF = require('../../common/storage-fileBased-defaults.json');

const paddingLength = String(STORAGECONF.synclist.randomRange).length - 1;
const fsExists = util.promisify(fs.exists);
const fsReadFile = util.promisify(fs.readFile);
const fsWriteFile = util.promisify(fs.writeFile);
const fsUnlink = util.promisify(fs.unlink);


storage.meta[util.promisify.custom] = ({key}) => {
  return new Promise((resolve, reject) => {
    storage.meta({key}, resolve, reject);
  });
};
const getMeta = util.promisify(storage.meta);

const fold = function (key) {
  return `/${key.substr(0, STORAGECONF.storage.substorageIdLength)}/`;
};

async function pullList () {
  const list = [];
  for (let i = 0; i < STORAGECONF.synclist.randomRange; i++) {
    const randomIdx = i.toString().padStart(paddingLength, '0');
    const filePath = STORAGECONF.storage.storagePath + '/sync' + randomIdx;
    let fileExists;
    try {
      fileExists = await fsExists(filePath);
      if (fileExists) {
        const syncRead = await fsReadFile(filePath);
        const syncKey = syncRead.toString(); // separate variable to avoid returning a Promise
        const syncFile = syncKey.split(':')[0]; // remove pubKey and signature
        let dataExists;
        try {
          dataExists = await fsExists(STORAGECONF.storage.storagePath + fold(syncFile) + syncFile);
          if (dataExists) list.push(syncFile);
        } catch (e) {
          console.log('Removing sync index ' + randomIdx + ' from pull list.');
          try {
            fsUnlink(filePath);
          } catch (e) {
            console.log('Cannot remove sync index ' + randomIdx + ' from pull list!');
          }
        }
      }
    } catch (e) { console.error('Seek and/or read error when synchronizing pull list! '+e); }
  }
  return list;
};

async function pull (proc) {
  const list = await pullList();
  const result = [];
  for (let key of list) {
    if(typeof key === 'string') {
      try {
        meta = await getMeta({key});
        const modHops = isNaN(meta.hops)?0:meta.hops+1;
        const modTime = meta.time.update>meta.time.create?meta.time.update:meta.time.create;
        if(modTime !== null) result.push(`${modHops}/${modTime}/${key}/${meta.hash}`+(meta.publicKey?`/${meta.publicKey}/${meta.signature}`:''));
      } catch (e) { /* console.error('Cannot read meta file (pull)! '+e); */ }
    } else {
      console.error('Empty entry in list items (pull)!');
    }
  }
  return proc.done(result);
};

async function writeSyncFile (keyAndSignature, dataCallback, errorCallback) {
  if(typeof keyAndSignature === 'string' && keyAndSignature !== '') {
    const key = keyAndSignature.split(':')[0];
    if(typeof key !== '') {
      const filePath = STORAGECONF.storage.storagePath + fold(key) + key;
      let fileExists;
      try {
        fileExists = await fsExists(filePath);
      } catch (e) {
        return errorCallback('Seek error when synchronizing (writeSyncFile)! '+e);
      }
      const list = await pullList();
      if (fileExists && !list.includes(key)) {
        const randomIdx = (Math.floor(Math.random() * STORAGECONF.synclist.randomRange)).toString().padStart(paddingLength, '0');
        try {
          await fsWriteFile(STORAGECONF.storage.storagePath + '/sync' + randomIdx, key);
        } catch (e) {
          return errorCallback('Cannot write sync file! '+e);
        }
        return dataCallback('Queued on index ' + randomIdx + '.');
      } else if (!fileExists) {
        return errorCallback('Cannot queue non-existant file!');
      } else {
        return dataCallback('File already queued for synchronization.');
      }
    } else {
      return errorCallback('Expect string containing storage key!');
    }
  } else {
    return errorCallback('Expect string containing storage key!');
  }
};

async function queue (proc, data) {
  const key = proc.command && proc.command[1] ? proc.command[1] : data.key;
  return writeSyncFile(key, proc.done, proc.fail);
};

exports.queue = queue;
exports.pull = pull;

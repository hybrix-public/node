function findPair (proc) {
  let result = [];
  const fromBase = proc.command[1];
  const toSymbol = proc.command[2];
  const minimalAmount = proc.command[3];
  const accounts = proc.peek('local::accounts') || [];
  for (const accountID in accounts) {
    if(typeof accounts[accountID].pairs[`${fromBase}:${toSymbol}`] !== 'undefined') {
      let balanceSymbol = proc.peek(`supplyOverrideSymbols.${toSymbol}`) || toSymbol;
      let pair = accounts[accountID].pairs[`${fromBase}:${toSymbol}`];
      pair.accountID = accountID;
      pair.balance = proc.peek(`local::accounts.${accountID}.balances.${balanceSymbol}`);
      if (typeof pair.balance === 'undefined' || Number(pair.balance) > Number(minimalAmount)) {
        result.push(pair);
      }
    }
  }
  return proc.done(result);
}

// exports
exports.findPair = findPair;

// (C) 2022 hybrix / Joachim de Koning
// hybrixd module - smtp/module.js
// Module to send emails using SMTP

// required libraries in this context
const conf = require('../../lib/conf/conf');
const { SMTPClient } = require('emailjs');
const fs = require('fs');
const util = require('util');
const fsExists = util.promisify(fs.exists);
const fsReadFile = util.promisify(fs.readFile);

const template = async function (proc,data) {
  if (!data.template) {
    return proc.fail('Expecting a template code!');
  }
  let templateSubject,templateMessage;
  let templateExists = false;
  const filePath = '../modules/smtp/messages.conf';
  if (typeof conf.get('smtp.template',true) !== 'undefined') templateExists = 'conf';
  else templateExists = await fsExists(filePath);
  if (templateExists) {
    try {
      let messages = {};
      if (templateExists === 'conf') messages = conf.get('smtp.template');
      else messages = JSON.parse( await fsReadFile(filePath) );
      if (messages.hasOwnProperty('en') && messages.en.hasOwnProperty(data.template)) {
        templateSubject = messages.en[data.template][0];
        templateMessage = messages.en[data.template][1];
        if(messages.hasOwnProperty('variables') && typeof messages.variables === 'object') {
          for (const key in messages.variables) {
            const varRegex = new RegExp(`%${key}%`,'g');
            templateSubject = templateSubject.replace(varRegex,messages.variables[key]);
            templateMessage = templateMessage.replace(varRegex,messages.variables[key]);
          }
        }
        if(data.hasOwnProperty('variables') && typeof data.variables === 'object') {
          for (const key in data.variables) {
            const varRegex = new RegExp(`%${key}%`,'g');
            templateSubject = templateSubject.replace(varRegex,data.variables[key]);
            templateMessage = templateMessage.replace(varRegex,data.variables[key].replace(/\\\\n/g, "\n"));
          }
        }
      } else {
        return proc.fail('Message template "'+data.template+'" not found!');
      }
      const message = {
        to: typeof data.to === 'string' && data.to.indexOf('@')>0?data.to:undefined,
        cc: data.cc?data.cc:undefined,
        subject: data.subject?data.subject:templateSubject,
        message: templateMessage
      };
      return proc.done(message);
    } catch(err) {
      return proc.fail('Messages template file malformed! '+err);
    }
  } else {
    return proc.fail('No messages template file '+filePath+' exists!');
  }
}

function send (proc,data) {
  if (!(conf.get('smtp.host') && conf.get('smtp.port') && conf.get('smtp.username') && conf.get('smtp.password') && conf.get('smtp.from'))) {
        console.log(JSON.stringify({
        host: conf.get('smtp.host'),
        port: conf.get('smtp.port'),
        ssl: conf.get('smtp.ssl'),
        user: conf.get('smtp.username'),
        from: conf.get('smtp.from'),
        password: conf.get('smtp.password')
      }));
    return proc.fail('Please configure SMTP module in hybrixd.conf! Add fields: host, port, ssl (boolean), username, password, from.');
  }
  const smtpConnection = new SMTPClient({
    host: conf.get('smtp.host'),
    port: conf.get('smtp.port'),
    ssl: conf.get('smtp.ssl'),
    user: conf.get('smtp.username'),
    password: conf.get('smtp.password')
  });
  const fromAddress = conf.get('smtp.from');
  if (!(typeof data.to === 'string' && data.to.indexOf('@')>0)||!data.subject||!data.message) {
    return proc.fail('Please configure message in data stream! Example: {to:"target@host.net",subject:"hello",message:"world!"}');
  }
  const message = {
    from: fromAddress,
    to: data.to?data.to:'',
    cc: data.cc?data.cc:undefined,
    subject: data.subject?data.subject:'',
    text: data.message?data.message.replace(/\\n/g, "\n"):''
  };

  // send the message and get a callback with an error or details of the message that was sent
  smtpConnection.send(
    message,
    (err, message) => {
      if(err) {
        return proc.fail('SMTP module error: '+err);
      } else {
        return proc.done(message);
      }
    }
  );
}

// exports
exports.sendMessage = send;
exports.template = template;

// (C) 2021 hybrix / Joachim de Koning
// hybrixd module - assetfunctions/module.js
// Helper module for exotic functions pertaining to assets

// required libraries in this context
const bchaddr = require('bchaddrjs');

function cashaddrToLegacy(proc) {
  const address = proc.command[1];
  const result = bchaddr.toLegacyAddress(address);
  return proc.done(result);
}

// exports
exports.cashaddrToLegacy = cashaddrToLegacy;

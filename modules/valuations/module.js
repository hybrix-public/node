const fs = require('fs');
const {sanitizeSymbol, makeSymbolList} = require('./parse');
const spawnParseJs = '../modules/valuations/spawnParse.js';

const INTERMEDIATE_SYMBOL_WHITELIST = ['BTC', 'ETH', 'USD', 'EUR', 'CNY'];
const MAX_HOPS = 5;
const OPTIMIZED_MODE_RANGE_THRESHOLD = 1.05; // max 5% difference allowed!
const SPAWN_TASK_TIMEOUT = 120000;

function unique (array) {
  const set = new Set(array);
  return [...set];
}

function copy (data) {
  return JSON.parse(JSON.stringify(data));
}

function singleHop (exchangeRates, startSymbol, history, rateMode) {
  const accumulator = {};
  if (!exchangeRates) return accumulator;
  if (exchangeRates.hasOwnProperty(startSymbol)) {
    const quotes = exchangeRates[startSymbol].quotes;
    const quotesKeys = Object.keys(quotes);
    for (var i=0, n=quotesKeys.length; i < n; ++i ) {
      const intermediateSymbol = quotesKeys[i];
      const rate = quotes[intermediateSymbol][rateMode];
      if (typeof rate !== 'undefined' && !isNaN(rate.rate)) {
        accumulator[intermediateSymbol] = {
          rate: history.rate * rate.rate,
          transactionPath: history.transactionPath.concat([{exchange: rate.exchange, from: startSymbol, to: intermediateSymbol, rate: rate.rate}])
        }
      }
    }
  }
  return accumulator;
}

function optimalTransaction (history1, history2) {
  const currencies = [...new Set(Object.keys(history1).concat(Object.keys(history2)))];
  const accumulator = {};
  for (var i=0, n=currencies.length; i < n; ++i ) {
    const symbol = currencies[i];
    if (!history1.hasOwnProperty(symbol)) accumulator[symbol] = history2[symbol];
    else if (!history2.hasOwnProperty(symbol)) accumulator[symbol] = history1[symbol];
    else if (history1[symbol].rate > history2[symbol].rate) accumulator[symbol] = history1[symbol];
    else accumulator[symbol] = history2[symbol];
  }
  return accumulator;
}

function bestTransactionChain (exchangeRates, startSymbol, targetSymbol, maxHops, shortestPath, rateMode, whitelist, useWhitelist) {
  whitelist = unique(whitelist.concat([startSymbol, targetSymbol]));
  let transactionChains = {};
  transactionChains[startSymbol] = {rate: 1, transactionPath: []};
  for (let i = 0; i < maxHops; i++) {
    if (shortestPath && transactionChains.hasOwnProperty(targetSymbol)) break;
    let intermediarySymbols = unique(Object.keys(transactionChains));
    if (useWhitelist) intermediarySymbols = intermediarySymbols.filter(symbol => whitelist.includes(symbol));
    const accumulator = intermediarySymbols
      .map(symbol => singleHop(exchangeRates, symbol, transactionChains[symbol], rateMode))
      .concat(transactionChains);
    transactionChains = accumulator.reduce(optimalTransaction, {});
  }
  if (transactionChains.hasOwnProperty(targetSymbol)) {
    return transactionChains[targetSymbol];
  } else {
    return {rate: 0, transactionPath: [], error: 1};
  }
}

// asynchronous functions

const rate = async (proc, data) => {
  let prices = proc.peek('local::prices', {});
  let sourceSymbol = data.source.toUpperCase();
  let targetSymbol = data.target.toUpperCase();
  sourceSymbol = sanitizeSymbol(sourceSymbol);
  targetSymbol = sanitizeSymbol(targetSymbol);
  if (sourceSymbol === null) return proc.fail(`Symbol '${data.source}' is unknown.`);
  if (targetSymbol === null) return proc.fail(`Symbol '${data.target}' is unknown.`);

  const amount = data.amount === 'undefined' || typeof data.amount === 'undefined' ? 1 : parseFloat(data.amount);
  if (isNaN(amount)) return proc.fail('Expected a numeric amount.');

  let mode = 'optimized';
  if (data.mode === 'max') mode = 'highest_rate';
  else if (data.mode === 'min') mode = 'lowest_rate';
  else if (data.mode === 'average') mode = 'average_rate';
  else if (data.mode === 'median') mode = 'median_rate';
  else if (data.mode === 'optimized') mode = 'optimized';
  else if (data.mode === 'range') mode = 'range';
  else if (data.mode === 'meta') mode = 'meta';

  let r;
  if (sourceSymbol === targetSymbol && mode !== 'meta') {
    r = amount;
  } else if (mode === 'meta') {
    const resultLow = bestTransactionChain(prices, sourceSymbol, targetSymbol, MAX_HOPS, false, 'lowest_rate', INTERMEDIATE_SYMBOL_WHITELIST, true);
    const resultHigh = bestTransactionChain(prices, sourceSymbol, targetSymbol, MAX_HOPS, false, 'highest_rate', INTERMEDIATE_SYMBOL_WHITELIST, true);
    const resultMedian = bestTransactionChain(prices, sourceSymbol, targetSymbol, MAX_HOPS, false, 'median_rate', INTERMEDIATE_SYMBOL_WHITELIST, true);
    // DEPRECATED: const resultAverage = bestTransactionChain(prices, sourceSymbol, targetSymbol, MAX_HOPS, false, 'average_rate', INTERMEDIATE_SYMBOL_WHITELIST, true);
    if (resultLow.error) return proc.fail(resultLow.error, 'Failed to compute meta rate!');
    else {
      r = {
        min: {rate: resultLow.rate * amount, path: copy(resultLow.transactionPath)},
        max: {rate: resultHigh.rate * amount, path: copy(resultHigh.transactionPath)},
        median: {rate: resultMedian.rate * amount, path: copy(resultMedian.transactionPath)},
        average: {rate: (resultLow.rate+resultHigh.rate) / 2 * amount, path: copy(resultLow.transactionPath).concat(resultHigh.transactionPath)}
      };
    }
  } else {
    if (amount === 0) r = 0; // shortcut
    else {
      let result,resultA,resultB;
      if(mode === 'average_rate' || mode === 'range' || mode === 'optimized') {
        resultA = bestTransactionChain(prices, sourceSymbol, targetSymbol, MAX_HOPS, false, 'lowest_rate', INTERMEDIATE_SYMBOL_WHITELIST, true);
        resultB = bestTransactionChain(prices, sourceSymbol, targetSymbol, MAX_HOPS, false, 'highest_rate', INTERMEDIATE_SYMBOL_WHITELIST, true);
        if (resultA.hasOwnProperty('error') && resultA.error) {
          if (resultB.hasOwnProperty('error') && resultB.error) return proc.fail(resultA.error || resultB.error, 'Failed to compute rate!');
          else return proc.fail(resultA.error, 'Failed to compute low rate!');
        } else if (resultB.hasOwnProperty('error') && resultB.error) return proc.fail(resultB.error, 'Failed to compute high rate!');
        else {
          if (mode === 'optimized') {
            const resultM = bestTransactionChain(prices, sourceSymbol, targetSymbol, MAX_HOPS, false, 'median_rate', INTERMEDIATE_SYMBOL_WHITELIST, true);
            if (resultM && resultM.error) return proc.fail(resultM.error, 'Failed to compute optimized rate!');
            const rateAvg = (resultA.rate + resultB.rate) / 2;
            if (rateAvg < resultM.rate * OPTIMIZED_MODE_RANGE_THRESHOLD && rateAvg > resultM.rate / OPTIMIZED_MODE_RANGE_THRESHOLD) {
              r = rateAvg * amount;
            } else {
              r = resultM.rate * amount;
            }
          } else if (mode === 'range') {
            r = [resultA.rate * amount, resultB.rate * amount];
          } else {
            r = (resultA.rate + resultB.rate) / 2 * amount;
          }
        }
      } else {
        result = bestTransactionChain(prices, sourceSymbol, targetSymbol, MAX_HOPS, false, mode, INTERMEDIATE_SYMBOL_WHITELIST, true);
        if (result.error) return proc.fail(result.error, 'Failed to compute rate!');
        else r = result.rate * amount;
      }
    }
  }

  return proc.done(r);
}

function spawnTask (proc, data) {
  const task = proc.command && proc.command[1] ? proc.command[1] : '';
  const { fork } = require('child_process');
  const childProcess = fork(spawnParseJs,['child'],{detached:true,timeout:SPAWN_TASK_TIMEOUT});
  childProcess.once('message', (message) => {
    const err = message.err;
    const result = message.result;
    if (err === 0) proc.done(result);
    else {
      proc.warn(`spawnTask error -> ${err}`);
      proc.fail(`spawnTask error -> ${err}`);
    }
  });
  childProcess.send({task,data});
}

exports.rate = rate;
exports.spawnTask = spawnTask;

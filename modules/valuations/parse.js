const ACCEPTED_RATE_RANGE_THRESHOLD = 1.15; // only a 15% difference between lowest and highest is accepted
const VIRTCHAR = '_';

const unifications = { // pre-defined price unifications and sanitations
  usd: [
    'tusd', 'usdc', 'usdt',
    'bnb.busd', 'bnb.vusdt',
    'eth.eusd', 'eth.dai', 'eth.tusd', 'eth.usdc', 'eth.usdt', 'trx.usdt', 'waves.usd', 'xrp.usd'
  ],
  eur: [
    'eurs', 'eurt',
    'eth.eurs', 'eth.eurt', 'waves.eur'
  ],
  cny: [
    'cnht', 'ecny',
    'eth.cnht', 'bnb.ecny'
  ],
  hy: [
    'eth.hy', 'vic.hy', 'tomo.hy-legacy', 'hy-legacy'
  ],
  smly: [
    'smiley', 'bnb.smly'
  ],
  bch: [
    'sbch'
  ],
  xau: [
    'paxg',
    'eth.paxg'
  ]
};

const FIAT_SYMBOLS = [ 'cny', 'eur', 'usd', 'gbp', 'jpy', 'aud', 'cad', 'bgn', 'brl', 'chf', 'czk', 'dkk', 'huf', 'hkd', 'hrk', 'idr', 'ils', 'inr', 'isk', 'krw', 'mxn', 'myr', 'nok', 'nzd', 'php', 'pln', 'ron', 'rub', 'sek', 'sgd', 'thb', 'try', 'zar' ];

// if symbol = 'BLA' and hybrix symbol BLA does not exists but BASE.BLA does, then use that
function sanitizeSymbol (symbolInput) {
  if (typeof symbolInput !== 'string') return null;

  let symbol = symbolInput.toLowerCase();

  if (symbol.charAt(0)===VIRTCHAR) {  // in case of a virtual asset, split and take the end
    symbol = symbol.split('.').pop();
  }

  for (const unifiedSymbol in unifications) {
    if (unifications[unifiedSymbol].includes(symbol)) return unifiedSymbol.toUpperCase();
  }

  const hybrixSymbols = Object.keys(global.hybrixd.asset);
  if (hybrixSymbols.includes(symbol)) return symbol.toUpperCase(); // 'BLA' exists as hybrix asset
  else if (FIAT_SYMBOLS.includes(symbol)) return symbol.toUpperCase(); // 'BLA' exists as fiat currency
  else {
    const tokenSymbols = [];
    for (const hybrixSymbol of hybrixSymbols) {
      if (hybrixSymbol.endsWith('.' + symbol)) tokenSymbols.push(hybrixSymbol); // 'BASE.BLA' exists as hybrix asset
    }
    if (tokenSymbols.length === 1) return tokenSymbols[0].toUpperCase(); // return 'BASE.BLA'
    // if both 'ETH.BLA' and 'WAVES.BLA' exist. Do not sanitize
    return symbolInput.toUpperCase();
  }
}

function addQuote (quotes, sourceSymbol, targetSymbol, price) {
  // Don't add quote if the input fails these requirements
  if (typeof sourceSymbol !== 'string' || typeof targetSymbol !== 'string' || isNaN(price)) return quotes;
  sourceSymbol = sourceSymbol.toUpperCase();
  targetSymbol = targetSymbol.toUpperCase();
  if (sourceSymbol === targetSymbol || price === 0 || !isFinite(price)) return quotes;

  if (!quotes.hasOwnProperty(sourceSymbol)) quotes[sourceSymbol] = {quotes: {}};

  quotes[sourceSymbol].quotes[targetSymbol] = price;
  return quotes;
}

function addBiQuote (quotes, sourceSymbol, targetSymbol, price) {
  const parsedPrice = parseFloat(price);
  if (!isNaN(parsedPrice)) {
    addQuote(quotes, sourceSymbol, targetSymbol, parsedPrice);
    addQuote(quotes, targetSymbol, sourceSymbol, 1 / parsedPrice);
  }
}


function parseCoingecko (obj) {
  /* https://api.coingecko.com/api/v3/simple/price?ids=hybrix,ethereum&vs_currencies=usd */
  const name = 'coingecko';
  const quotes = {};
  if (typeof obj !== 'object' || obj === null) return {name, quotes};

  const nameToSymbol = {
    'ethereum':'ETH',
    'dogecoin':'DOGE',
    'litecoin':'LTC',
    'polygon':'MATIC',
    'avalanche':'AVAX',
    'shiba-inu':'SHIB',
    'tron':'TRX',
    'uniswap':'UNI',
    'wrapped-bitcoin':'WBTC',
    'bitcoin-cash':'BCH'
  }
  Object.keys(obj).forEach(function (key, index) {
    const price = obj[key].usd;
    const sourceSymbol = nameToSymbol[key]?nameToSymbol[key]:key.toUpperCase();
    if (sourceSymbol) {
      // DEBUG: if(targetSymbol === 'HY') console.log(` >>> ${name} ${price} ${targetSymbol}`);
      addBiQuote(quotes, sourceSymbol, 'USD', price);
    }
  });
  return {name: name, quotes};
}

function parseBancor (obj) {
  const name = 'bancor';
  const quotes = {};
  if (typeof obj !== 'object' || obj === null || !obj.hasOwnProperty('data')) return {name, quotes};

  Object.keys(obj.data).forEach(function (key, index) {
    const price = obj.data[key].price.usd;
    const sourceSymbol = obj.data[key].symbol;
    // DEBUG: if(targetSymbol === 'HY') console.log(` >>> ${name} ${price} ${targetSymbol}`);
    addBiQuote(quotes, sourceSymbol, 'USD', price);
  });
  return {name: name, quotes};
}

function parseCoinmarketcap (obj) {
  const name = 'coinmarketcap';
  const quotes = {};
  if (typeof obj !== 'object' || obj === null || !obj.hasOwnProperty('data')) return {name, quotes};

  Object.keys(obj.data).forEach(function (key, index) {
    const price = 1 / obj.data[key].quotes.USD.price;
    const targetSymbol = obj.data[key].symbol;
    addBiQuote(quotes, 'USD', targetSymbol, price);
  });
  return {name: name, quotes};
}

function parseCoinbase (obj) {
  const name = 'coinbase';
  const quotes = {};
  if (typeof obj !== 'object' || obj === null || typeof obj.data !== 'object' || obj.data === null) return {name, quotes};

  const objKeys = Object.keys(obj.data.rates);
  for (var i=0, n=objKeys.length; i < n; ++i ) {
    const targetSymbol = objKeys[i];
    const price = obj.data.rates[targetSymbol];
    addBiQuote(quotes, 'USD', targetSymbol, price);
  }
  return {name, quotes};
}

function parseBinance (obj) {
  const name = 'binance';
  const baseCurrencies = [
    'BTC', 'ETH', 'BNB', 'TRX',
    'USDT', 'USDC', 'BUSD', 'DAI',
    'EUR', 'AUD', 'GBP', 'RUB', 'VAI', 'TRY', 'BRL', 'BIDR', 'PAX'
  ];
  const ignoreSymbols = [ 'ALPHA' ];
  const quotes = {};
  if (typeof obj !== 'object' || obj === null) return {name, quotes};

  const objKeys = Object.keys(obj);
  for (var i=0, n=objKeys.length; i < n; ++i ) {
    const key = objKeys[i];
    const symbolPair = obj[key].symbol;
    const targetSymbol = baseCurrencies.find(currency => symbolPair.endsWith(currency));
    if (targetSymbol) {
      const sourceSymbol = symbolPair.slice(0, symbolPair.length - targetSymbol.length);
      if (!ignoreSymbols.includes(sourceSymbol) && !ignoreSymbols.includes(targetSymbol)) {
        const price = parseFloat(obj[key].price);
        addBiQuote(quotes, sourceSymbol, targetSymbol, price);
      }
    }
  }
  return {name, quotes};
}

function parseHitbtc (priceObjects, symbolObjects, symbols) {
  const name = 'hitbtc';
  const quotes = {};
  if (!(priceObjects instanceof Array) || !(symbolObjects instanceof Array)) return {name, quotes};

  /*
priceObjects [
  {
    id: 'BRDETH',
    baseCurrency: 'BRD',
    quoteCurrency: 'ETH',
    quantityIncrement: '0.1',
    tickSize: '0.00000001',
    takeLiquidityRate: '0.0025',
    provideLiquidityRate: '0.001',
    feeCurrency: 'ETH'
  }, ...
  symbolObjects [
    {
      symbol: 'VEOBTC',
      ask: '0.001519',
      bid: '0.001300',
      last: '0.001398',
      low: '0.001398',
      high: '0.001400',
      open: '0.001516',
      volume: '0.267',
      volumeQuote: '0.000373268',
      timestamp: '2021-02-24T08:55:00.001Z'
    }, ...
 */
  const symbolObjectsByPair = {};
  symbolObjects.forEach(symbolObject => { symbolObjectsByPair[symbolObject.symbol] = symbolObject; });
  const priceObjectsByPair = {};
  priceObjects.forEach(priceObject => { priceObjectsByPair[priceObject.id] = priceObject; });
  for (const pair in priceObjectsByPair) {
    const priceObject = priceObjectsByPair[pair];
    if (symbolObjectsByPair.hasOwnProperty(pair)) {
      const symbolObject = symbolObjectsByPair[pair];
      const baseCurrency = sanitizeSymbol(priceObject.baseCurrency, symbols);
      const quoteCurrency = sanitizeSymbol(priceObject.quoteCurrency, symbols);
      if (quoteCurrency && baseCurrency) { // only if symbols are known in hybrix
        if (parseFloat(symbolObject.ask)/parseFloat(symbolObject.bid) <= 0.2) { // only if bid and ask are within 20%
          const price = (parseFloat(symbolObject.bid) + parseFloat(symbolObject.ask)) * 0.5;
          addBiQuote(quotes, baseCurrency, quoteCurrency, price);
        }
      }
    }
  }
  return {name, quotes};
}

function parseDefault (data, name) {
  const quotes = {};
  if (!(data instanceof Array)) return {name, quotes};
  for (const pair of data) {
    addBiQuote(quotes, pair.from, pair.to, pair.price);
  }
  return {name, quotes};
}

/*
exchangeRates =  {
[sourceCurrency]: {
 quotes: {
   [targetCurrency]: {
     sources,
     highest_rate  {exchange, rate},
     lowest_rate: {exchange, rate},
     median_rate: {exchange, rate}
     }
   }
 }
}

sources = [{name, quotes},...]
 */
function addUpdatedExchangeRates (exchangeRates, sources) {
  for (const source of sources) { // source = {name, quotes}
    try {
      const name = source.name;
      for (const sourceSymbol in source.quotes) {
        if (!exchangeRates.hasOwnProperty(sourceSymbol)) exchangeRates[sourceSymbol] = {quotes: {}};
        const newQuotes = source.quotes[sourceSymbol].quotes;
        for (const targetSymbol in newQuotes) {
          if (!exchangeRates[sourceSymbol].quotes.hasOwnProperty(targetSymbol)) exchangeRates[sourceSymbol].quotes[targetSymbol] = {sources: {}};
          exchangeRates[sourceSymbol].quotes[targetSymbol].sources[name] = newQuotes[targetSymbol];
        }
      }
    } catch (error) {
      const name = typeof source === 'object' && source !== null ? source.name : 'unknown';
      // DEPRECATED: proc.warn(`Failed to add source ${name} : ${error}`);
    }
  }
  return exchangeRates;
}

/* IN exchangeRates =  {[sourceCurrency]: {quotes:{[targetCurrency] : {sources}}}} // might not all have rates yet
 OUT exchangeRates =  {
 [sourceCurrency]: {
  quotes: {
    [targetCurrency]: {
      sources,
      highest_rate  {exchange, rate},
      lowest_rate: {exchange, rate},
      median_rate: {exchange, rate}
      }
    }
  }
}
*/
function enrichExchangeRates (exchangeRates) {
  const exchangeRatesKeys = Object.keys(exchangeRates);
  for (var i=0, n=exchangeRatesKeys.length; i < n; ++i ) {
    const sourceSymbol = exchangeRatesKeys[i];
    const quotes = exchangeRates[sourceSymbol].quotes;
    if (typeof quotes !== 'undefined') {
      const quotesKeys = Object.keys(quotes);
      for (var j=0, m=quotesKeys.length; j < m; ++j ) {
        const targetSymbol = quotesKeys[j];
        const exchanges = quotes[targetSymbol].sources;
        const sortedExchangePrices = Object.keys(exchanges).sort((exchange1, exchange2) => exchanges[exchange2] - exchanges[exchange1]);

        const lowPoint = sortedExchangePrices.length - 1;
        let highestPrice = exchanges[sortedExchangePrices[0]];
        let lowestPrice = exchanges[sortedExchangePrices[lowPoint]];
        if (lowestPrice === null) lowestPrice = highestPrice;
        else if (highestPrice === null) highestPrice = lowestPrice;

        if (lowestPrice && highestPrice && lowestPrice * ACCEPTED_RATE_RANGE_THRESHOLD < highestPrice && sortedExchangePrices.length < 3) {
          delete exchangeRates[sourceSymbol].quotes[targetSymbol];
        } else {
          exchangeRates[sourceSymbol].quotes[targetSymbol].highest_rate = {exchange: sortedExchangePrices[0], rate: highestPrice};
          exchangeRates[sourceSymbol].quotes[targetSymbol].lowest_rate = {exchange: sortedExchangePrices[lowPoint], rate: lowestPrice};
          // median
          const midPoint = (sortedExchangePrices.length - 1) / 2;
          const floorExchange = sortedExchangePrices[Math.floor(midPoint)];
          const ceilExchange = sortedExchangePrices[Math.ceil(midPoint)];
          const exchange = floorExchange === ceilExchange ? ceilExchange : (floorExchange + '|' + ceilExchange);
          exchangeRates[sourceSymbol].quotes[targetSymbol].median_rate = {
            exchange,
            rate: (parseFloat(exchanges[floorExchange]) + parseFloat(exchanges[ceilExchange])) / 2
          };
        }
      }
    }
  }
  return exchangeRates;
}

function makeSymbolList (enrichedExchangeRates) {
  const symbols = [];
  for (const sourceSymbol in enrichedExchangeRates) {
    if (!symbols.includes(sourceSymbol)) symbols.push(sourceSymbol);
    for (const targetSymbol in enrichedExchangeRates[sourceSymbol].quotes) {
      if (!symbols.includes(targetSymbol)) symbols.push(targetSymbol);
    }
  }
  return symbols;
}

function parseData (data) {
  const currentExchangeRates = typeof data === 'object' && data !== null && typeof data.rates === 'object' && data.rates !== null ? data.rates : {};
  const updatedExchangeRates = addUpdatedExchangeRates(currentExchangeRates, [
    parseDefault(data.hybrixSwapRates, 'hybrixSwapRates'),
    parseDefault(data.EUCentralBank, 'EUCentralBank'),
    parseDefault(data.coincodex, 'coincodex'),
    parseDefault(data.cryptocompare, 'cryptocompare'),
    parseCoinbase(data.coinbase),
    parseBinance(data.binance),
    parseCoingecko(data.coingecko)
    // These sources have been disabled!
    // parseBancor(data.bancor),
    // parseDefault(data.uni_swap_v2, 'uni_swap_v2'),
    // parseDefault(data.nomics, 'nomics'),
    // parseCoinmarketcap(data.coinmarketcap)
    // parseDefault(data.tomo_dex, 'tomo_dex')
    // parseHitbtc(data.hitbtc_symbols, data.hitbtc_prices, data.symbols)
  ]);
  const enrichedExchangeRates = enrichExchangeRates(updatedExchangeRates);
  return enrichedExchangeRates;
}

exports.sanitizeSymbol = sanitizeSymbol;
exports.makeSymbolList = makeSymbolList;
exports.parseData = parseData;

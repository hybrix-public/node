const process = require('process');
process.once('message', (message) => {
  const task = message.task;
  const data = message.data;
  const parse = require('./parse');
  var result = null;
  var err = 0;
  if (task === 'parseData') result = parse.parseData(data);
  else if (task === 'makeSymbolList') result = parse.makeSymbolList(data);
  else err = 'spawnParse expects task! Should contain one of: parseData, makeSymbolList';
  if (!result) err = 'unexpectedly produced no output!';
  process.send({err,result});
});

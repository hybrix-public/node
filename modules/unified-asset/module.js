// (C) 2020 hybrix / Rouke Pouw / Joachim de Koning
// hybrixd module - unified asset
// Module to unify assets

// required libraries in this context
const compressUnifiedAddress = require('../../common/compress-unified-address');

// functions
function compress (proc, address) {
  const symbol = proc.command[1];
  proc.done(symbol + ':' + compressUnifiedAddress.encode(address));
}

function decompress (proc, address) {
  const symbol = proc.command[1];
  let decoded = null;
  if (symbol) {
    if (address.startsWith(symbol + ':')) { // `${symbol}:${encoded}`
      decoded = compressUnifiedAddress.decode(address.split(':')[1]);
    } else if(!address.includes(':')) {
      decoded = compressUnifiedAddress.decode(address);
    } else if(address.includes(':') && address.includes(',')) {
      decoded = address;
    } else {
      proc.fail('Unified asset main symbol deviates from prefix symbol!');
    }
  } else {
    proc.fail('Unified asset main symbol must be specified!');
  }
  if (decoded === null) return proc.fail('Could not parse unified asset address! -> '+address);
  return proc.done(decoded); // `${subSymbol}:${subAddress},...`
}

// exports
exports.compressUnifiedAddress = compress;
exports.decompressUnifiedAddress = decompress;
